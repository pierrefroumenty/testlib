export interface Territory {
    id: string;
    name: string;
    region_id: string;
    hidden?: boolean;
}
