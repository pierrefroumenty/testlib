export interface Index<T> {
    [key: number]: T;
}
