export interface Commune {
    id: string;
    name: string;
    region_id: string;
    territory_id: string;
    contract_id: string;
    hidden?: boolean;
}
