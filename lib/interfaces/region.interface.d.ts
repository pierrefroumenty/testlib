export interface Region {
    id: string;
    name: string;
    hidden?: boolean;
}
