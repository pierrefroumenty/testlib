import { Region } from "./region.interface";
import { Contract } from "./contract.interface";
import { Territory } from "./territory.interface";
import { Commune } from "./commune.interface";
export interface FilterOptions {
    regions: Array<Region>;
    territories: Array<Territory>;
    contracts: Array<Contract>;
    communes: Array<Commune>;
}
