export interface Contract {
    id: string;
    name: string;
    region_id: string;
    territory_id: string;
    hidden?: boolean;
}
