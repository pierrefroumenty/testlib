import { OnChanges, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
export declare class IsLoadingDirective implements OnChanges {
    private viewContainerRef;
    private componentFactoryResolver;
    loader: any;
    isLoading: boolean;
    constructor(viewContainerRef: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver);
    ngOnChanges(): void;
    removeLoader(): void;
    addLoader(): void;
}
