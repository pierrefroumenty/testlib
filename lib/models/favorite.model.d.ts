import { Filter } from './filter.model';
export declare class Favorite {
    private _id;
    private _filter;
    private _label;
    constructor(data?: any);
    getId(): number;
    getFilter(): Filter;
    getLabel(): string;
    setId(id: number | null): Favorite;
    setFilter(filter: Filter): Favorite;
    setLabel(label: string): Favorite;
    static fromJSON(json: any): Favorite;
    toObject(): object;
    toJSON(): string;
}
