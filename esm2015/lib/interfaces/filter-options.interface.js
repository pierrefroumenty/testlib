/**
 * @fileoverview added by tsickle
 * Generated from: lib/interfaces/filter-options.interface.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function FilterOptions() { }
if (false) {
    /** @type {?} */
    FilterOptions.prototype.regions;
    /** @type {?} */
    FilterOptions.prototype.territories;
    /** @type {?} */
    FilterOptions.prototype.contracts;
    /** @type {?} */
    FilterOptions.prototype.communes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLW9wdGlvbnMuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHRlbGVvL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmZhY2VzL2ZpbHRlci1vcHRpb25zLmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUtBLG1DQUtDOzs7SUFKQyxnQ0FBdUI7O0lBQ3ZCLG9DQUE4Qjs7SUFDOUIsa0NBQTJCOztJQUMzQixpQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWdpb24gfSBmcm9tIFwiLi9yZWdpb24uaW50ZXJmYWNlXCI7XHJcbmltcG9ydCB7IENvbnRyYWN0IH0gZnJvbSBcIi4vY29udHJhY3QuaW50ZXJmYWNlXCI7XHJcbmltcG9ydCB7IFRlcnJpdG9yeSB9IGZyb20gXCIuL3RlcnJpdG9yeS5pbnRlcmZhY2VcIjtcclxuaW1wb3J0IHsgQ29tbXVuZSB9IGZyb20gXCIuL2NvbW11bmUuaW50ZXJmYWNlXCI7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZpbHRlck9wdGlvbnMge1xyXG4gIHJlZ2lvbnM6IEFycmF5PFJlZ2lvbj47XHJcbiAgdGVycml0b3JpZXM6IEFycmF5PFRlcnJpdG9yeT47XHJcbiAgY29udHJhY3RzOiBBcnJheTxDb250cmFjdD47XHJcbiAgY29tbXVuZXM6IEFycmF5PENvbW11bmU+O1xyXG59Il19