/**
 * @fileoverview added by tsickle
 * Generated from: lib/interfaces/commune.interface.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function Commune() { }
if (false) {
    /** @type {?} */
    Commune.prototype.id;
    /** @type {?} */
    Commune.prototype.name;
    /** @type {?} */
    Commune.prototype.region_id;
    /** @type {?} */
    Commune.prototype.territory_id;
    /** @type {?} */
    Commune.prototype.contract_id;
    /** @type {?|undefined} */
    Commune.prototype.hidden;
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbXVuZS5pbnRlcmZhY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AdGVsZW8vY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2ludGVyZmFjZXMvY29tbXVuZS5pbnRlcmZhY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSw2QkFRQzs7O0lBUEcscUJBQVU7O0lBQ1YsdUJBQVk7O0lBQ1osNEJBQWlCOztJQUNqQiwrQkFBb0I7O0lBQ3BCLDhCQUFtQjs7SUFDbkIseUJBQWlCOztBQUVwQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBDb21tdW5lIHtcclxuICAgIGlkOiBzdHJpbmdcclxuICAgIG5hbWU6IHN0cmluZ1xyXG4gICAgcmVnaW9uX2lkOiBzdHJpbmdcclxuICAgIHRlcnJpdG9yeV9pZDogc3RyaW5nXHJcbiAgICBjb250cmFjdF9pZDogc3RyaW5nXHJcbiAgICBoaWRkZW4/OiBib29sZWFuO1xyXG5cclxufTsiXX0=