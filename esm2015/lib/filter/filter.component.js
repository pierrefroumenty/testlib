/**
 * @fileoverview added by tsickle
 * Generated from: lib/filter/filter.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { cloneDeep } from 'lodash';
import { Filter } from '../models/filter.model';
export class FilterComponent {
    constructor() {
        this.options = null; // don't modify this object, except the ".hidden" properties
        this.filterChanged = new EventEmitter();
        this.deleteFavorite = new EventEmitter();
        this.saveAsFavorite = new EventEmitter();
        this.sortedOptions = null;
        this.devMode = false;
        this.closed = false;
        this.selectedTab = 0;
        this.form = {
            contracts: [],
            regions: [],
            territories: [],
            communes: [],
            searchRegions: '',
            searchTerritories: '',
            searchContracts: '',
            searchCommunes: '',
        };
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.filter)
            this.setForm(this.filter);
        if (changes.options) {
            this.options = cloneDeep(this.options);
            this.updateOptions();
        }
    }
    /**
     * @return {?}
     */
    isSubmittable() {
        return this.form.regions.length || this.form.territories.length || this.form.contracts.length;
    }
    /**
     * @param {?} filter
     * @return {?}
     */
    setForm(filter) {
        this.form.contracts = filter.getContracts();
        this.form.regions = filter.getRegions();
        this.form.territories = filter.getTerritories();
        this.form.communes = filter.getCommunes();
        this.onChange();
    }
    /**
     * @return {?}
     */
    onChange() {
        // clean user selection (remove impossible cases)
        this.cleanSelection();
        this.updateOptions();
    }
    // -----------------------------------------
    /**
     * @return {?}
     */
    cleanSelection() {
        if (!this.options)
            return;
        /** @type {?} */
        let filter = new Filter(this.form);
        /** @type {?} */
        let cleanedFilter = cloneDeep(filter);
        /** @type {?} */
        let currentTerritories = cleanedFilter.getTerritories().map((/**
         * @param {?} id
         * @return {?}
         */
        id => this.options.territories.find((/**
         * @param {?} t
         * @return {?}
         */
        t => t.id == id))));
        /** @type {?} */
        let currentContracts = cleanedFilter.getContracts().map((/**
         * @param {?} id
         * @return {?}
         */
        id => this.options.contracts.find((/**
         * @param {?} c
         * @return {?}
         */
        c => c.id == id))));
        /** @type {?} */
        let currentCommunes = cleanedFilter.getCommunes().map((/**
         * @param {?} id
         * @return {?}
         */
        id => this.options.communes.find((/**
         * @param {?} c
         * @return {?}
         */
        c => c.id == id))));
        // filter by regions
        if (cleanedFilter.getRegions().length) {
            cleanedFilter.setTerritories(currentTerritories.filter((/**
             * @param {?} t
             * @return {?}
             */
            (t) => cleanedFilter.getRegions().includes(t.region_id))).map((/**
             * @param {?} t
             * @return {?}
             */
            t => t.id)));
            cleanedFilter.setContracts(currentContracts.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getRegions().includes(c.region_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getRegions().includes(c.region_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
        }
        // filter by territories
        if (cleanedFilter.getTerritories().length) {
            cleanedFilter.setContracts(currentContracts.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getTerritories().includes(c.territory_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getTerritories().includes(c.territory_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
        }
        // filter by contracts
        if (cleanedFilter.getContracts().length) {
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getContracts().includes(c.contract_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
        }
        // update form if the cleaned version of the filter is different
        if (!cleanedFilter.isEqualTo(filter))
            this.setForm(cleanedFilter);
    }
    // -----------------------------------------
    /**
     * @return {?}
     */
    updateOptions() {
        if (!this.options)
            return;
        /** @type {?} */
        let filter = new Filter(this.form);
        /** @type {?} */
        const filterRegions = filter.getRegions();
        /** @type {?} */
        const filterTerritories = filter.getTerritories();
        /** @type {?} */
        const filterContracts = filter.getContracts();
        // filter region options
        this.options.regions.map((/**
         * @param {?} r
         * @return {?}
         */
        r => {
            /** @type {?} */
            const passSearchFilter = !this.form.searchRegions.length || (String.search(this.form.searchRegions, r.name) || String.search(this.form.searchRegions, r.id));
            /** @type {?} */
            const pass = passSearchFilter;
            r.hidden = !pass;
            return r;
        }));
        // filter territory options
        this.options.territories.map((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            /** @type {?} */
            const passRegionFilter = !filterRegions.length || filterRegions.includes(t.region_id);
            /** @type {?} */
            const passSearchFilter = !this.form.searchTerritories.length || (String.search(this.form.searchTerritories, t.name) || String.search(this.form.searchTerritories, t.id));
            /** @type {?} */
            const pass = passRegionFilter && passSearchFilter;
            /** @type {?} */
            const hidden = !pass;
            if (t.hidden !== hidden)
                t.hidden = hidden;
            return t;
        }));
        // filter contract options
        this.options.contracts.map((/**
         * @param {?} c
         * @return {?}
         */
        c => {
            /** @type {?} */
            const passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
            /** @type {?} */
            const passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
            /** @type {?} */
            const passSearchFilter = !this.form.searchContracts.length || (String.search(this.form.searchContracts, c.name) || String.search(this.form.searchContracts, c.id));
            /** @type {?} */
            const pass = passRegionFilter && passTerritoryFilter && passSearchFilter;
            /** @type {?} */
            const hidden = !pass;
            if (c.hidden !== hidden)
                c.hidden = hidden;
            return c;
        }));
        // filter commune options
        this.options.communes.map((/**
         * @param {?} c
         * @return {?}
         */
        c => {
            /** @type {?} */
            const passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
            /** @type {?} */
            const passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
            /** @type {?} */
            const passContractFilter = !filterContracts.length || filterContracts.includes(c.contract_id);
            /** @type {?} */
            const passSearchFilter = !this.form.searchCommunes.length || (String.search(this.form.searchCommunes, c.name) || String.search(this.form.searchCommunes, c.id));
            /** @type {?} */
            const pass = passRegionFilter && passTerritoryFilter && passContractFilter && passSearchFilter;
            /** @type {?} */
            const hidden = !pass;
            if (c.hidden !== hidden)
                c.hidden = hidden;
            return c;
        }));
        // Below, we put hidden options at the end, so that mat-select's scrolling behaves correctly
        // 1. this way we also can use "visibility" css property which is faster than *ngIf which would insert/removes DOM nodes
        // 2. this way, using the search field doesn't remove our previous selection
        this.sortedOptions = {
            regions: this.options.regions.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
            territories: this.options.territories.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
            contracts: this.options.contracts.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
            communes: this.options.communes.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
        };
    }
    /**
     * @param {?} index
     * @param {?} element
     * @return {?}
     */
    trackElement(index, element) {
        return element ? element.id : null;
    }
    // SELECT / DESELECT --------------------------------------------
    /**
     * @return {?}
     */
    selectAllRegions() {
        if (!this.options)
            return;
        this.form.regions = this.options.regions.map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.id));
        this.onChange();
    }
    /**
     * @return {?}
     */
    deselectAllRegions() {
        this.form.regions = [];
        this.form.territories = [];
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    }
    /**
     * @return {?}
     */
    selectAllTerritories() {
        if (!this.options)
            return;
        this.form.territories = this.options.territories.map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.id));
        this.onChange();
    }
    /**
     * @return {?}
     */
    deselectAllTerritories() {
        this.form.territories = [];
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    }
    /**
     * @return {?}
     */
    selectAllContracts() {
        if (!this.options)
            return;
        this.form.contracts = this.options.contracts.map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.id));
        this.onChange();
    }
    /**
     * @return {?}
     */
    deselectAllContracts() {
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    _handleKeydown(event) {
        event.stopPropagation();
    }
    /**
     * @return {?}
     */
    getRegionsPlaceholder() {
        /** @type {?} */
        const selectedOptions = this.form && (this.form.regions || []).length;
        /** @type {?} */
        const availableOptions = this.countRegionsOptions();
        return this.devMode ? `Régions (${selectedOptions}/${availableOptions})` : `Régions (${selectedOptions})`;
    }
    /**
     * @return {?}
     */
    getTerritoriesPlaceholder() {
        /** @type {?} */
        const selectedOptions = this.form && (this.form.territories || []).length;
        /** @type {?} */
        const availableOptions = this.countTerritoriesOptions();
        return this.devMode ? `Territoires (${selectedOptions}/${availableOptions})` : `Territoires (${selectedOptions})`;
    }
    /**
     * @return {?}
     */
    getContractsPlaceholder() {
        /** @type {?} */
        const selectedOptions = this.form && (this.form.contracts || []).length;
        /** @type {?} */
        const availableOptions = this.countContractsOptions();
        return this.devMode ? `Contrats (${selectedOptions}/${availableOptions})` : `Contrats (${selectedOptions})`;
    }
    /**
     * @return {?}
     */
    countRegionsOptions() {
        if (!this.options)
            return 0;
        return this.options.regions.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.hidden)).length;
    }
    /**
     * @return {?}
     */
    countTerritoriesOptions() {
        if (!this.options)
            return 0;
        return this.options.territories.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.hidden)).length;
    }
    /**
     * @return {?}
     */
    countContractsOptions() {
        if (!this.options)
            return 0;
        return this.options.contracts.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.hidden)).length;
    }
    // APPLY -------------------------------------------------------------
    /**
     * @param {?} field
     * @return {?}
     */
    emptySearchField(field) {
        // empty search fields
        if (this.form[field].length) {
            this.form[field] = '';
            this.updateOptions();
        }
    }
    /**
     * @return {?}
     */
    reinit() {
        /** @type {?} */
        const emptyFilter = new Filter();
        // empty search fields
        this.form.searchRegions = '';
        this.form.searchTerritories = '';
        this.form.searchContracts = '';
        this.form.searchCommunes = '';
        this.setForm(emptyFilter);
    }
    /**
     * @return {?}
     */
    _saveAsFavorite() {
        /** @type {?} */
        let filter = new Filter(this.form);
        this.saveAsFavorite.emit(filter);
    }
    /**
     * @param {?} favorite
     * @return {?}
     */
    _deleteFavorite(favorite) {
        this.deleteFavorite.emit(favorite);
    }
    /**
     * @return {?}
     */
    applyFilter() {
        /** @type {?} */
        let filter = new Filter(this.form);
        this.filterChanged.emit(filter);
    }
    /**
     * @param {?} favorite
     * @return {?}
     */
    applyFavorite(favorite) {
        this.setForm(favorite.getFilter());
        this.applyFilter();
        this.goToFilterTab();
    }
    /**
     * @return {?}
     */
    goToFilterTab() {
        this.selectedTab = 0;
    }
    /**
     * @return {?}
     */
    closeNav() {
        this.closed = true;
    }
    /**
     * @return {?}
     */
    openNav() {
        this.closed = false;
    }
}
FilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'teleo-filter',
                template: "<!-- HIDE/SHOW BUTTON -->\n<div [ngClass]=\"{'filter-button' : true, 'filter-button--hidden' : !closed}\" (click)=\"openNav();\">\n  <mat-icon class=\"filter-button__icon\">filter_list</mat-icon>\n</div>\n\n<div class=\"wrapper\" [ngClass]=\"{'wrapper--hidden': closed}\">\n\n  <!-- HIDE BUTTON -->\n  <mat-icon class=\"sidebar-close\" (click)=\"closeNav()\">arrow_back</mat-icon>\n\n  <mat-tab-group [(selectedIndex)]=\"selectedTab\" dynamicHeight=\"true\" animationDuration=\"200ms\"> \n    \n    <!-- FILTER ----------------------------------->\n    <mat-tab label=\"FILTRE\">\n      \n      <!-- BODY -->\n      <div class=\"filters\" [appIsLoading]=\"!options\">\n\n        <!-- REGIONS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchRegions')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getRegionsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.regions\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchRegions\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllRegions()\">Toutes</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllRegions()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let region of sortedOptions?.regions; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: region.hidden ? 'none' : 'flex'}\" [value]=\"region.id\" matTooltip=\"{{ region.name }}\"\n                matTooltipPosition=\"above\" matTooltipShowDelay=\"500\">\n                {{ region.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- TERRITOIRES -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchTerritories')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getTerritoriesPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.territories\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchTerritories\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllTerritories()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllTerritories()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let territory of sortedOptions?.territories; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: territory.hidden ? 'none' : 'flex'}\" \n                [value]=\"territory.id\" matTooltip=\"{{ territory.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ territory.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- CONTRATS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchContracts')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getContractsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.contracts\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchContracts\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllContracts()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllContracts()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let contract of sortedOptions?.contracts; trackBy: trackElement\">\n              <mat-option [value]=\"contract.id\" [ngStyle]=\"{display: contract.hidden ? 'none' : 'flex'}\" \n                matTooltip=\"{{ contract.id }} - {{ contract.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ contract.id }} - {{ contract.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n\n\n\n      </div>\n    </mat-tab>\n\n\n    <!-- FAVORITES ----------------------------------------------------->\n    <mat-tab label=\"FAVORIS\" *ngIf=\"favorites\">\n      <div class=\"favorites\" [appIsLoading]=\"isFetchingFavorites$ | async\">\n        <div class=\"favorite\" *ngFor=\"let favorite of (favorites | keyvalue)\">\n          <div class=\"favorite__label\" matRipple matTooltip=\"{{ favorite?.value?.getLabel() }}\" matTooltipPosition=\"above\" matTooltipShowDelay=\"500\" (click)=\"applyFavorite(favorite.value)\">{{ favorite?.value?.getLabel() }}</div>\n          <div class=\"favorite__delete\" matRipple (click)=\"_deleteFavorite(favorite.value)\"><mat-icon>delete</mat-icon></div>\n        </div>\n      </div>\n      \n    </mat-tab>\n  </mat-tab-group>\n\n  <!-- FOOTER -->\n  <div [ngClass]=\"{'filter-footer--hidden': selectedTab !== 0, 'filter-footer': true}\">\n    <button mat-stroked-button color=\"primary\" (click)=\"reinit();\">R\u00C9INITIALISER LES FILTRES</button>\n    <button mat-stroked-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"_saveAsFavorite()\">SAUVEGARDER</button>\n    <button mat-raised-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"applyFilter()\">APPLIQUER</button>\n  </div>\n</div>",
                styles: [":host{position:relative}:host ::ng-deep .mat-tab-label{min-width:115px;font-family:TheSans,Roboto,sans-serif;font-size:16px;line-height:22px;font-weight:600;height:72px;color:#353535;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}:host ::ng-deep .mat-tab-label.mat-tab-label-active{color:#353535;opacity:1}.dropdown-controls{display:-webkit-box;display:flex;position:-webkit-sticky;position:sticky;top:0;background:#fff;z-index:10;box-shadow:0 0 3px 1px #c5c5c5}.filter-button{position:absolute;top:0;left:0;width:36px;height:72px;background-color:#804180;border-radius:0 16px 16px 0;box-shadow:0 2px 4px 0 rgba(0,0,0,.5);color:#fff;cursor:pointer;z-index:90;-webkit-transition:left .2s;transition:left .2s}.filter-button__icon{margin-top:24px;margin-left:4px}.filter-button--hidden{left:-40px}.filters{padding:25px;display:block;overflow-y:auto;opacity:1}.favorites{display:block;overflow-y:auto;opacity:1;min-height:220px;max-height:50vh;overflow:auto}.favorites:empty{display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.favorites:empty::before{content:\"Vous n'avez pas de favoris\"}.favorite{display:-webkit-box;display:flex;width:100%;-webkit-box-align:stretch;align-items:stretch;border-bottom:#f0f0f0;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer}.favorite:hover{background:#f0f0f0}.favorite__label{padding:15px 0 15px 15px;font-family:TheSans,Roboto,sans-serif;-webkit-box-flex:1;flex-grow:1;text-overflow:ellipsis;white-space:nowrap;overflow:hidden}.favorite__delete{cursor:pointer;width:50px;height:50px;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center;display:-webkit-box;display:flex;flex-shrink:0}.favorite__delete:hover{color:#e0421f}.wrapper{margin-left:15px;width:300px;max-width:300px;z-index:90;border-radius:0 20px;box-shadow:0 2px 10px 0 rgba(0,0,0,.24);font-family:TheSans,Roboto,sans-serif;background:#fff;-webkit-transition:width .2s,left .2s,margin .2s;transition:width .2s,left .2s,margin .2s;position:relative;left:0;overflow:hidden}.wrapper--hidden{width:0;left:-300px;margin-left:0;margin-right:51px}.w100{width:100%}.sidebar-close{position:absolute;right:28px;top:24px;font-size:20px;cursor:pointer;font-weight:700;color:#000;z-index:100}.filter-footer{font-weight:600;max-height:350px;padding:24px;border-top:1px solid #ccc;border-bottom-left-radius:20px;overflow:hidden;-webkit-transition:max-height .2s,padding .2s;transition:max-height .2s,padding .2s}.filter-footer button{width:100%;display:block;margin:0 auto 9.5px;height:37px}.filter-footer--hidden{max-height:0;padding:0}.checkbox{margin-top:5px;padding:0;box-sizing:border-box;font-size:13px;margin-bottom:30px}.checkbox ::ng-deep .mat-checkbox-label{white-space:normal}.search{width:100%;border:none;padding:10px 15px}.search:focus{outline:0}"]
            }] }
];
/** @nocollapse */
FilterComponent.ctorParameters = () => [];
FilterComponent.propDecorators = {
    options: [{ type: Input, args: ['options',] }],
    filter: [{ type: Input, args: ['filter',] }],
    favorites: [{ type: Input, args: ['favorites',] }],
    isFetchingFavorites$: [{ type: Input, args: ['isFetchingFavorites$',] }],
    filterChanged: [{ type: Output, args: ['filterChanged',] }],
    deleteFavorite: [{ type: Output, args: ['deleteFavorite',] }],
    saveAsFavorite: [{ type: Output, args: ['saveAsFavorite',] }]
};
if (false) {
    /** @type {?} */
    FilterComponent.prototype.options;
    /** @type {?} */
    FilterComponent.prototype.filter;
    /** @type {?} */
    FilterComponent.prototype.favorites;
    /** @type {?} */
    FilterComponent.prototype.isFetchingFavorites$;
    /** @type {?} */
    FilterComponent.prototype.filterChanged;
    /** @type {?} */
    FilterComponent.prototype.deleteFavorite;
    /** @type {?} */
    FilterComponent.prototype.saveAsFavorite;
    /** @type {?} */
    FilterComponent.prototype.sortedOptions;
    /** @type {?} */
    FilterComponent.prototype.devMode;
    /** @type {?} */
    FilterComponent.prototype.closed;
    /** @type {?} */
    FilterComponent.prototype.selectedTab;
    /** @type {?} */
    FilterComponent.prototype.form;
}
class String {
    /**
     * return true if each terms (space-separated) of $needle
     * are present in $haystack
     * @param {?} needle
     * @param {?} haystack
     * @return {?}
     */
    static search(needle, haystack) {
        // clean strings
        needle = this.replaceAccentedCharacters(needle.toLowerCase());
        haystack = this.replaceAccentedCharacters(haystack.toLowerCase());
        // split string in search terms
        /** @type {?} */
        const terms = needle.split(/\s+/);
        // check each term is present
        for (let term of terms) {
            if (!haystack.includes(term))
                return false;
        }
        return true;
    }
    /**
     * @param {?} str
     * @return {?}
     */
    static replaceAccentedCharacters(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B0ZWxlby9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZmlsdGVyL2ZpbHRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUNuQyxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFhaEQsTUFBTSxPQUFPLGVBQWU7SUEwQjFCO1FBeEJrQixZQUFPLEdBQWtCLElBQUksQ0FBQyxDQUFDLDREQUE0RDtRQUtwRixrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDMUMsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBWSxDQUFDO1FBQzlDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUV0RSxrQkFBYSxHQUFrQixJQUFJLENBQUM7UUFDcEMsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixXQUFNLEdBQVksS0FBSyxDQUFDO1FBQ3hCLGdCQUFXLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLFNBQUksR0FBUTtZQUNWLFNBQVMsRUFBRSxFQUFFO1lBQ2IsT0FBTyxFQUFFLEVBQUU7WUFDWCxXQUFXLEVBQUUsRUFBRTtZQUNmLFFBQVEsRUFBRSxFQUFFO1lBQ1osYUFBYSxFQUFFLEVBQUU7WUFDakIsaUJBQWlCLEVBQUUsRUFBRTtZQUNyQixlQUFlLEVBQUUsRUFBRTtZQUNuQixjQUFjLEVBQUUsRUFBRTtTQUNuQixDQUFBO0lBR0QsQ0FBQzs7OztJQUVELFFBQVE7SUFFUixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFPO1FBQ2pCLElBQUksT0FBTyxDQUFDLE1BQU07WUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QyxJQUFJLE9BQU8sQ0FBQyxPQUFPLEVBQUU7WUFDbkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUN0QjtJQUNILENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztJQUNoRyxDQUFDOzs7OztJQUVELE9BQU8sQ0FBQyxNQUFjO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQUMzQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUE7UUFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBQy9DLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQTtRQUN6QyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7SUFDakIsQ0FBQzs7OztJQUdELFFBQVE7UUFDTixpREFBaUQ7UUFDakQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBQ3JCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtJQUN0QixDQUFDOzs7OztJQUlELGNBQWM7UUFFWixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFNOztZQUVyQixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzs7WUFDOUIsYUFBYSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUM7O1lBRWpDLGtCQUFrQixHQUFnQixhQUFhLENBQUMsY0FBYyxFQUFFLENBQUMsR0FBRzs7OztRQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSTs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUMsRUFBQzs7WUFDMUgsZ0JBQWdCLEdBQWUsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUc7Ozs7UUFBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFDLEVBQUM7O1lBQ25ILGVBQWUsR0FBYyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRzs7OztRQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSTs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUMsRUFBQztRQUVuSCxvQkFBb0I7UUFDcEIsSUFBSSxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsTUFBTSxFQUFFO1lBQ3JDLGFBQWEsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsTUFBTTs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBQyxDQUFDLEdBQUc7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFBO1lBQy9ILGFBQWEsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTTs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBQyxDQUFDLEdBQUc7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFBO1lBQzNILGFBQWEsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLE1BQU07Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQUMsQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQTtTQUMxSDtRQUNELHdCQUF3QjtRQUN4QixJQUFJLGFBQWEsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNLEVBQUU7WUFDekMsYUFBYSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxFQUFDLENBQUMsR0FBRzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUE7WUFDbEksYUFBYSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsTUFBTTs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxhQUFhLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBQyxDQUFDLEdBQUc7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFBO1NBQ2pJO1FBQ0Qsc0JBQXNCO1FBQ3RCLElBQUksYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sRUFBRTtZQUN2QyxhQUFhLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxFQUFDLENBQUMsR0FBRzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUE7U0FDOUg7UUFFRCxnRUFBZ0U7UUFDaEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQTtJQUNuRSxDQUFDOzs7OztJQU1ELGFBQWE7UUFFWCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPOztZQUV0QixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzs7Y0FDNUIsYUFBYSxHQUFHLE1BQU0sQ0FBQyxVQUFVLEVBQUU7O2NBQ25DLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxjQUFjLEVBQUU7O2NBQzNDLGVBQWUsR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFO1FBRTdDLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUU7O2tCQUNyQixnQkFBZ0IsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDOztrQkFDdEosSUFBSSxHQUFHLGdCQUFnQjtZQUM3QixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDO1lBQ2pCLE9BQU8sQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFDLENBQUE7UUFFRiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFOztrQkFDekIsZ0JBQWdCLEdBQUcsQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQzs7a0JBQy9FLGdCQUFnQixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7O2tCQUNsSyxJQUFJLEdBQUcsZ0JBQWdCLElBQUksZ0JBQWdCOztrQkFDM0MsTUFBTSxHQUFHLENBQUMsSUFBSTtZQUNwQixJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssTUFBTTtnQkFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUUzQyxPQUFPLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBQyxDQUFBO1FBRUYsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRTs7a0JBQ3ZCLGdCQUFnQixHQUFHLENBQUMsYUFBYSxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7O2tCQUMvRSxtQkFBbUIsR0FBRyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sSUFBSSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQzs7a0JBQzdGLGdCQUFnQixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7O2tCQUM1SixJQUFJLEdBQUcsZ0JBQWdCLElBQUksbUJBQW1CLElBQUksZ0JBQWdCOztrQkFDbEUsTUFBTSxHQUFHLENBQUMsSUFBSTtZQUNwQixJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssTUFBTTtnQkFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUMzQyxPQUFPLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBQyxDQUFBO1FBRUYseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRTs7a0JBQ3RCLGdCQUFnQixHQUFHLENBQUMsYUFBYSxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7O2tCQUMvRSxtQkFBbUIsR0FBRyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sSUFBSSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQzs7a0JBQzdGLGtCQUFrQixHQUFHLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7O2tCQUN2RixnQkFBZ0IsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDOztrQkFDekosSUFBSSxHQUFHLGdCQUFnQixJQUFJLG1CQUFtQixJQUFJLGtCQUFrQixJQUFJLGdCQUFnQjs7a0JBQ3hGLE1BQU0sR0FBRyxDQUFDLElBQUk7WUFDcEIsSUFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLE1BQU07Z0JBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDM0MsT0FBTyxDQUFDLENBQUM7UUFDWCxDQUFDLEVBQUMsQ0FBQTtRQUdGLDRGQUE0RjtRQUM1Rix3SEFBd0g7UUFDeEgsNEVBQTRFO1FBQzVFLElBQUksQ0FBQyxhQUFhLEdBQUc7WUFDbkIsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7O1lBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUMsV0FBVyxFQUFFLE1BQU0sRUFBQyxDQUFDLEVBQUM7WUFDL0ksV0FBVyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUk7Ozs7O1lBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUMsV0FBVyxFQUFFLE1BQU0sRUFBQyxDQUFDLEVBQUM7WUFDeEosU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUk7Ozs7O1lBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUMsV0FBVyxFQUFFLE1BQU0sRUFBQyxDQUFDLEVBQUM7WUFDcEosUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUk7Ozs7O1lBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUMsV0FBVyxFQUFFLE1BQU0sRUFBQyxDQUFDLEVBQUM7U0FDbkosQ0FBQTtJQUNILENBQUM7Ozs7OztJQUVELFlBQVksQ0FBQyxLQUFhLEVBQUUsT0FBWTtRQUN0QyxPQUFPLE9BQU8sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO0lBQ3BDLENBQUM7Ozs7O0lBSUQsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHOzs7O1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7Ozs7SUFFRCxrQkFBa0I7UUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBRUQsb0JBQW9CO1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUFFLE9BQU87UUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRzs7OztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBRUQsc0JBQXNCO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBRUQsa0JBQWtCO1FBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUFFLE9BQU87UUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRzs7OztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBRUQsb0JBQW9CO1FBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7Ozs7O0lBSUQsY0FBYyxDQUFDLEtBQW9CO1FBQ2pDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMxQixDQUFDOzs7O0lBRUQscUJBQXFCOztjQUNiLGVBQWUsR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLENBQUMsTUFBTTs7Y0FDL0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1FBQ25ELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsWUFBWSxlQUFlLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLENBQUMsWUFBWSxlQUFlLEdBQUcsQ0FBQTtJQUMzRyxDQUFDOzs7O0lBRUQseUJBQXlCOztjQUNqQixlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQyxDQUFDLE1BQU07O2NBQ25FLGdCQUFnQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtRQUN2RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixlQUFlLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLGVBQWUsR0FBRyxDQUFBO0lBQ25ILENBQUM7Ozs7SUFFRCx1QkFBdUI7O2NBQ2YsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNOztjQUNqRSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMscUJBQXFCLEVBQUU7UUFDckQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxhQUFhLGVBQWUsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsQ0FBQyxhQUFhLGVBQWUsR0FBRyxDQUFBO0lBQzdHLENBQUM7Ozs7SUFFRCxtQkFBbUI7UUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDNUIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7O1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUMsQ0FBQyxNQUFNLENBQUE7SUFDakUsQ0FBQzs7OztJQUVELHVCQUF1QjtRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPLENBQUMsQ0FBQztRQUM1QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU07Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxDQUFDLE1BQU0sQ0FBQTtJQUNyRSxDQUFDOzs7O0lBRUQscUJBQXFCO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzVCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTTs7OztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFDLENBQUMsTUFBTSxDQUFBO0lBQ25FLENBQUM7Ozs7OztJQUdELGdCQUFnQixDQUFDLEtBQUs7UUFDcEIsc0JBQXNCO1FBQ3RCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUU7WUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQzs7OztJQUVELE1BQU07O2NBQ0UsV0FBVyxHQUFHLElBQUksTUFBTSxFQUFFO1FBRWhDLHNCQUFzQjtRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUE7UUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUE7UUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFBO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQTtRQUU3QixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0lBQzNCLENBQUM7Ozs7SUFFRCxlQUFlOztZQUNULE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLFFBQWtCO1FBQ2hDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBRXBDLENBQUM7Ozs7SUFHRCxXQUFXOztZQUNMLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLFFBQWtCO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDOzs7O0lBR0QsYUFBYTtRQUNYLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFHRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDckIsQ0FBQzs7OztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7WUFoVEYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4Qiw0cExBQXNDOzthQUV2Qzs7Ozs7c0JBR0UsS0FBSyxTQUFDLFNBQVM7cUJBQ2YsS0FBSyxTQUFDLFFBQVE7d0JBQ2QsS0FBSyxTQUFDLFdBQVc7bUNBQ2pCLEtBQUssU0FBQyxzQkFBc0I7NEJBRTVCLE1BQU0sU0FBQyxlQUFlOzZCQUN0QixNQUFNLFNBQUMsZ0JBQWdCOzZCQUN2QixNQUFNLFNBQUMsZ0JBQWdCOzs7O0lBUHhCLGtDQUFnRDs7SUFDaEQsaUNBQWdDOztJQUNoQyxvQ0FBK0M7O0lBQy9DLCtDQUF5RTs7SUFFekUsd0NBQW9FOztJQUNwRSx5Q0FBd0U7O0lBQ3hFLHlDQUFzRTs7SUFFdEUsd0NBQW9DOztJQUNwQyxrQ0FBZ0I7O0lBQ2hCLGlDQUF3Qjs7SUFDeEIsc0NBQWdCOztJQUNoQiwrQkFTQzs7QUF1UkgsTUFBTSxNQUFNOzs7Ozs7OztJQU9WLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBYyxFQUFFLFFBQWdCO1FBQzFDLGdCQUFnQjtRQUNoQixNQUFNLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFBO1FBQzdELFFBQVEsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7OztjQUU1RCxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDakMsNkJBQTZCO1FBQzdCLEtBQUssSUFBSSxJQUFJLElBQUksS0FBSyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFBRSxPQUFPLEtBQUssQ0FBQztTQUM5QztRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUc7UUFDaEMsT0FBTyxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUMsQ0FBQTtJQUMvRCxDQUFDO0NBRUYiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjbG9uZURlZXAgfSBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHsgRmlsdGVyIH0gZnJvbSAnLi4vbW9kZWxzL2ZpbHRlci5tb2RlbCc7XG5pbXBvcnQgeyBGaWx0ZXJPcHRpb25zIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9maWx0ZXItb3B0aW9ucy5pbnRlcmZhY2UnO1xuaW1wb3J0IHsgVGVycml0b3J5IH0gZnJvbSAnLi4vaW50ZXJmYWNlcy90ZXJyaXRvcnkuaW50ZXJmYWNlJztcbmltcG9ydCB7IENvbnRyYWN0IH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jb250cmFjdC5pbnRlcmZhY2UnO1xuaW1wb3J0IHsgQ29tbXVuZSB9IGZyb20gJy4uL2ludGVyZmFjZXMvY29tbXVuZS5pbnRlcmZhY2UnO1xuaW1wb3J0IHsgRmF2b3JpdGUgfSBmcm9tICcuLi9tb2RlbHMvZmF2b3JpdGUubW9kZWwnO1xuaW1wb3J0IHsgSW5kZXggfSBmcm9tICcuLi9pbnRlcmZhY2VzL2luZGV4LmludGVyZmFjZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3RlbGVvLWZpbHRlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9maWx0ZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9maWx0ZXIuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgRmlsdGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoJ29wdGlvbnMnKSBvcHRpb25zOiBGaWx0ZXJPcHRpb25zID0gbnVsbDsgLy8gZG9uJ3QgbW9kaWZ5IHRoaXMgb2JqZWN0LCBleGNlcHQgdGhlIFwiLmhpZGRlblwiIHByb3BlcnRpZXNcbiAgQElucHV0KCdmaWx0ZXInKSBmaWx0ZXI6IEZpbHRlcjtcbiAgQElucHV0KCdmYXZvcml0ZXMnKSBmYXZvcml0ZXM6IEluZGV4PEZhdm9yaXRlPjtcbiAgQElucHV0KCdpc0ZldGNoaW5nRmF2b3JpdGVzJCcpIGlzRmV0Y2hpbmdGYXZvcml0ZXMkOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuXG4gIEBPdXRwdXQoJ2ZpbHRlckNoYW5nZWQnKSBmaWx0ZXJDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxGaWx0ZXI+KCk7XG4gIEBPdXRwdXQoJ2RlbGV0ZUZhdm9yaXRlJykgZGVsZXRlRmF2b3JpdGUgPSBuZXcgRXZlbnRFbWl0dGVyPEZhdm9yaXRlPigpO1xuICBAT3V0cHV0KCdzYXZlQXNGYXZvcml0ZScpIHNhdmVBc0Zhdm9yaXRlID0gbmV3IEV2ZW50RW1pdHRlcjxGaWx0ZXI+KCk7XG5cbiAgc29ydGVkT3B0aW9uczogRmlsdGVyT3B0aW9ucyA9IG51bGw7XG4gIGRldk1vZGUgPSBmYWxzZTtcbiAgY2xvc2VkOiBib29sZWFuID0gZmFsc2U7XG4gIHNlbGVjdGVkVGFiID0gMDtcbiAgZm9ybTogYW55ID0ge1xuICAgIGNvbnRyYWN0czogW10sXG4gICAgcmVnaW9uczogW10sXG4gICAgdGVycml0b3JpZXM6IFtdLFxuICAgIGNvbW11bmVzOiBbXSxcbiAgICBzZWFyY2hSZWdpb25zOiAnJyxcbiAgICBzZWFyY2hUZXJyaXRvcmllczogJycsXG4gICAgc2VhcmNoQ29udHJhY3RzOiAnJyxcbiAgICBzZWFyY2hDb21tdW5lczogJycsXG4gIH1cblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzKSB7XG4gICAgaWYgKGNoYW5nZXMuZmlsdGVyKSB0aGlzLnNldEZvcm0odGhpcy5maWx0ZXIpO1xuICAgIGlmIChjaGFuZ2VzLm9wdGlvbnMpIHtcbiAgICAgIHRoaXMub3B0aW9ucyA9IGNsb25lRGVlcCh0aGlzLm9wdGlvbnMpO1xuICAgICAgdGhpcy51cGRhdGVPcHRpb25zKCk7XG4gICAgfVxuICB9XG5cbiAgaXNTdWJtaXR0YWJsZSgpIHtcbiAgICByZXR1cm4gdGhpcy5mb3JtLnJlZ2lvbnMubGVuZ3RoIHx8IHRoaXMuZm9ybS50ZXJyaXRvcmllcy5sZW5ndGggfHwgdGhpcy5mb3JtLmNvbnRyYWN0cy5sZW5ndGg7XG4gIH1cblxuICBzZXRGb3JtKGZpbHRlcjogRmlsdGVyKSB7XG4gICAgdGhpcy5mb3JtLmNvbnRyYWN0cyA9IGZpbHRlci5nZXRDb250cmFjdHMoKVxuICAgIHRoaXMuZm9ybS5yZWdpb25zID0gZmlsdGVyLmdldFJlZ2lvbnMoKVxuICAgIHRoaXMuZm9ybS50ZXJyaXRvcmllcyA9IGZpbHRlci5nZXRUZXJyaXRvcmllcygpXG4gICAgdGhpcy5mb3JtLmNvbW11bmVzID0gZmlsdGVyLmdldENvbW11bmVzKClcbiAgICB0aGlzLm9uQ2hhbmdlKClcbiAgfVxuXG5cbiAgb25DaGFuZ2UoKSB7XG4gICAgLy8gY2xlYW4gdXNlciBzZWxlY3Rpb24gKHJlbW92ZSBpbXBvc3NpYmxlIGNhc2VzKVxuICAgIHRoaXMuY2xlYW5TZWxlY3Rpb24oKVxuICAgIHRoaXMudXBkYXRlT3B0aW9ucygpXG4gIH1cblxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gIGNsZWFuU2VsZWN0aW9uKCk6IHZvaWQge1xuXG4gICAgaWYgKCF0aGlzLm9wdGlvbnMpIHJldHVyblxuXG4gICAgbGV0IGZpbHRlciA9IG5ldyBGaWx0ZXIodGhpcy5mb3JtKVxuICAgIGxldCBjbGVhbmVkRmlsdGVyID0gY2xvbmVEZWVwKGZpbHRlcilcblxuICAgIGxldCBjdXJyZW50VGVycml0b3JpZXM6IFRlcnJpdG9yeVtdID0gY2xlYW5lZEZpbHRlci5nZXRUZXJyaXRvcmllcygpLm1hcChpZCA9PiB0aGlzLm9wdGlvbnMudGVycml0b3JpZXMuZmluZCh0ID0+IHQuaWQgPT0gaWQpKTtcbiAgICBsZXQgY3VycmVudENvbnRyYWN0czogQ29udHJhY3RbXSA9IGNsZWFuZWRGaWx0ZXIuZ2V0Q29udHJhY3RzKCkubWFwKGlkID0+IHRoaXMub3B0aW9ucy5jb250cmFjdHMuZmluZChjID0+IGMuaWQgPT0gaWQpKTtcbiAgICBsZXQgY3VycmVudENvbW11bmVzOiBDb21tdW5lW10gPSBjbGVhbmVkRmlsdGVyLmdldENvbW11bmVzKCkubWFwKGlkID0+IHRoaXMub3B0aW9ucy5jb21tdW5lcy5maW5kKGMgPT4gYy5pZCA9PSBpZCkpO1xuXG4gICAgLy8gZmlsdGVyIGJ5IHJlZ2lvbnNcbiAgICBpZiAoY2xlYW5lZEZpbHRlci5nZXRSZWdpb25zKCkubGVuZ3RoKSB7XG4gICAgICBjbGVhbmVkRmlsdGVyLnNldFRlcnJpdG9yaWVzKGN1cnJlbnRUZXJyaXRvcmllcy5maWx0ZXIoKHQpID0+IGNsZWFuZWRGaWx0ZXIuZ2V0UmVnaW9ucygpLmluY2x1ZGVzKHQucmVnaW9uX2lkKSkubWFwKHQgPT4gdC5pZCkpXG4gICAgICBjbGVhbmVkRmlsdGVyLnNldENvbnRyYWN0cyhjdXJyZW50Q29udHJhY3RzLmZpbHRlcigoYykgPT4gY2xlYW5lZEZpbHRlci5nZXRSZWdpb25zKCkuaW5jbHVkZXMoYy5yZWdpb25faWQpKS5tYXAoYyA9PiBjLmlkKSlcbiAgICAgIGNsZWFuZWRGaWx0ZXIuc2V0Q29tbXVuZXMoY3VycmVudENvbW11bmVzLmZpbHRlcigoYykgPT4gY2xlYW5lZEZpbHRlci5nZXRSZWdpb25zKCkuaW5jbHVkZXMoYy5yZWdpb25faWQpKS5tYXAoYyA9PiBjLmlkKSlcbiAgICB9XG4gICAgLy8gZmlsdGVyIGJ5IHRlcnJpdG9yaWVzXG4gICAgaWYgKGNsZWFuZWRGaWx0ZXIuZ2V0VGVycml0b3JpZXMoKS5sZW5ndGgpIHtcbiAgICAgIGNsZWFuZWRGaWx0ZXIuc2V0Q29udHJhY3RzKGN1cnJlbnRDb250cmFjdHMuZmlsdGVyKChjKSA9PiBjbGVhbmVkRmlsdGVyLmdldFRlcnJpdG9yaWVzKCkuaW5jbHVkZXMoYy50ZXJyaXRvcnlfaWQpKS5tYXAoYyA9PiBjLmlkKSlcbiAgICAgIGNsZWFuZWRGaWx0ZXIuc2V0Q29tbXVuZXMoY3VycmVudENvbW11bmVzLmZpbHRlcigoYykgPT4gY2xlYW5lZEZpbHRlci5nZXRUZXJyaXRvcmllcygpLmluY2x1ZGVzKGMudGVycml0b3J5X2lkKSkubWFwKGMgPT4gYy5pZCkpXG4gICAgfVxuICAgIC8vIGZpbHRlciBieSBjb250cmFjdHNcbiAgICBpZiAoY2xlYW5lZEZpbHRlci5nZXRDb250cmFjdHMoKS5sZW5ndGgpIHtcbiAgICAgIGNsZWFuZWRGaWx0ZXIuc2V0Q29tbXVuZXMoY3VycmVudENvbW11bmVzLmZpbHRlcigoYykgPT4gY2xlYW5lZEZpbHRlci5nZXRDb250cmFjdHMoKS5pbmNsdWRlcyhjLmNvbnRyYWN0X2lkKSkubWFwKGMgPT4gYy5pZCkpXG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIGZvcm0gaWYgdGhlIGNsZWFuZWQgdmVyc2lvbiBvZiB0aGUgZmlsdGVyIGlzIGRpZmZlcmVudFxuICAgIGlmICghY2xlYW5lZEZpbHRlci5pc0VxdWFsVG8oZmlsdGVyKSkgdGhpcy5zZXRGb3JtKGNsZWFuZWRGaWx0ZXIpXG4gIH1cblxuXG5cbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuICB1cGRhdGVPcHRpb25zKCkge1xuXG4gICAgaWYgKCF0aGlzLm9wdGlvbnMpIHJldHVybjtcblxuICAgIGxldCBmaWx0ZXIgPSBuZXcgRmlsdGVyKHRoaXMuZm9ybSk7XG4gICAgY29uc3QgZmlsdGVyUmVnaW9ucyA9IGZpbHRlci5nZXRSZWdpb25zKCk7XG4gICAgY29uc3QgZmlsdGVyVGVycml0b3JpZXMgPSBmaWx0ZXIuZ2V0VGVycml0b3JpZXMoKTtcbiAgICBjb25zdCBmaWx0ZXJDb250cmFjdHMgPSBmaWx0ZXIuZ2V0Q29udHJhY3RzKCk7XG5cbiAgICAvLyBmaWx0ZXIgcmVnaW9uIG9wdGlvbnNcbiAgICB0aGlzLm9wdGlvbnMucmVnaW9ucy5tYXAociA9PiB7XG4gICAgICBjb25zdCBwYXNzU2VhcmNoRmlsdGVyID0gIXRoaXMuZm9ybS5zZWFyY2hSZWdpb25zLmxlbmd0aCB8fCAoU3RyaW5nLnNlYXJjaCh0aGlzLmZvcm0uc2VhcmNoUmVnaW9ucywgci5uYW1lKSB8fCBTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hSZWdpb25zLCByLmlkKSlcbiAgICAgIGNvbnN0IHBhc3MgPSBwYXNzU2VhcmNoRmlsdGVyO1xuICAgICAgci5oaWRkZW4gPSAhcGFzcztcbiAgICAgIHJldHVybiByO1xuICAgIH0pXG5cbiAgICAvLyBmaWx0ZXIgdGVycml0b3J5IG9wdGlvbnNcbiAgICB0aGlzLm9wdGlvbnMudGVycml0b3JpZXMubWFwKHQgPT4ge1xuICAgICAgY29uc3QgcGFzc1JlZ2lvbkZpbHRlciA9ICFmaWx0ZXJSZWdpb25zLmxlbmd0aCB8fCBmaWx0ZXJSZWdpb25zLmluY2x1ZGVzKHQucmVnaW9uX2lkKVxuICAgICAgY29uc3QgcGFzc1NlYXJjaEZpbHRlciA9ICF0aGlzLmZvcm0uc2VhcmNoVGVycml0b3JpZXMubGVuZ3RoIHx8IChTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hUZXJyaXRvcmllcywgdC5uYW1lKSB8fCBTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hUZXJyaXRvcmllcywgdC5pZCkpXG4gICAgICBjb25zdCBwYXNzID0gcGFzc1JlZ2lvbkZpbHRlciAmJiBwYXNzU2VhcmNoRmlsdGVyO1xuICAgICAgY29uc3QgaGlkZGVuID0gIXBhc3M7XG4gICAgICBpZiAodC5oaWRkZW4gIT09IGhpZGRlbikgdC5oaWRkZW4gPSBoaWRkZW47XG5cbiAgICAgIHJldHVybiB0O1xuICAgIH0pXG5cbiAgICAvLyBmaWx0ZXIgY29udHJhY3Qgb3B0aW9uc1xuICAgIHRoaXMub3B0aW9ucy5jb250cmFjdHMubWFwKGMgPT4ge1xuICAgICAgY29uc3QgcGFzc1JlZ2lvbkZpbHRlciA9ICFmaWx0ZXJSZWdpb25zLmxlbmd0aCB8fCBmaWx0ZXJSZWdpb25zLmluY2x1ZGVzKGMucmVnaW9uX2lkKVxuICAgICAgY29uc3QgcGFzc1RlcnJpdG9yeUZpbHRlciA9ICFmaWx0ZXJUZXJyaXRvcmllcy5sZW5ndGggfHwgZmlsdGVyVGVycml0b3JpZXMuaW5jbHVkZXMoYy50ZXJyaXRvcnlfaWQpXG4gICAgICBjb25zdCBwYXNzU2VhcmNoRmlsdGVyID0gIXRoaXMuZm9ybS5zZWFyY2hDb250cmFjdHMubGVuZ3RoIHx8IChTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hDb250cmFjdHMsIGMubmFtZSkgfHwgU3RyaW5nLnNlYXJjaCh0aGlzLmZvcm0uc2VhcmNoQ29udHJhY3RzLCBjLmlkKSlcbiAgICAgIGNvbnN0IHBhc3MgPSBwYXNzUmVnaW9uRmlsdGVyICYmIHBhc3NUZXJyaXRvcnlGaWx0ZXIgJiYgcGFzc1NlYXJjaEZpbHRlcjtcbiAgICAgIGNvbnN0IGhpZGRlbiA9ICFwYXNzO1xuICAgICAgaWYgKGMuaGlkZGVuICE9PSBoaWRkZW4pIGMuaGlkZGVuID0gaGlkZGVuO1xuICAgICAgcmV0dXJuIGM7XG4gICAgfSlcblxuICAgIC8vIGZpbHRlciBjb21tdW5lIG9wdGlvbnNcbiAgICB0aGlzLm9wdGlvbnMuY29tbXVuZXMubWFwKGMgPT4ge1xuICAgICAgY29uc3QgcGFzc1JlZ2lvbkZpbHRlciA9ICFmaWx0ZXJSZWdpb25zLmxlbmd0aCB8fCBmaWx0ZXJSZWdpb25zLmluY2x1ZGVzKGMucmVnaW9uX2lkKVxuICAgICAgY29uc3QgcGFzc1RlcnJpdG9yeUZpbHRlciA9ICFmaWx0ZXJUZXJyaXRvcmllcy5sZW5ndGggfHwgZmlsdGVyVGVycml0b3JpZXMuaW5jbHVkZXMoYy50ZXJyaXRvcnlfaWQpXG4gICAgICBjb25zdCBwYXNzQ29udHJhY3RGaWx0ZXIgPSAhZmlsdGVyQ29udHJhY3RzLmxlbmd0aCB8fCBmaWx0ZXJDb250cmFjdHMuaW5jbHVkZXMoYy5jb250cmFjdF9pZClcbiAgICAgIGNvbnN0IHBhc3NTZWFyY2hGaWx0ZXIgPSAhdGhpcy5mb3JtLnNlYXJjaENvbW11bmVzLmxlbmd0aCB8fCAoU3RyaW5nLnNlYXJjaCh0aGlzLmZvcm0uc2VhcmNoQ29tbXVuZXMsIGMubmFtZSkgfHwgU3RyaW5nLnNlYXJjaCh0aGlzLmZvcm0uc2VhcmNoQ29tbXVuZXMsIGMuaWQpKVxuICAgICAgY29uc3QgcGFzcyA9IHBhc3NSZWdpb25GaWx0ZXIgJiYgcGFzc1RlcnJpdG9yeUZpbHRlciAmJiBwYXNzQ29udHJhY3RGaWx0ZXIgJiYgcGFzc1NlYXJjaEZpbHRlcjtcbiAgICAgIGNvbnN0IGhpZGRlbiA9ICFwYXNzO1xuICAgICAgaWYgKGMuaGlkZGVuICE9PSBoaWRkZW4pIGMuaGlkZGVuID0gaGlkZGVuO1xuICAgICAgcmV0dXJuIGM7XG4gICAgfSlcblxuICAgIFxuICAgIC8vIEJlbG93LCB3ZSBwdXQgaGlkZGVuIG9wdGlvbnMgYXQgdGhlIGVuZCwgc28gdGhhdCBtYXQtc2VsZWN0J3Mgc2Nyb2xsaW5nIGJlaGF2ZXMgY29ycmVjdGx5XG4gICAgLy8gMS4gdGhpcyB3YXkgd2UgYWxzbyBjYW4gdXNlIFwidmlzaWJpbGl0eVwiIGNzcyBwcm9wZXJ0eSB3aGljaCBpcyBmYXN0ZXIgdGhhbiAqbmdJZiB3aGljaCB3b3VsZCBpbnNlcnQvcmVtb3ZlcyBET00gbm9kZXNcbiAgICAvLyAyLiB0aGlzIHdheSwgdXNpbmcgdGhlIHNlYXJjaCBmaWVsZCBkb2Vzbid0IHJlbW92ZSBvdXIgcHJldmlvdXMgc2VsZWN0aW9uXG4gICAgdGhpcy5zb3J0ZWRPcHRpb25zID0ge1xuICAgICAgcmVnaW9uczogdGhpcy5vcHRpb25zLnJlZ2lvbnMuc29ydCgoYSxiKSA9PiAoTnVtYmVyKGEuaGlkZGVuKSAtIE51bWJlcihiLmhpZGRlbikpIHx8IGEubmFtZS5sb2NhbGVDb21wYXJlKGIubmFtZSwgJ2ZyJywge3NlbnNpdGl2aXR5OiBcImJhc2VcIn0pKSxcbiAgICAgIHRlcnJpdG9yaWVzOiB0aGlzLm9wdGlvbnMudGVycml0b3JpZXMuc29ydCgoYSxiKSA9PiAoTnVtYmVyKGEuaGlkZGVuKSAtIE51bWJlcihiLmhpZGRlbikpIHx8ICBhLm5hbWUubG9jYWxlQ29tcGFyZShiLm5hbWUsICdmcicsIHtzZW5zaXRpdml0eTogXCJiYXNlXCJ9KSksXG4gICAgICBjb250cmFjdHM6IHRoaXMub3B0aW9ucy5jb250cmFjdHMuc29ydCgoYSxiKSA9PiAoTnVtYmVyKGEuaGlkZGVuKSAtIE51bWJlcihiLmhpZGRlbikpIHx8ICBhLm5hbWUubG9jYWxlQ29tcGFyZShiLm5hbWUsICdmcicsIHtzZW5zaXRpdml0eTogXCJiYXNlXCJ9KSksXG4gICAgICBjb21tdW5lczogdGhpcy5vcHRpb25zLmNvbW11bmVzLnNvcnQoKGEsYikgPT4gKE51bWJlcihhLmhpZGRlbikgLSBOdW1iZXIoYi5oaWRkZW4pKSB8fCAgYS5uYW1lLmxvY2FsZUNvbXBhcmUoYi5uYW1lLCAnZnInLCB7c2Vuc2l0aXZpdHk6IFwiYmFzZVwifSkpLFxuICAgIH1cbiAgfVxuXG4gIHRyYWNrRWxlbWVudChpbmRleDogbnVtYmVyLCBlbGVtZW50OiBhbnkpIHtcbiAgICByZXR1cm4gZWxlbWVudCA/IGVsZW1lbnQuaWQgOiBudWxsXG4gIH1cblxuICAvLyBTRUxFQ1QgLyBERVNFTEVDVCAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gIHNlbGVjdEFsbFJlZ2lvbnMoKSB7XG4gICAgaWYgKCF0aGlzLm9wdGlvbnMpIHJldHVybjtcbiAgICB0aGlzLmZvcm0ucmVnaW9ucyA9IHRoaXMub3B0aW9ucy5yZWdpb25zLm1hcChpdGVtID0+IGl0ZW0uaWQpO1xuICAgIHRoaXMub25DaGFuZ2UoKTtcbiAgfVxuXG4gIGRlc2VsZWN0QWxsUmVnaW9ucygpIHtcbiAgICB0aGlzLmZvcm0ucmVnaW9ucyA9IFtdO1xuICAgIHRoaXMuZm9ybS50ZXJyaXRvcmllcyA9IFtdO1xuICAgIHRoaXMuZm9ybS5jb250cmFjdHMgPSBbXTtcbiAgICB0aGlzLmZvcm0uY29tbXVuZXMgPSBbXTtcbiAgICB0aGlzLm9uQ2hhbmdlKCk7XG4gIH1cblxuICBzZWxlY3RBbGxUZXJyaXRvcmllcygpIHtcbiAgICBpZiAoIXRoaXMub3B0aW9ucykgcmV0dXJuO1xuICAgIHRoaXMuZm9ybS50ZXJyaXRvcmllcyA9IHRoaXMub3B0aW9ucy50ZXJyaXRvcmllcy5tYXAoaXRlbSA9PiBpdGVtLmlkKTtcbiAgICB0aGlzLm9uQ2hhbmdlKCk7XG4gIH1cblxuICBkZXNlbGVjdEFsbFRlcnJpdG9yaWVzKCkge1xuICAgIHRoaXMuZm9ybS50ZXJyaXRvcmllcyA9IFtdO1xuICAgIHRoaXMuZm9ybS5jb250cmFjdHMgPSBbXTtcbiAgICB0aGlzLmZvcm0uY29tbXVuZXMgPSBbXTtcbiAgICB0aGlzLm9uQ2hhbmdlKCk7XG4gIH1cblxuICBzZWxlY3RBbGxDb250cmFjdHMoKSB7XG4gICAgaWYgKCF0aGlzLm9wdGlvbnMpIHJldHVybjtcbiAgICB0aGlzLmZvcm0uY29udHJhY3RzID0gdGhpcy5vcHRpb25zLmNvbnRyYWN0cy5tYXAoaXRlbSA9PiBpdGVtLmlkKTtcbiAgICB0aGlzLm9uQ2hhbmdlKCk7XG4gIH1cblxuICBkZXNlbGVjdEFsbENvbnRyYWN0cygpIHtcbiAgICB0aGlzLmZvcm0uY29udHJhY3RzID0gW107XG4gICAgdGhpcy5mb3JtLmNvbW11bmVzID0gW107XG4gICAgdGhpcy5vbkNoYW5nZSgpO1xuICB9XG5cblxuXG4gIF9oYW5kbGVLZXlkb3duKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gIH1cblxuICBnZXRSZWdpb25zUGxhY2Vob2xkZXIoKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPcHRpb25zID0gdGhpcy5mb3JtICYmICh0aGlzLmZvcm0ucmVnaW9ucyB8fCBbXSkubGVuZ3RoXG4gICAgY29uc3QgYXZhaWxhYmxlT3B0aW9ucyA9IHRoaXMuY291bnRSZWdpb25zT3B0aW9ucygpO1xuICAgIHJldHVybiB0aGlzLmRldk1vZGUgPyBgUsOpZ2lvbnMgKCR7c2VsZWN0ZWRPcHRpb25zfS8ke2F2YWlsYWJsZU9wdGlvbnN9KWAgOiBgUsOpZ2lvbnMgKCR7c2VsZWN0ZWRPcHRpb25zfSlgXG4gIH1cblxuICBnZXRUZXJyaXRvcmllc1BsYWNlaG9sZGVyKCkge1xuICAgIGNvbnN0IHNlbGVjdGVkT3B0aW9ucyA9IHRoaXMuZm9ybSAmJiAodGhpcy5mb3JtLnRlcnJpdG9yaWVzIHx8IFtdKS5sZW5ndGhcbiAgICBjb25zdCBhdmFpbGFibGVPcHRpb25zID0gdGhpcy5jb3VudFRlcnJpdG9yaWVzT3B0aW9ucygpO1xuICAgIHJldHVybiB0aGlzLmRldk1vZGUgPyBgVGVycml0b2lyZXMgKCR7c2VsZWN0ZWRPcHRpb25zfS8ke2F2YWlsYWJsZU9wdGlvbnN9KWAgOiBgVGVycml0b2lyZXMgKCR7c2VsZWN0ZWRPcHRpb25zfSlgXG4gIH1cblxuICBnZXRDb250cmFjdHNQbGFjZWhvbGRlcigpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9wdGlvbnMgPSB0aGlzLmZvcm0gJiYgKHRoaXMuZm9ybS5jb250cmFjdHMgfHwgW10pLmxlbmd0aFxuICAgIGNvbnN0IGF2YWlsYWJsZU9wdGlvbnMgPSB0aGlzLmNvdW50Q29udHJhY3RzT3B0aW9ucygpO1xuICAgIHJldHVybiB0aGlzLmRldk1vZGUgPyBgQ29udHJhdHMgKCR7c2VsZWN0ZWRPcHRpb25zfS8ke2F2YWlsYWJsZU9wdGlvbnN9KWAgOiBgQ29udHJhdHMgKCR7c2VsZWN0ZWRPcHRpb25zfSlgXG4gIH1cblxuICBjb3VudFJlZ2lvbnNPcHRpb25zKCkge1xuICAgIGlmICghdGhpcy5vcHRpb25zKSByZXR1cm4gMDtcbiAgICByZXR1cm4gdGhpcy5vcHRpb25zLnJlZ2lvbnMuZmlsdGVyKGl0ZW0gPT4gIWl0ZW0uaGlkZGVuKS5sZW5ndGhcbiAgfVxuXG4gIGNvdW50VGVycml0b3JpZXNPcHRpb25zKCkge1xuICAgIGlmICghdGhpcy5vcHRpb25zKSByZXR1cm4gMDtcbiAgICByZXR1cm4gdGhpcy5vcHRpb25zLnRlcnJpdG9yaWVzLmZpbHRlcihpdGVtID0+ICFpdGVtLmhpZGRlbikubGVuZ3RoXG4gIH1cblxuICBjb3VudENvbnRyYWN0c09wdGlvbnMoKSB7XG4gICAgaWYgKCF0aGlzLm9wdGlvbnMpIHJldHVybiAwO1xuICAgIHJldHVybiB0aGlzLm9wdGlvbnMuY29udHJhY3RzLmZpbHRlcihpdGVtID0+ICFpdGVtLmhpZGRlbikubGVuZ3RoXG4gIH1cbiAgLy8gQVBQTFkgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gIGVtcHR5U2VhcmNoRmllbGQoZmllbGQpIHtcbiAgICAvLyBlbXB0eSBzZWFyY2ggZmllbGRzXG4gICAgaWYgKHRoaXMuZm9ybVtmaWVsZF0ubGVuZ3RoKSB7XG4gICAgICB0aGlzLmZvcm1bZmllbGRdID0gJyc7XG4gICAgICB0aGlzLnVwZGF0ZU9wdGlvbnMoKTtcbiAgICB9XG4gIH1cblxuICByZWluaXQoKSB7XG4gICAgY29uc3QgZW1wdHlGaWx0ZXIgPSBuZXcgRmlsdGVyKCk7XG5cbiAgICAvLyBlbXB0eSBzZWFyY2ggZmllbGRzXG4gICAgdGhpcy5mb3JtLnNlYXJjaFJlZ2lvbnMgPSAnJ1xuICAgIHRoaXMuZm9ybS5zZWFyY2hUZXJyaXRvcmllcyA9ICcnXG4gICAgdGhpcy5mb3JtLnNlYXJjaENvbnRyYWN0cyA9ICcnXG4gICAgdGhpcy5mb3JtLnNlYXJjaENvbW11bmVzID0gJydcblxuICAgIHRoaXMuc2V0Rm9ybShlbXB0eUZpbHRlcilcbiAgfVxuXG4gIF9zYXZlQXNGYXZvcml0ZSgpIHtcbiAgICBsZXQgZmlsdGVyID0gbmV3IEZpbHRlcih0aGlzLmZvcm0pO1xuICAgIHRoaXMuc2F2ZUFzRmF2b3JpdGUuZW1pdChmaWx0ZXIpO1xuICB9XG5cbiAgX2RlbGV0ZUZhdm9yaXRlKGZhdm9yaXRlOiBGYXZvcml0ZSkge1xuICAgIHRoaXMuZGVsZXRlRmF2b3JpdGUuZW1pdChmYXZvcml0ZSlcblxuICB9XG5cblxuICBhcHBseUZpbHRlcigpIHtcbiAgICBsZXQgZmlsdGVyID0gbmV3IEZpbHRlcih0aGlzLmZvcm0pO1xuICAgIHRoaXMuZmlsdGVyQ2hhbmdlZC5lbWl0KGZpbHRlcik7XG4gIH1cblxuICBhcHBseUZhdm9yaXRlKGZhdm9yaXRlOiBGYXZvcml0ZSkge1xuICAgIHRoaXMuc2V0Rm9ybShmYXZvcml0ZS5nZXRGaWx0ZXIoKSk7XG4gICAgdGhpcy5hcHBseUZpbHRlcigpO1xuICAgIHRoaXMuZ29Ub0ZpbHRlclRhYigpO1xuICB9XG5cblxuICBnb1RvRmlsdGVyVGFiKCkge1xuICAgIHRoaXMuc2VsZWN0ZWRUYWIgPSAwO1xuICB9XG5cblxuICBjbG9zZU5hdigpIHtcbiAgICB0aGlzLmNsb3NlZCA9IHRydWU7XG4gIH1cblxuICBvcGVuTmF2KCkge1xuICAgIHRoaXMuY2xvc2VkID0gZmFsc2U7XG4gIH1cblxufVxuXG5jbGFzcyBTdHJpbmcge1xuXG5cbiAgLyoqXG4gICAqIHJldHVybiB0cnVlIGlmIGVhY2ggdGVybXMgKHNwYWNlLXNlcGFyYXRlZCkgb2YgJG5lZWRsZVxuICAgKiBhcmUgcHJlc2VudCBpbiAkaGF5c3RhY2tcbiAgICovXG4gIHN0YXRpYyBzZWFyY2gobmVlZGxlOiBzdHJpbmcsIGhheXN0YWNrOiBzdHJpbmcpIHtcbiAgICAgIC8vIGNsZWFuIHN0cmluZ3NcbiAgICAgIG5lZWRsZSA9IHRoaXMucmVwbGFjZUFjY2VudGVkQ2hhcmFjdGVycyhuZWVkbGUudG9Mb3dlckNhc2UoKSlcbiAgICAgIGhheXN0YWNrID0gdGhpcy5yZXBsYWNlQWNjZW50ZWRDaGFyYWN0ZXJzKGhheXN0YWNrLnRvTG93ZXJDYXNlKCkpO1xuICAgICAgLy8gc3BsaXQgc3RyaW5nIGluIHNlYXJjaCB0ZXJtc1xuICAgICAgY29uc3QgdGVybXMgPSBuZWVkbGUuc3BsaXQoL1xccysvKTtcbiAgICAgIC8vIGNoZWNrIGVhY2ggdGVybSBpcyBwcmVzZW50XG4gICAgICBmb3IgKGxldCB0ZXJtIG9mIHRlcm1zKSB7XG4gICAgICAgICAgaWYgKCFoYXlzdGFjay5pbmNsdWRlcyh0ZXJtKSkgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBzdGF0aWMgcmVwbGFjZUFjY2VudGVkQ2hhcmFjdGVycyhzdHIpIHtcbiAgICAgIHJldHVybiBzdHIubm9ybWFsaXplKFwiTkZEXCIpLnJlcGxhY2UoL1tcXHUwMzAwLVxcdTAzNmZdL2csIFwiXCIpXG4gIH1cblxufSJdfQ==