/**
 * @fileoverview added by tsickle
 * Generated from: lib/models/favorite.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Filter } from './filter.model';
export class Favorite {
    /**
     * @param {?=} data
     */
    constructor(data = {}) {
        this.setId(data.id);
        this.setFilter(data.filter || new Filter());
        this.setLabel(data.label || '');
    }
    // GETTERS -------------------------------------------
    /**
     * @return {?}
     */
    getId() {
        return this._id;
    }
    /**
     * @return {?}
     */
    getFilter() {
        return this._filter || new Filter();
    }
    /**
     * @return {?}
     */
    getLabel() {
        return this._label || '';
    }
    // SETTERS -------------------------------------------
    /**
     * @param {?} id
     * @return {?}
     */
    setId(id) {
        this._id = id;
        return this;
    }
    /**
     * @param {?} filter
     * @return {?}
     */
    setFilter(filter) {
        this._filter = filter;
        return this;
    }
    /**
     * @param {?} label
     * @return {?}
     */
    setLabel(label) {
        this._label = label;
        return this;
    }
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJSON(json) {
        json.filter = Filter.fromJSON(json.filter);
        return new Favorite(json);
    }
    /**
     * @return {?}
     */
    toObject() {
        /** @type {?} */
        let o = {
            label: this.getLabel(),
            filter: this.getFilter(),
        };
        if (this.getId())
            o.id = this.getId();
        return o;
    }
    /**
     * @return {?}
     */
    toJSON() {
        return JSON.stringify(this.toObject());
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._id;
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._filter;
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._label;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AdGVsZW8vY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9mYXZvcml0ZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUV4QyxNQUFNLE9BQU8sUUFBUTs7OztJQU1qQixZQUFZLE9BQVksRUFBRTtRQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7OztJQU1NLEtBQUs7UUFDUixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDcEIsQ0FBQzs7OztJQUNNLFNBQVM7UUFDWixPQUFPLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxNQUFNLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7O0lBQ00sUUFBUTtRQUNYLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUM7SUFDN0IsQ0FBQzs7Ozs7O0lBS00sS0FBSyxDQUFDLEVBQWlCO1FBQ3pCLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ2QsT0FBTyxJQUFJLENBQUM7SUFDakIsQ0FBQzs7Ozs7SUFDTSxTQUFTLENBQUMsTUFBYztRQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUN0QixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUNNLFFBQVEsQ0FBQyxLQUFhO1FBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7OztJQUlNLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBVTtRQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQzFDLE9BQU8sSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUE7SUFDN0IsQ0FBQzs7OztJQUVNLFFBQVE7O1lBQ1AsQ0FBQyxHQUFRO1lBQ1QsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDdEIsTUFBTSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUU7U0FDekI7UUFDRCxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFBRSxDQUFDLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtRQUN2QyxPQUFPLENBQUMsQ0FBQTtJQUNaLENBQUM7Ozs7SUFFTSxNQUFNO1FBQ1QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBO0lBQzFDLENBQUM7Q0FDSjs7Ozs7O0lBM0RHLHVCQUFvQjs7Ozs7SUFDcEIsMkJBQXdCOzs7OztJQUN4QiwwQkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGaWx0ZXIgfSBmcm9tICcuL2ZpbHRlci5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRmF2b3JpdGUge1xyXG5cclxuICAgIHByaXZhdGUgX2lkOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9maWx0ZXI6IEZpbHRlcjtcclxuICAgIHByaXZhdGUgX2xhYmVsOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZGF0YTogYW55ID0ge30pIHtcclxuICAgICAgICB0aGlzLnNldElkKGRhdGEuaWQpO1xyXG4gICAgICAgIHRoaXMuc2V0RmlsdGVyKGRhdGEuZmlsdGVyIHx8IG5ldyBGaWx0ZXIoKSk7XHJcbiAgICAgICAgdGhpcy5zZXRMYWJlbChkYXRhLmxhYmVsIHx8ICcnKTtcclxuICAgIH1cclxuXHJcblxyXG5cclxuICAgIC8vIEdFVFRFUlMgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIHB1YmxpYyBnZXRJZCgpIDogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faWQ7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgZ2V0RmlsdGVyKCkgOiBGaWx0ZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9maWx0ZXIgfHwgbmV3IEZpbHRlcigpO1xyXG4gICAgfVxyXG4gICAgcHVibGljIGdldExhYmVsKCkgOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9sYWJlbCB8fCAnJztcclxuICAgIH1cclxuICAgXHJcblxyXG4gICAgLy8gU0VUVEVSUyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgcHVibGljIHNldElkKGlkOiBudW1iZXIgfCBudWxsKSA6IEZhdm9yaXRlIHtcclxuICAgICAgICAgdGhpcy5faWQgPSBpZDtcclxuICAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgc2V0RmlsdGVyKGZpbHRlcjogRmlsdGVyKSA6IEZhdm9yaXRlIHtcclxuICAgICAgICB0aGlzLl9maWx0ZXIgPSBmaWx0ZXI7XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgc2V0TGFiZWwobGFiZWw6IHN0cmluZykgOiBGYXZvcml0ZSB7XHJcbiAgICAgICAgdGhpcy5fbGFiZWwgPSBsYWJlbDtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuICAgXHJcbiAgICAvLyBTRVJJQUxJWkFUSU9OIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb24gOiBhbnkpIDogRmF2b3JpdGUge1xyXG4gICAgICAgIGpzb24uZmlsdGVyID0gRmlsdGVyLmZyb21KU09OKGpzb24uZmlsdGVyKVxyXG4gICAgICAgIHJldHVybiBuZXcgRmF2b3JpdGUoanNvbilcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdG9PYmplY3QoKSA6IG9iamVjdCB7XHJcbiAgICAgICAgbGV0IG86YW55ID0gIHtcclxuICAgICAgICAgICAgbGFiZWw6IHRoaXMuZ2V0TGFiZWwoKSxcclxuICAgICAgICAgICAgZmlsdGVyOiB0aGlzLmdldEZpbHRlcigpLFxyXG4gICAgICAgICAgfTtcclxuICAgICAgICAgIGlmICh0aGlzLmdldElkKCkpIG8uaWQgPSB0aGlzLmdldElkKClcclxuICAgICAgICByZXR1cm4gb1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBwdWJsaWMgdG9KU09OKCkgOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeSh0aGlzLnRvT2JqZWN0KCkpXHJcbiAgICB9XHJcbn0iXX0=