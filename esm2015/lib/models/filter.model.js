/**
 * @fileoverview added by tsickle
 * Generated from: lib/models/filter.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// import * as moment from 'moment';
import { pick as _pick } from 'lodash';
export class Filter {
    /**
     * @param {?=} data
     */
    constructor(data = {}) {
        // this.setDate(data.date || Filter.defaults.date);
        // this.setTypes(data.types || Filter.defaults.type);
        // this.setModes(data.modes || Filter.defaults.modes);
        this.setRegions(data.regions || Filter.defaults.regions);
        this.setTerritories(data.territories || Filter.defaults.territories);
        this.setContracts(data.contracts || Filter.defaults.contracts);
        this.setCommunes(data.communes || Filter.defaults.communes);
    }
    // ------------------------------------------------
    /**
     * @return {?}
     */
    isEmpty() {
        return (this.getRegions().length === 0
            && this.getTerritories().length === 0
            && this.getContracts().length === 0
            && this.getCommunes().length === 0);
    }
    /**
     * @return {?}
     */
    isSingleContract() {
        /** @type {?} */
        const isSingle = (this.getContracts().length === 1 && this.getCommunes().length === 0);
        return isSingle ? this.singleContract() : false;
    }
    /**
     * @return {?}
     */
    singleContract() {
        return this.getContracts()[0];
    }
    /**
     * @return {?}
     */
    isSingleCommune() {
        /** @type {?} */
        const isSingle = (this.getCommunes().length === 1);
        return isSingle ? this.getCommunes()[0] : false;
    }
    // GETTERS -------------------------------------------
    // public getDate() {
    //     return this._date || Filter.defaults.date;
    // }
    // public getDateParam() {
    //     return moment(this.getDate()).format('YYYY-MM-DD');
    // }
    // public getTypes(): string[] {
    //     return this._types || Filter.defaults.types;
    // }
    // public getModes(): string[] {
    //     return this._modes || Filter.defaults.modes;
    // }
    /**
     * @return {?}
     */
    getRegions() {
        return this._regions || Filter.defaults.regions;
    }
    /**
     * @return {?}
     */
    getTerritories() {
        return this._territories || Filter.defaults.territories;
    }
    /**
     * @return {?}
     */
    getContracts() {
        return this._contracts || Filter.defaults.contracts;
    }
    /**
     * @return {?}
     */
    getCommunes() {
        return this._communes || Filter.defaults.communes;
    }
    // SETTERS -------------------------------------------
    // public setDate(date: Date) {
    //     this._date = date;
    // }
    // public setTypes(types: string[]) {
    //     this._types = types;
    // }
    // public setModes(modes: string[]) {
    //     this._modes = modes
    // }
    /**
     * @param {?} regions
     * @return {?}
     */
    setRegions(regions) {
        this._regions = regions;
    }
    /**
     * @param {?} territories
     * @return {?}
     */
    setTerritories(territories) {
        this._territories = territories;
    }
    /**
     * @param {?} contracts
     * @return {?}
     */
    setContracts(contracts) {
        this._contracts = contracts;
    }
    /**
     * @param {?} communes
     * @return {?}
     */
    setCommunes(communes) {
        this._communes = communes;
    }
    /**
     * @param {?} filter
     * @return {?}
     */
    isEqualTo(filter) {
        return (this.toJSON() === filter.toJSON());
    }
    /**
     * @return {?}
     */
    hasPerimeter() {
        return (this.getRegions().length || this.getTerritories().length || this.getContracts().length);
    }
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJSON(json) {
        return new Filter(json);
    }
    /**
     * @return {?}
     */
    toObject() {
        return {
            regions: this.getRegions(),
            territories: this.getTerritories(),
            contracts: this.getContracts(),
            communes: this.getCommunes(),
        };
    }
    /**
     * @return {?}
     */
    toJSON() {
        return JSON.stringify(this.toObject());
    }
    /**
     * @return {?}
     */
    toStorage() {
        /** @type {?} */
        let json = this.toJSON();
        localStorage.setItem("filter", json);
    }
    /**
     * @return {?}
     */
    static fromStorage() {
        /** @type {?} */
        let filter = new Filter();
        // fill from storage
        /** @type {?} */
        let params = localStorage.getItem("filter");
        if (params) {
            params = JSON.parse(params);
            filter.setRegions(params['regions'] || this.defaults.regions);
            filter.setTerritories(params['territories'] || this.defaults.territories);
            filter.setContracts(params['contracts'] || this.defaults.contracts);
            filter.setCommunes(params['communes'] || this.defaults.communes);
            // filter.setDate(new Date(params['date'] || this.defaults.date));
            // filter.setTypes(params['types'] || this.defaults.types);
            // filter.setModes(params['modes'] || this.defaults.modes);
        }
        return filter;
    }
    /**
     * @return {?}
     */
    clearStorage() {
        localStorage.removeItem("filter");
    }
    // QUERY PARAMS --------------------------------------------------------
    /**
     * @param {?=} fields
     * @return {?}
     */
    toParams(fields = null) {
        /** @type {?} */
        let params = {
            // day: this.getDateParam(),
            // types: this.getTypes().join(','),
            // modes: this.getModes().join(','),
            regions: this.getRegions().join(','),
            territories: this.getTerritories().join(','),
            contracts: this.getContracts().join(','),
            communes: this.getCommunes().join(','),
        }
        // pick the fields we want
        ;
        // pick the fields we want
        if (fields)
            params = _pick(params, fields);
        return params;
    }
}
Filter.defaults = {
    // date: new Date(),
    // types: ['DBO', 'OM'],
    // modes: ['RR', 'TR'],
    regions: [],
    territories: [],
    contracts: [],
    communes: [],
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    Filter.defaults;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._date;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._types;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._modes;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._regions;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._territories;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._contracts;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._communes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHRlbGVvL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvZmlsdGVyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLE9BQU8sRUFBRSxJQUFJLElBQUksS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRXZDLE1BQU0sT0FBTyxNQUFNOzs7O0lBb0JmLFlBQVksT0FBWSxFQUFFO1FBQ3RCLG1EQUFtRDtRQUNuRCxxREFBcUQ7UUFDckQsc0RBQXNEO1FBQ3RELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7O0lBR00sT0FBTztRQUNWLE9BQU8sQ0FDSCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUM7ZUFDM0IsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDO2VBQ2xDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQztlQUNoQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FDckMsQ0FBQTtJQUNMLENBQUM7Ozs7SUFHTSxnQkFBZ0I7O2NBQ2IsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7UUFDdEYsT0FBTyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFTSxjQUFjO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFTSxlQUFlOztjQUNaLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1FBQ2xELE9BQU8sUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUNwRCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztJQWlCTSxVQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFDTSxjQUFjO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztJQUM1RCxDQUFDOzs7O0lBQ00sWUFBWTtRQUNmLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUN4RCxDQUFDOzs7O0lBQ00sV0FBVztRQUNkLE9BQU8sSUFBSSxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztJQUN0RCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7SUFhTSxVQUFVLENBQUMsT0FBaUI7UUFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFDTSxjQUFjLENBQUMsV0FBcUI7UUFDdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUE7SUFDbkMsQ0FBQzs7Ozs7SUFDTSxZQUFZLENBQUMsU0FBbUI7UUFDbkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7SUFDaEMsQ0FBQzs7Ozs7SUFDTSxXQUFXLENBQUMsUUFBa0I7UUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFHTSxTQUFTLENBQUMsTUFBYztRQUMzQixPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFBO0lBQzlDLENBQUM7Ozs7SUFFTSxZQUFZO1FBQ2YsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDcEcsQ0FBQzs7Ozs7O0lBSU0sTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFZO1FBQy9CLE9BQU8sSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVNLFFBQVE7UUFDWCxPQUFRO1lBQ0osT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDMUIsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDbEMsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDOUIsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUU7U0FJL0IsQ0FBQztJQUNOLENBQUM7Ozs7SUFFTSxNQUFNO1FBQ1QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFTSxTQUFTOztZQUNSLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFO1FBQ3hCLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Ozs7SUFFTSxNQUFNLENBQUMsV0FBVzs7WUFFakIsTUFBTSxHQUFHLElBQUksTUFBTSxFQUFFOzs7WUFHckIsTUFBTSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQzNDLElBQUksTUFBTSxFQUFFO1lBQ1IsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDNUIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM5RCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDcEUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRSxrRUFBa0U7WUFDbEUsMkRBQTJEO1lBQzNELDJEQUEyRDtTQUM5RDtRQUNELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7Ozs7SUFFTSxZQUFZO1FBQ2YsWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7Ozs7SUFJRCxRQUFRLENBQUMsU0FBbUIsSUFBSTs7WUFDeEIsTUFBTSxHQUFHOzs7O1lBSVQsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ3BDLFdBQVcsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUM1QyxTQUFTLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDeEMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1NBQ3pDO1FBRUQsMEJBQTBCOztRQUExQiwwQkFBMEI7UUFDMUIsSUFBSSxNQUFNO1lBQUUsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFMUMsT0FBTyxNQUFNLENBQUM7SUFFbEIsQ0FBQzs7QUEvS3VCLGVBQVEsR0FBUTs7OztJQUlwQyxPQUFPLEVBQUUsRUFBRTtJQUNYLFdBQVcsRUFBRSxFQUFFO0lBQ2YsU0FBUyxFQUFFLEVBQUU7SUFDYixRQUFRLEVBQUUsRUFBRTtDQUNmLENBQUE7Ozs7OztJQVJELGdCQVFDOzs7OztJQWhCRCx1QkFBb0I7Ozs7O0lBQ3BCLHdCQUF5Qjs7Ozs7SUFDekIsd0JBQXlCOzs7OztJQUN6QiwwQkFBMkI7Ozs7O0lBQzNCLDhCQUErQjs7Ozs7SUFDL0IsNEJBQTZCOzs7OztJQUM3QiwyQkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbXBvcnQgKiBhcyBtb21lbnQgZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgcGljayBhcyBfcGljayB9IGZyb20gJ2xvZGFzaCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRmlsdGVyIHtcclxuXHJcbiAgICBwcml2YXRlIF9kYXRlOiBEYXRlO1xyXG4gICAgcHJpdmF0ZSBfdHlwZXM6IHN0cmluZ1tdO1xyXG4gICAgcHJpdmF0ZSBfbW9kZXM6IHN0cmluZ1tdO1xyXG4gICAgcHJpdmF0ZSBfcmVnaW9uczogc3RyaW5nW107XHJcbiAgICBwcml2YXRlIF90ZXJyaXRvcmllczogc3RyaW5nW107XHJcbiAgICBwcml2YXRlIF9jb250cmFjdHM6IHN0cmluZ1tdO1xyXG4gICAgcHJpdmF0ZSBfY29tbXVuZXM6IHN0cmluZ1tdO1xyXG5cclxuICAgIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRzOiBhbnkgPSB7XHJcbiAgICAgICAgLy8gZGF0ZTogbmV3IERhdGUoKSxcclxuICAgICAgICAvLyB0eXBlczogWydEQk8nLCAnT00nXSxcclxuICAgICAgICAvLyBtb2RlczogWydSUicsICdUUiddLFxyXG4gICAgICAgIHJlZ2lvbnM6IFtdLFxyXG4gICAgICAgIHRlcnJpdG9yaWVzOiBbXSxcclxuICAgICAgICBjb250cmFjdHM6IFtdLFxyXG4gICAgICAgIGNvbW11bmVzOiBbXSxcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihkYXRhOiBhbnkgPSB7fSkge1xyXG4gICAgICAgIC8vIHRoaXMuc2V0RGF0ZShkYXRhLmRhdGUgfHwgRmlsdGVyLmRlZmF1bHRzLmRhdGUpO1xyXG4gICAgICAgIC8vIHRoaXMuc2V0VHlwZXMoZGF0YS50eXBlcyB8fCBGaWx0ZXIuZGVmYXVsdHMudHlwZSk7XHJcbiAgICAgICAgLy8gdGhpcy5zZXRNb2RlcyhkYXRhLm1vZGVzIHx8IEZpbHRlci5kZWZhdWx0cy5tb2Rlcyk7XHJcbiAgICAgICAgdGhpcy5zZXRSZWdpb25zKGRhdGEucmVnaW9ucyB8fCBGaWx0ZXIuZGVmYXVsdHMucmVnaW9ucyk7XHJcbiAgICAgICAgdGhpcy5zZXRUZXJyaXRvcmllcyhkYXRhLnRlcnJpdG9yaWVzIHx8IEZpbHRlci5kZWZhdWx0cy50ZXJyaXRvcmllcyk7XHJcbiAgICAgICAgdGhpcy5zZXRDb250cmFjdHMoZGF0YS5jb250cmFjdHMgfHwgRmlsdGVyLmRlZmF1bHRzLmNvbnRyYWN0cyk7XHJcbiAgICAgICAgdGhpcy5zZXRDb21tdW5lcyhkYXRhLmNvbW11bmVzIHx8IEZpbHRlci5kZWZhdWx0cy5jb21tdW5lcyk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICBwdWJsaWMgaXNFbXB0eSgpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICB0aGlzLmdldFJlZ2lvbnMoKS5sZW5ndGggPT09IDBcclxuICAgICAgICAgICAgJiYgdGhpcy5nZXRUZXJyaXRvcmllcygpLmxlbmd0aCA9PT0gMFxyXG4gICAgICAgICAgICAmJiB0aGlzLmdldENvbnRyYWN0cygpLmxlbmd0aCA9PT0gMFxyXG4gICAgICAgICAgICAmJiB0aGlzLmdldENvbW11bmVzKCkubGVuZ3RoID09PSAwXHJcbiAgICAgICAgKVxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBwdWJsaWMgaXNTaW5nbGVDb250cmFjdCgpOiBib29sZWFuIHwgc3RyaW5nIHtcclxuICAgICAgICBjb25zdCBpc1NpbmdsZSA9ICh0aGlzLmdldENvbnRyYWN0cygpLmxlbmd0aCA9PT0gMSAmJiB0aGlzLmdldENvbW11bmVzKCkubGVuZ3RoID09PSAwKTtcclxuICAgICAgICByZXR1cm4gaXNTaW5nbGUgPyB0aGlzLnNpbmdsZUNvbnRyYWN0KCkgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2luZ2xlQ29udHJhY3QoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRDb250cmFjdHMoKVswXTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaXNTaW5nbGVDb21tdW5lKCk6IGJvb2xlYW4gfCBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGlzU2luZ2xlID0gKHRoaXMuZ2V0Q29tbXVuZXMoKS5sZW5ndGggPT09IDEpO1xyXG4gICAgICAgIHJldHVybiBpc1NpbmdsZSA/IHRoaXMuZ2V0Q29tbXVuZXMoKVswXSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvLyBHRVRURVJTIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvLyBwdWJsaWMgZ2V0RGF0ZSgpIHtcclxuICAgIC8vICAgICByZXR1cm4gdGhpcy5fZGF0ZSB8fCBGaWx0ZXIuZGVmYXVsdHMuZGF0ZTtcclxuICAgIC8vIH1cclxuICAgIC8vIHB1YmxpYyBnZXREYXRlUGFyYW0oKSB7XHJcbiAgICAvLyAgICAgcmV0dXJuIG1vbWVudCh0aGlzLmdldERhdGUoKSkuZm9ybWF0KCdZWVlZLU1NLUREJyk7XHJcbiAgICAvLyB9XHJcbiAgICAvLyBwdWJsaWMgZ2V0VHlwZXMoKTogc3RyaW5nW10ge1xyXG4gICAgLy8gICAgIHJldHVybiB0aGlzLl90eXBlcyB8fCBGaWx0ZXIuZGVmYXVsdHMudHlwZXM7XHJcbiAgICAvLyB9XHJcbiAgICAvLyBwdWJsaWMgZ2V0TW9kZXMoKTogc3RyaW5nW10ge1xyXG4gICAgLy8gICAgIHJldHVybiB0aGlzLl9tb2RlcyB8fCBGaWx0ZXIuZGVmYXVsdHMubW9kZXM7XHJcbiAgICAvLyB9XHJcbiAgICBwdWJsaWMgZ2V0UmVnaW9ucygpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3JlZ2lvbnMgfHwgRmlsdGVyLmRlZmF1bHRzLnJlZ2lvbnM7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgZ2V0VGVycml0b3JpZXMoKTogc3RyaW5nW10ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl90ZXJyaXRvcmllcyB8fCBGaWx0ZXIuZGVmYXVsdHMudGVycml0b3JpZXM7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgZ2V0Q29udHJhY3RzKCk6IHN0cmluZ1tdIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29udHJhY3RzIHx8IEZpbHRlci5kZWZhdWx0cy5jb250cmFjdHM7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgZ2V0Q29tbXVuZXMoKTogc3RyaW5nW10ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jb21tdW5lcyB8fCBGaWx0ZXIuZGVmYXVsdHMuY29tbXVuZXM7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU0VUVEVSUyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgLy8gcHVibGljIHNldERhdGUoZGF0ZTogRGF0ZSkge1xyXG4gICAgLy8gICAgIHRoaXMuX2RhdGUgPSBkYXRlO1xyXG4gICAgLy8gfVxyXG4gICAgLy8gcHVibGljIHNldFR5cGVzKHR5cGVzOiBzdHJpbmdbXSkge1xyXG4gICAgLy8gICAgIHRoaXMuX3R5cGVzID0gdHlwZXM7XHJcbiAgICAvLyB9XHJcbiAgICAvLyBwdWJsaWMgc2V0TW9kZXMobW9kZXM6IHN0cmluZ1tdKSB7XHJcbiAgICAvLyAgICAgdGhpcy5fbW9kZXMgPSBtb2Rlc1xyXG4gICAgLy8gfVxyXG4gICAgcHVibGljIHNldFJlZ2lvbnMocmVnaW9uczogc3RyaW5nW10pIHtcclxuICAgICAgICB0aGlzLl9yZWdpb25zID0gcmVnaW9ucztcclxuICAgIH1cclxuICAgIHB1YmxpYyBzZXRUZXJyaXRvcmllcyh0ZXJyaXRvcmllczogc3RyaW5nW10pIHtcclxuICAgICAgICB0aGlzLl90ZXJyaXRvcmllcyA9IHRlcnJpdG9yaWVzXHJcbiAgICB9XHJcbiAgICBwdWJsaWMgc2V0Q29udHJhY3RzKGNvbnRyYWN0czogc3RyaW5nW10pIHtcclxuICAgICAgICB0aGlzLl9jb250cmFjdHMgPSBjb250cmFjdHM7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgc2V0Q29tbXVuZXMoY29tbXVuZXM6IHN0cmluZ1tdKSB7XHJcbiAgICAgICAgdGhpcy5fY29tbXVuZXMgPSBjb21tdW5lcztcclxuICAgIH1cclxuXHJcblxyXG4gICAgcHVibGljIGlzRXF1YWxUbyhmaWx0ZXI6IEZpbHRlcik6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAodGhpcy50b0pTT04oKSA9PT0gZmlsdGVyLnRvSlNPTigpKVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoYXNQZXJpbWV0ZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuICh0aGlzLmdldFJlZ2lvbnMoKS5sZW5ndGggfHwgdGhpcy5nZXRUZXJyaXRvcmllcygpLmxlbmd0aCB8fCB0aGlzLmdldENvbnRyYWN0cygpLmxlbmd0aCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU0VSSUFMSVpBVElPTiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSlNPTihqc29uOiBvYmplY3QpOiBGaWx0ZXIge1xyXG4gICAgICAgIHJldHVybiBuZXcgRmlsdGVyKGpzb24pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b09iamVjdCgpIDogb2JqZWN0IHtcclxuICAgICAgICByZXR1cm4gIHtcclxuICAgICAgICAgICAgcmVnaW9uczogdGhpcy5nZXRSZWdpb25zKCksXHJcbiAgICAgICAgICAgIHRlcnJpdG9yaWVzOiB0aGlzLmdldFRlcnJpdG9yaWVzKCksXHJcbiAgICAgICAgICAgIGNvbnRyYWN0czogdGhpcy5nZXRDb250cmFjdHMoKSxcclxuICAgICAgICAgICAgY29tbXVuZXM6IHRoaXMuZ2V0Q29tbXVuZXMoKSxcclxuICAgICAgICAgICAgLy8gZGF0ZTogdGhpcy5nZXREYXRlKCksXHJcbiAgICAgICAgICAgIC8vIHR5cGVzOiB0aGlzLmdldFR5cGVzKCksXHJcbiAgICAgICAgICAgIC8vIG1vZGVzOiB0aGlzLmdldE1vZGVzKCksXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgcHVibGljIHRvSlNPTigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeSh0aGlzLnRvT2JqZWN0KCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b1N0b3JhZ2UoKSB7XHJcbiAgICAgICAgbGV0IGpzb24gPSB0aGlzLnRvSlNPTigpXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJmaWx0ZXJcIiwganNvbik7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tU3RvcmFnZSgpOiBGaWx0ZXIge1xyXG5cclxuICAgICAgICBsZXQgZmlsdGVyID0gbmV3IEZpbHRlcigpO1xyXG5cclxuICAgICAgICAvLyBmaWxsIGZyb20gc3RvcmFnZVxyXG4gICAgICAgIGxldCBwYXJhbXMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImZpbHRlclwiKTtcclxuICAgICAgICBpZiAocGFyYW1zKSB7XHJcbiAgICAgICAgICAgIHBhcmFtcyA9IEpTT04ucGFyc2UocGFyYW1zKTtcclxuICAgICAgICAgICAgZmlsdGVyLnNldFJlZ2lvbnMocGFyYW1zWydyZWdpb25zJ10gfHwgdGhpcy5kZWZhdWx0cy5yZWdpb25zKTtcclxuICAgICAgICAgICAgZmlsdGVyLnNldFRlcnJpdG9yaWVzKHBhcmFtc1sndGVycml0b3JpZXMnXSB8fCB0aGlzLmRlZmF1bHRzLnRlcnJpdG9yaWVzKTtcclxuICAgICAgICAgICAgZmlsdGVyLnNldENvbnRyYWN0cyhwYXJhbXNbJ2NvbnRyYWN0cyddIHx8IHRoaXMuZGVmYXVsdHMuY29udHJhY3RzKTtcclxuICAgICAgICAgICAgZmlsdGVyLnNldENvbW11bmVzKHBhcmFtc1snY29tbXVuZXMnXSB8fCB0aGlzLmRlZmF1bHRzLmNvbW11bmVzKTtcclxuICAgICAgICAgICAgLy8gZmlsdGVyLnNldERhdGUobmV3IERhdGUocGFyYW1zWydkYXRlJ10gfHwgdGhpcy5kZWZhdWx0cy5kYXRlKSk7XHJcbiAgICAgICAgICAgIC8vIGZpbHRlci5zZXRUeXBlcyhwYXJhbXNbJ3R5cGVzJ10gfHwgdGhpcy5kZWZhdWx0cy50eXBlcyk7XHJcbiAgICAgICAgICAgIC8vIGZpbHRlci5zZXRNb2RlcyhwYXJhbXNbJ21vZGVzJ10gfHwgdGhpcy5kZWZhdWx0cy5tb2Rlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmaWx0ZXJcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNsZWFyU3RvcmFnZSgpIHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImZpbHRlclwiKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBRVUVSWSBQQVJBTVMgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICB0b1BhcmFtcyhmaWVsZHM6IHN0cmluZ1tdID0gbnVsbCk6IGFueSB7XHJcbiAgICAgICAgbGV0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgLy8gZGF5OiB0aGlzLmdldERhdGVQYXJhbSgpLFxyXG4gICAgICAgICAgICAvLyB0eXBlczogdGhpcy5nZXRUeXBlcygpLmpvaW4oJywnKSxcclxuICAgICAgICAgICAgLy8gbW9kZXM6IHRoaXMuZ2V0TW9kZXMoKS5qb2luKCcsJyksXHJcbiAgICAgICAgICAgIHJlZ2lvbnM6IHRoaXMuZ2V0UmVnaW9ucygpLmpvaW4oJywnKSxcclxuICAgICAgICAgICAgdGVycml0b3JpZXM6IHRoaXMuZ2V0VGVycml0b3JpZXMoKS5qb2luKCcsJyksXHJcbiAgICAgICAgICAgIGNvbnRyYWN0czogdGhpcy5nZXRDb250cmFjdHMoKS5qb2luKCcsJyksXHJcbiAgICAgICAgICAgIGNvbW11bmVzOiB0aGlzLmdldENvbW11bmVzKCkuam9pbignLCcpLFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gcGljayB0aGUgZmllbGRzIHdlIHdhbnRcclxuICAgICAgICBpZiAoZmllbGRzKSBwYXJhbXMgPSBfcGljayhwYXJhbXMsIGZpZWxkcylcclxuXHJcbiAgICAgICAgcmV0dXJuIHBhcmFtcztcclxuXHJcbiAgICB9XHJcblxyXG5cclxufSJdfQ==