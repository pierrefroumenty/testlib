/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/is-loading.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { LoadingOverlayComponent } from '../loading-overlay/loading-overlay.component';
export class IsLoadingDirective {
    /**
     * @param {?} viewContainerRef
     * @param {?} componentFactoryResolver
     */
    constructor(viewContainerRef, componentFactoryResolver) {
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.isLoading = false;
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.isLoading ? this.addLoader() : this.removeLoader();
    }
    /**
     * @return {?}
     */
    removeLoader() {
        this.loader && this.loader.destroy();
    }
    /**
     * @return {?}
     */
    addLoader() {
        if (this.loader)
            return;
        /** @type {?} */
        const factory = this.componentFactoryResolver.resolveComponentFactory(LoadingOverlayComponent);
        this.loader = this.viewContainerRef.createComponent(factory);
    }
}
IsLoadingDirective.decorators = [
    { type: Directive, args: [{
                selector: '[appIsLoading]'
            },] }
];
/** @nocollapse */
IsLoadingDirective.ctorParameters = () => [
    { type: ViewContainerRef },
    { type: ComponentFactoryResolver }
];
IsLoadingDirective.propDecorators = {
    isLoading: [{ type: Input, args: ['appIsLoading',] }]
};
if (false) {
    /** @type {?} */
    IsLoadingDirective.prototype.loader;
    /** @type {?} */
    IsLoadingDirective.prototype.isLoading;
    /**
     * @type {?}
     * @private
     */
    IsLoadingDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @private
     */
    IsLoadingDirective.prototype.componentFactoryResolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXMtbG9hZGluZy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AdGVsZW8vY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2RpcmVjdGl2ZXMvaXMtbG9hZGluZy5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFHLEtBQUssRUFBYSxnQkFBZ0IsRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUt2RixNQUFNLE9BQU8sa0JBQWtCOzs7OztJQUs3QixZQUNVLGdCQUFrQyxFQUNsQyx3QkFBa0Q7UUFEbEQscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBSHJDLGNBQVMsR0FBRyxLQUFLLENBQUM7SUFNekMsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUMxRCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQTtJQUN0QyxDQUFDOzs7O0lBRUQsU0FBUztRQUNQLElBQUksSUFBSSxDQUFDLE1BQU07WUFBRSxPQUFPOztjQUNsQixPQUFPLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixDQUFDO1FBQzlGLElBQUksQ0FBQyxNQUFNLEdBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQTtJQUMvRCxDQUFDOzs7WUEzQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7YUFDM0I7Ozs7WUFMc0MsZ0JBQWdCO1lBQUUsd0JBQXdCOzs7d0JBVTlFLEtBQUssU0FBQyxjQUFjOzs7O0lBRnJCLG9DQUFPOztJQUVQLHVDQUF5Qzs7Ozs7SUFFdkMsOENBQTBDOzs7OztJQUMxQyxzREFBMEQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsICBJbnB1dCwgT25DaGFuZ2VzLCBWaWV3Q29udGFpbmVyUmVmLCBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IExvYWRpbmdPdmVybGF5Q29tcG9uZW50IH0gZnJvbSAnLi4vbG9hZGluZy1vdmVybGF5L2xvYWRpbmctb3ZlcmxheS5jb21wb25lbnQnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbYXBwSXNMb2FkaW5nXSdcbn0pXG5leHBvcnQgY2xhc3MgSXNMb2FkaW5nRGlyZWN0aXZlIGltcGxlbWVudHMgT25DaGFuZ2VzIHtcblxuICBsb2FkZXI7XG5cbiAgQElucHV0KCdhcHBJc0xvYWRpbmcnKSBpc0xvYWRpbmcgPSBmYWxzZTtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmLFxuICAgIHByaXZhdGUgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICkge1xuXG4gIH1cblxuICBuZ09uQ2hhbmdlcygpIHtcbiAgICB0aGlzLmlzTG9hZGluZyA/IHRoaXMuYWRkTG9hZGVyKCkgOiB0aGlzLnJlbW92ZUxvYWRlcigpO1xuICB9XG5cbiAgcmVtb3ZlTG9hZGVyKCkge1xuICAgIHRoaXMubG9hZGVyICYmIHRoaXMubG9hZGVyLmRlc3Ryb3koKVxuICB9XG5cbiAgYWRkTG9hZGVyKCkge1xuICAgIGlmICh0aGlzLmxvYWRlcikgcmV0dXJuO1xuICAgIGNvbnN0IGZhY3RvcnkgPSB0aGlzLmNvbXBvbmVudEZhY3RvcnlSZXNvbHZlci5yZXNvbHZlQ29tcG9uZW50RmFjdG9yeShMb2FkaW5nT3ZlcmxheUNvbXBvbmVudCk7XG4gICAgdGhpcy5sb2FkZXIgID0gdGhpcy52aWV3Q29udGFpbmVyUmVmLmNyZWF0ZUNvbXBvbmVudChmYWN0b3J5KVxuICB9XG5cbn1cbiJdfQ==