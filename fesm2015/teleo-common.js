import { EventEmitter, Component, Input, Output, Injectable, ɵɵdefineInjectable, Directive, ViewContainerRef, ComponentFactoryResolver, NgModule } from '@angular/core';
import 'rxjs';
import { pick, cloneDeep } from 'lodash';
import { CommonModule as CommonModule$1 } from '@angular/common';
import { MatIconModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatRippleModule } from '@angular/material/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/models/filter.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Filter {
    /**
     * @param {?=} data
     */
    constructor(data = {}) {
        // this.setDate(data.date || Filter.defaults.date);
        // this.setTypes(data.types || Filter.defaults.type);
        // this.setModes(data.modes || Filter.defaults.modes);
        this.setRegions(data.regions || Filter.defaults.regions);
        this.setTerritories(data.territories || Filter.defaults.territories);
        this.setContracts(data.contracts || Filter.defaults.contracts);
        this.setCommunes(data.communes || Filter.defaults.communes);
    }
    // ------------------------------------------------
    /**
     * @return {?}
     */
    isEmpty() {
        return (this.getRegions().length === 0
            && this.getTerritories().length === 0
            && this.getContracts().length === 0
            && this.getCommunes().length === 0);
    }
    /**
     * @return {?}
     */
    isSingleContract() {
        /** @type {?} */
        const isSingle = (this.getContracts().length === 1 && this.getCommunes().length === 0);
        return isSingle ? this.singleContract() : false;
    }
    /**
     * @return {?}
     */
    singleContract() {
        return this.getContracts()[0];
    }
    /**
     * @return {?}
     */
    isSingleCommune() {
        /** @type {?} */
        const isSingle = (this.getCommunes().length === 1);
        return isSingle ? this.getCommunes()[0] : false;
    }
    // GETTERS -------------------------------------------
    // public getDate() {
    //     return this._date || Filter.defaults.date;
    // }
    // public getDateParam() {
    //     return moment(this.getDate()).format('YYYY-MM-DD');
    // }
    // public getTypes(): string[] {
    //     return this._types || Filter.defaults.types;
    // }
    // public getModes(): string[] {
    //     return this._modes || Filter.defaults.modes;
    // }
    /**
     * @return {?}
     */
    getRegions() {
        return this._regions || Filter.defaults.regions;
    }
    /**
     * @return {?}
     */
    getTerritories() {
        return this._territories || Filter.defaults.territories;
    }
    /**
     * @return {?}
     */
    getContracts() {
        return this._contracts || Filter.defaults.contracts;
    }
    /**
     * @return {?}
     */
    getCommunes() {
        return this._communes || Filter.defaults.communes;
    }
    // SETTERS -------------------------------------------
    // public setDate(date: Date) {
    //     this._date = date;
    // }
    // public setTypes(types: string[]) {
    //     this._types = types;
    // }
    // public setModes(modes: string[]) {
    //     this._modes = modes
    // }
    /**
     * @param {?} regions
     * @return {?}
     */
    setRegions(regions) {
        this._regions = regions;
    }
    /**
     * @param {?} territories
     * @return {?}
     */
    setTerritories(territories) {
        this._territories = territories;
    }
    /**
     * @param {?} contracts
     * @return {?}
     */
    setContracts(contracts) {
        this._contracts = contracts;
    }
    /**
     * @param {?} communes
     * @return {?}
     */
    setCommunes(communes) {
        this._communes = communes;
    }
    /**
     * @param {?} filter
     * @return {?}
     */
    isEqualTo(filter) {
        return (this.toJSON() === filter.toJSON());
    }
    /**
     * @return {?}
     */
    hasPerimeter() {
        return (this.getRegions().length || this.getTerritories().length || this.getContracts().length);
    }
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJSON(json) {
        return new Filter(json);
    }
    /**
     * @return {?}
     */
    toObject() {
        return {
            regions: this.getRegions(),
            territories: this.getTerritories(),
            contracts: this.getContracts(),
            communes: this.getCommunes(),
        };
    }
    /**
     * @return {?}
     */
    toJSON() {
        return JSON.stringify(this.toObject());
    }
    /**
     * @return {?}
     */
    toStorage() {
        /** @type {?} */
        let json = this.toJSON();
        localStorage.setItem("filter", json);
    }
    /**
     * @return {?}
     */
    static fromStorage() {
        /** @type {?} */
        let filter = new Filter();
        // fill from storage
        /** @type {?} */
        let params = localStorage.getItem("filter");
        if (params) {
            params = JSON.parse(params);
            filter.setRegions(params['regions'] || this.defaults.regions);
            filter.setTerritories(params['territories'] || this.defaults.territories);
            filter.setContracts(params['contracts'] || this.defaults.contracts);
            filter.setCommunes(params['communes'] || this.defaults.communes);
            // filter.setDate(new Date(params['date'] || this.defaults.date));
            // filter.setTypes(params['types'] || this.defaults.types);
            // filter.setModes(params['modes'] || this.defaults.modes);
        }
        return filter;
    }
    /**
     * @return {?}
     */
    clearStorage() {
        localStorage.removeItem("filter");
    }
    // QUERY PARAMS --------------------------------------------------------
    /**
     * @param {?=} fields
     * @return {?}
     */
    toParams(fields = null) {
        /** @type {?} */
        let params = {
            // day: this.getDateParam(),
            // types: this.getTypes().join(','),
            // modes: this.getModes().join(','),
            regions: this.getRegions().join(','),
            territories: this.getTerritories().join(','),
            contracts: this.getContracts().join(','),
            communes: this.getCommunes().join(','),
        }
        // pick the fields we want
        ;
        // pick the fields we want
        if (fields)
            params = pick(params, fields);
        return params;
    }
}
Filter.defaults = {
    // date: new Date(),
    // types: ['DBO', 'OM'],
    // modes: ['RR', 'TR'],
    regions: [],
    territories: [],
    contracts: [],
    communes: [],
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    Filter.defaults;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._date;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._types;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._modes;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._regions;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._territories;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._contracts;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._communes;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/filter/filter.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FilterComponent {
    constructor() {
        this.options = null; // don't modify this object, except the ".hidden" properties
        this.filterChanged = new EventEmitter();
        this.deleteFavorite = new EventEmitter();
        this.saveAsFavorite = new EventEmitter();
        this.sortedOptions = null;
        this.devMode = false;
        this.closed = false;
        this.selectedTab = 0;
        this.form = {
            contracts: [],
            regions: [],
            territories: [],
            communes: [],
            searchRegions: '',
            searchTerritories: '',
            searchContracts: '',
            searchCommunes: '',
        };
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.filter)
            this.setForm(this.filter);
        if (changes.options) {
            this.options = cloneDeep(this.options);
            this.updateOptions();
        }
    }
    /**
     * @return {?}
     */
    isSubmittable() {
        return this.form.regions.length || this.form.territories.length || this.form.contracts.length;
    }
    /**
     * @param {?} filter
     * @return {?}
     */
    setForm(filter) {
        this.form.contracts = filter.getContracts();
        this.form.regions = filter.getRegions();
        this.form.territories = filter.getTerritories();
        this.form.communes = filter.getCommunes();
        this.onChange();
    }
    /**
     * @return {?}
     */
    onChange() {
        // clean user selection (remove impossible cases)
        this.cleanSelection();
        this.updateOptions();
    }
    // -----------------------------------------
    /**
     * @return {?}
     */
    cleanSelection() {
        if (!this.options)
            return;
        /** @type {?} */
        let filter = new Filter(this.form);
        /** @type {?} */
        let cleanedFilter = cloneDeep(filter);
        /** @type {?} */
        let currentTerritories = cleanedFilter.getTerritories().map((/**
         * @param {?} id
         * @return {?}
         */
        id => this.options.territories.find((/**
         * @param {?} t
         * @return {?}
         */
        t => t.id == id))));
        /** @type {?} */
        let currentContracts = cleanedFilter.getContracts().map((/**
         * @param {?} id
         * @return {?}
         */
        id => this.options.contracts.find((/**
         * @param {?} c
         * @return {?}
         */
        c => c.id == id))));
        /** @type {?} */
        let currentCommunes = cleanedFilter.getCommunes().map((/**
         * @param {?} id
         * @return {?}
         */
        id => this.options.communes.find((/**
         * @param {?} c
         * @return {?}
         */
        c => c.id == id))));
        // filter by regions
        if (cleanedFilter.getRegions().length) {
            cleanedFilter.setTerritories(currentTerritories.filter((/**
             * @param {?} t
             * @return {?}
             */
            (t) => cleanedFilter.getRegions().includes(t.region_id))).map((/**
             * @param {?} t
             * @return {?}
             */
            t => t.id)));
            cleanedFilter.setContracts(currentContracts.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getRegions().includes(c.region_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getRegions().includes(c.region_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
        }
        // filter by territories
        if (cleanedFilter.getTerritories().length) {
            cleanedFilter.setContracts(currentContracts.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getTerritories().includes(c.territory_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getTerritories().includes(c.territory_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
        }
        // filter by contracts
        if (cleanedFilter.getContracts().length) {
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => cleanedFilter.getContracts().includes(c.contract_id))).map((/**
             * @param {?} c
             * @return {?}
             */
            c => c.id)));
        }
        // update form if the cleaned version of the filter is different
        if (!cleanedFilter.isEqualTo(filter))
            this.setForm(cleanedFilter);
    }
    // -----------------------------------------
    /**
     * @return {?}
     */
    updateOptions() {
        if (!this.options)
            return;
        /** @type {?} */
        let filter = new Filter(this.form);
        /** @type {?} */
        const filterRegions = filter.getRegions();
        /** @type {?} */
        const filterTerritories = filter.getTerritories();
        /** @type {?} */
        const filterContracts = filter.getContracts();
        // filter region options
        this.options.regions.map((/**
         * @param {?} r
         * @return {?}
         */
        r => {
            /** @type {?} */
            const passSearchFilter = !this.form.searchRegions.length || (String.search(this.form.searchRegions, r.name) || String.search(this.form.searchRegions, r.id));
            /** @type {?} */
            const pass = passSearchFilter;
            r.hidden = !pass;
            return r;
        }));
        // filter territory options
        this.options.territories.map((/**
         * @param {?} t
         * @return {?}
         */
        t => {
            /** @type {?} */
            const passRegionFilter = !filterRegions.length || filterRegions.includes(t.region_id);
            /** @type {?} */
            const passSearchFilter = !this.form.searchTerritories.length || (String.search(this.form.searchTerritories, t.name) || String.search(this.form.searchTerritories, t.id));
            /** @type {?} */
            const pass = passRegionFilter && passSearchFilter;
            /** @type {?} */
            const hidden = !pass;
            if (t.hidden !== hidden)
                t.hidden = hidden;
            return t;
        }));
        // filter contract options
        this.options.contracts.map((/**
         * @param {?} c
         * @return {?}
         */
        c => {
            /** @type {?} */
            const passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
            /** @type {?} */
            const passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
            /** @type {?} */
            const passSearchFilter = !this.form.searchContracts.length || (String.search(this.form.searchContracts, c.name) || String.search(this.form.searchContracts, c.id));
            /** @type {?} */
            const pass = passRegionFilter && passTerritoryFilter && passSearchFilter;
            /** @type {?} */
            const hidden = !pass;
            if (c.hidden !== hidden)
                c.hidden = hidden;
            return c;
        }));
        // filter commune options
        this.options.communes.map((/**
         * @param {?} c
         * @return {?}
         */
        c => {
            /** @type {?} */
            const passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
            /** @type {?} */
            const passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
            /** @type {?} */
            const passContractFilter = !filterContracts.length || filterContracts.includes(c.contract_id);
            /** @type {?} */
            const passSearchFilter = !this.form.searchCommunes.length || (String.search(this.form.searchCommunes, c.name) || String.search(this.form.searchCommunes, c.id));
            /** @type {?} */
            const pass = passRegionFilter && passTerritoryFilter && passContractFilter && passSearchFilter;
            /** @type {?} */
            const hidden = !pass;
            if (c.hidden !== hidden)
                c.hidden = hidden;
            return c;
        }));
        // Below, we put hidden options at the end, so that mat-select's scrolling behaves correctly
        // 1. this way we also can use "visibility" css property which is faster than *ngIf which would insert/removes DOM nodes
        // 2. this way, using the search field doesn't remove our previous selection
        this.sortedOptions = {
            regions: this.options.regions.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
            territories: this.options.territories.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
            contracts: this.options.contracts.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
            communes: this.options.communes.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }))),
        };
    }
    /**
     * @param {?} index
     * @param {?} element
     * @return {?}
     */
    trackElement(index, element) {
        return element ? element.id : null;
    }
    // SELECT / DESELECT --------------------------------------------
    /**
     * @return {?}
     */
    selectAllRegions() {
        if (!this.options)
            return;
        this.form.regions = this.options.regions.map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.id));
        this.onChange();
    }
    /**
     * @return {?}
     */
    deselectAllRegions() {
        this.form.regions = [];
        this.form.territories = [];
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    }
    /**
     * @return {?}
     */
    selectAllTerritories() {
        if (!this.options)
            return;
        this.form.territories = this.options.territories.map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.id));
        this.onChange();
    }
    /**
     * @return {?}
     */
    deselectAllTerritories() {
        this.form.territories = [];
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    }
    /**
     * @return {?}
     */
    selectAllContracts() {
        if (!this.options)
            return;
        this.form.contracts = this.options.contracts.map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.id));
        this.onChange();
    }
    /**
     * @return {?}
     */
    deselectAllContracts() {
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    _handleKeydown(event) {
        event.stopPropagation();
    }
    /**
     * @return {?}
     */
    getRegionsPlaceholder() {
        /** @type {?} */
        const selectedOptions = this.form && (this.form.regions || []).length;
        /** @type {?} */
        const availableOptions = this.countRegionsOptions();
        return this.devMode ? `Régions (${selectedOptions}/${availableOptions})` : `Régions (${selectedOptions})`;
    }
    /**
     * @return {?}
     */
    getTerritoriesPlaceholder() {
        /** @type {?} */
        const selectedOptions = this.form && (this.form.territories || []).length;
        /** @type {?} */
        const availableOptions = this.countTerritoriesOptions();
        return this.devMode ? `Territoires (${selectedOptions}/${availableOptions})` : `Territoires (${selectedOptions})`;
    }
    /**
     * @return {?}
     */
    getContractsPlaceholder() {
        /** @type {?} */
        const selectedOptions = this.form && (this.form.contracts || []).length;
        /** @type {?} */
        const availableOptions = this.countContractsOptions();
        return this.devMode ? `Contrats (${selectedOptions}/${availableOptions})` : `Contrats (${selectedOptions})`;
    }
    /**
     * @return {?}
     */
    countRegionsOptions() {
        if (!this.options)
            return 0;
        return this.options.regions.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.hidden)).length;
    }
    /**
     * @return {?}
     */
    countTerritoriesOptions() {
        if (!this.options)
            return 0;
        return this.options.territories.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.hidden)).length;
    }
    /**
     * @return {?}
     */
    countContractsOptions() {
        if (!this.options)
            return 0;
        return this.options.contracts.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.hidden)).length;
    }
    // APPLY -------------------------------------------------------------
    /**
     * @param {?} field
     * @return {?}
     */
    emptySearchField(field) {
        // empty search fields
        if (this.form[field].length) {
            this.form[field] = '';
            this.updateOptions();
        }
    }
    /**
     * @return {?}
     */
    reinit() {
        /** @type {?} */
        const emptyFilter = new Filter();
        // empty search fields
        this.form.searchRegions = '';
        this.form.searchTerritories = '';
        this.form.searchContracts = '';
        this.form.searchCommunes = '';
        this.setForm(emptyFilter);
    }
    /**
     * @return {?}
     */
    _saveAsFavorite() {
        /** @type {?} */
        let filter = new Filter(this.form);
        this.saveAsFavorite.emit(filter);
    }
    /**
     * @param {?} favorite
     * @return {?}
     */
    _deleteFavorite(favorite) {
        this.deleteFavorite.emit(favorite);
    }
    /**
     * @return {?}
     */
    applyFilter() {
        /** @type {?} */
        let filter = new Filter(this.form);
        this.filterChanged.emit(filter);
    }
    /**
     * @param {?} favorite
     * @return {?}
     */
    applyFavorite(favorite) {
        this.setForm(favorite.getFilter());
        this.applyFilter();
        this.goToFilterTab();
    }
    /**
     * @return {?}
     */
    goToFilterTab() {
        this.selectedTab = 0;
    }
    /**
     * @return {?}
     */
    closeNav() {
        this.closed = true;
    }
    /**
     * @return {?}
     */
    openNav() {
        this.closed = false;
    }
}
FilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'teleo-filter',
                template: "<!-- HIDE/SHOW BUTTON -->\n<div [ngClass]=\"{'filter-button' : true, 'filter-button--hidden' : !closed}\" (click)=\"openNav();\">\n  <mat-icon class=\"filter-button__icon\">filter_list</mat-icon>\n</div>\n\n<div class=\"wrapper\" [ngClass]=\"{'wrapper--hidden': closed}\">\n\n  <!-- HIDE BUTTON -->\n  <mat-icon class=\"sidebar-close\" (click)=\"closeNav()\">arrow_back</mat-icon>\n\n  <mat-tab-group [(selectedIndex)]=\"selectedTab\" dynamicHeight=\"true\" animationDuration=\"200ms\"> \n    \n    <!-- FILTER ----------------------------------->\n    <mat-tab label=\"FILTRE\">\n      \n      <!-- BODY -->\n      <div class=\"filters\" [appIsLoading]=\"!options\">\n\n        <!-- REGIONS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchRegions')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getRegionsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.regions\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchRegions\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllRegions()\">Toutes</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllRegions()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let region of sortedOptions?.regions; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: region.hidden ? 'none' : 'flex'}\" [value]=\"region.id\" matTooltip=\"{{ region.name }}\"\n                matTooltipPosition=\"above\" matTooltipShowDelay=\"500\">\n                {{ region.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- TERRITOIRES -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchTerritories')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getTerritoriesPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.territories\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchTerritories\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllTerritories()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllTerritories()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let territory of sortedOptions?.territories; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: territory.hidden ? 'none' : 'flex'}\" \n                [value]=\"territory.id\" matTooltip=\"{{ territory.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ territory.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- CONTRATS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchContracts')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getContractsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.contracts\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchContracts\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllContracts()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllContracts()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let contract of sortedOptions?.contracts; trackBy: trackElement\">\n              <mat-option [value]=\"contract.id\" [ngStyle]=\"{display: contract.hidden ? 'none' : 'flex'}\" \n                matTooltip=\"{{ contract.id }} - {{ contract.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ contract.id }} - {{ contract.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n\n\n\n      </div>\n    </mat-tab>\n\n\n    <!-- FAVORITES ----------------------------------------------------->\n    <mat-tab label=\"FAVORIS\" *ngIf=\"favorites\">\n      <div class=\"favorites\" [appIsLoading]=\"isFetchingFavorites$ | async\">\n        <div class=\"favorite\" *ngFor=\"let favorite of (favorites | keyvalue)\">\n          <div class=\"favorite__label\" matRipple matTooltip=\"{{ favorite?.value?.getLabel() }}\" matTooltipPosition=\"above\" matTooltipShowDelay=\"500\" (click)=\"applyFavorite(favorite.value)\">{{ favorite?.value?.getLabel() }}</div>\n          <div class=\"favorite__delete\" matRipple (click)=\"_deleteFavorite(favorite.value)\"><mat-icon>delete</mat-icon></div>\n        </div>\n      </div>\n      \n    </mat-tab>\n  </mat-tab-group>\n\n  <!-- FOOTER -->\n  <div [ngClass]=\"{'filter-footer--hidden': selectedTab !== 0, 'filter-footer': true}\">\n    <button mat-stroked-button color=\"primary\" (click)=\"reinit();\">R\u00C9INITIALISER LES FILTRES</button>\n    <button mat-stroked-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"_saveAsFavorite()\">SAUVEGARDER</button>\n    <button mat-raised-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"applyFilter()\">APPLIQUER</button>\n  </div>\n</div>",
                styles: [":host{position:relative}:host ::ng-deep .mat-tab-label{min-width:115px;font-family:TheSans,Roboto,sans-serif;font-size:16px;line-height:22px;font-weight:600;height:72px;color:#353535;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}:host ::ng-deep .mat-tab-label.mat-tab-label-active{color:#353535;opacity:1}.dropdown-controls{display:-webkit-box;display:flex;position:-webkit-sticky;position:sticky;top:0;background:#fff;z-index:10;box-shadow:0 0 3px 1px #c5c5c5}.filter-button{position:absolute;top:0;left:0;width:36px;height:72px;background-color:#804180;border-radius:0 16px 16px 0;box-shadow:0 2px 4px 0 rgba(0,0,0,.5);color:#fff;cursor:pointer;z-index:90;-webkit-transition:left .2s;transition:left .2s}.filter-button__icon{margin-top:24px;margin-left:4px}.filter-button--hidden{left:-40px}.filters{padding:25px;display:block;overflow-y:auto;opacity:1}.favorites{display:block;overflow-y:auto;opacity:1;min-height:220px;max-height:50vh;overflow:auto}.favorites:empty{display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.favorites:empty::before{content:\"Vous n'avez pas de favoris\"}.favorite{display:-webkit-box;display:flex;width:100%;-webkit-box-align:stretch;align-items:stretch;border-bottom:#f0f0f0;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer}.favorite:hover{background:#f0f0f0}.favorite__label{padding:15px 0 15px 15px;font-family:TheSans,Roboto,sans-serif;-webkit-box-flex:1;flex-grow:1;text-overflow:ellipsis;white-space:nowrap;overflow:hidden}.favorite__delete{cursor:pointer;width:50px;height:50px;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center;display:-webkit-box;display:flex;flex-shrink:0}.favorite__delete:hover{color:#e0421f}.wrapper{margin-left:15px;width:300px;max-width:300px;z-index:90;border-radius:0 20px;box-shadow:0 2px 10px 0 rgba(0,0,0,.24);font-family:TheSans,Roboto,sans-serif;background:#fff;-webkit-transition:width .2s,left .2s,margin .2s;transition:width .2s,left .2s,margin .2s;position:relative;left:0;overflow:hidden}.wrapper--hidden{width:0;left:-300px;margin-left:0;margin-right:51px}.w100{width:100%}.sidebar-close{position:absolute;right:28px;top:24px;font-size:20px;cursor:pointer;font-weight:700;color:#000;z-index:100}.filter-footer{font-weight:600;max-height:350px;padding:24px;border-top:1px solid #ccc;border-bottom-left-radius:20px;overflow:hidden;-webkit-transition:max-height .2s,padding .2s;transition:max-height .2s,padding .2s}.filter-footer button{width:100%;display:block;margin:0 auto 9.5px;height:37px}.filter-footer--hidden{max-height:0;padding:0}.checkbox{margin-top:5px;padding:0;box-sizing:border-box;font-size:13px;margin-bottom:30px}.checkbox ::ng-deep .mat-checkbox-label{white-space:normal}.search{width:100%;border:none;padding:10px 15px}.search:focus{outline:0}"]
            }] }
];
/** @nocollapse */
FilterComponent.ctorParameters = () => [];
FilterComponent.propDecorators = {
    options: [{ type: Input, args: ['options',] }],
    filter: [{ type: Input, args: ['filter',] }],
    favorites: [{ type: Input, args: ['favorites',] }],
    isFetchingFavorites$: [{ type: Input, args: ['isFetchingFavorites$',] }],
    filterChanged: [{ type: Output, args: ['filterChanged',] }],
    deleteFavorite: [{ type: Output, args: ['deleteFavorite',] }],
    saveAsFavorite: [{ type: Output, args: ['saveAsFavorite',] }]
};
if (false) {
    /** @type {?} */
    FilterComponent.prototype.options;
    /** @type {?} */
    FilterComponent.prototype.filter;
    /** @type {?} */
    FilterComponent.prototype.favorites;
    /** @type {?} */
    FilterComponent.prototype.isFetchingFavorites$;
    /** @type {?} */
    FilterComponent.prototype.filterChanged;
    /** @type {?} */
    FilterComponent.prototype.deleteFavorite;
    /** @type {?} */
    FilterComponent.prototype.saveAsFavorite;
    /** @type {?} */
    FilterComponent.prototype.sortedOptions;
    /** @type {?} */
    FilterComponent.prototype.devMode;
    /** @type {?} */
    FilterComponent.prototype.closed;
    /** @type {?} */
    FilterComponent.prototype.selectedTab;
    /** @type {?} */
    FilterComponent.prototype.form;
}
class String {
    /**
     * return true if each terms (space-separated) of $needle
     * are present in $haystack
     * @param {?} needle
     * @param {?} haystack
     * @return {?}
     */
    static search(needle, haystack) {
        // clean strings
        needle = this.replaceAccentedCharacters(needle.toLowerCase());
        haystack = this.replaceAccentedCharacters(haystack.toLowerCase());
        // split string in search terms
        /** @type {?} */
        const terms = needle.split(/\s+/);
        // check each term is present
        for (let term of terms) {
            if (!haystack.includes(term))
                return false;
        }
        return true;
    }
    /**
     * @param {?} str
     * @return {?}
     */
    static replaceAccentedCharacters(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/common.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CommonService {
    constructor() { }
}
CommonService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CommonService.ctorParameters = () => [];
/** @nocollapse */ CommonService.ngInjectableDef = ɵɵdefineInjectable({ factory: function CommonService_Factory() { return new CommonService(); }, token: CommonService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: lib/common.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CommonComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
CommonComponent.decorators = [
    { type: Component, args: [{
                selector: 'teleo-common',
                template: `
    <p>
      common works!
    </p>
  `
            }] }
];
/** @nocollapse */
CommonComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/loading-overlay/loading-overlay.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoadingOverlayComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
LoadingOverlayComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-loading-overlay',
                template: "<mat-spinner diameter=\"20\"></mat-spinner>\n",
                styles: [":host{position:absolute;top:0;left:0;bottom:0;right:0;background:rgba(255,255,255,.7);display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center;pointer-events:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}"]
            }] }
];
/** @nocollapse */
LoadingOverlayComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/is-loading.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class IsLoadingDirective {
    /**
     * @param {?} viewContainerRef
     * @param {?} componentFactoryResolver
     */
    constructor(viewContainerRef, componentFactoryResolver) {
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.isLoading = false;
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.isLoading ? this.addLoader() : this.removeLoader();
    }
    /**
     * @return {?}
     */
    removeLoader() {
        this.loader && this.loader.destroy();
    }
    /**
     * @return {?}
     */
    addLoader() {
        if (this.loader)
            return;
        /** @type {?} */
        const factory = this.componentFactoryResolver.resolveComponentFactory(LoadingOverlayComponent);
        this.loader = this.viewContainerRef.createComponent(factory);
    }
}
IsLoadingDirective.decorators = [
    { type: Directive, args: [{
                selector: '[appIsLoading]'
            },] }
];
/** @nocollapse */
IsLoadingDirective.ctorParameters = () => [
    { type: ViewContainerRef },
    { type: ComponentFactoryResolver }
];
IsLoadingDirective.propDecorators = {
    isLoading: [{ type: Input, args: ['appIsLoading',] }]
};
if (false) {
    /** @type {?} */
    IsLoadingDirective.prototype.loader;
    /** @type {?} */
    IsLoadingDirective.prototype.isLoading;
    /**
     * @type {?}
     * @private
     */
    IsLoadingDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @private
     */
    IsLoadingDirective.prototype.componentFactoryResolver;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/common.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CommonModule {
}
CommonModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CommonComponent,
                    FilterComponent,
                    LoadingOverlayComponent,
                    IsLoadingDirective,
                ],
                imports: [
                    BrowserAnimationsModule,
                    CommonModule$1,
                    MatIconModule,
                    MatTabsModule,
                    MatSelectModule,
                    MatFormFieldModule,
                    MatButtonModule,
                    FormsModule,
                    MatRippleModule,
                    MatTooltipModule,
                    MatProgressSpinnerModule,
                ],
                exports: [
                    CommonComponent,
                    FilterComponent,
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/models/favorite.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Favorite {
    /**
     * @param {?=} data
     */
    constructor(data = {}) {
        this.setId(data.id);
        this.setFilter(data.filter || new Filter());
        this.setLabel(data.label || '');
    }
    // GETTERS -------------------------------------------
    /**
     * @return {?}
     */
    getId() {
        return this._id;
    }
    /**
     * @return {?}
     */
    getFilter() {
        return this._filter || new Filter();
    }
    /**
     * @return {?}
     */
    getLabel() {
        return this._label || '';
    }
    // SETTERS -------------------------------------------
    /**
     * @param {?} id
     * @return {?}
     */
    setId(id) {
        this._id = id;
        return this;
    }
    /**
     * @param {?} filter
     * @return {?}
     */
    setFilter(filter) {
        this._filter = filter;
        return this;
    }
    /**
     * @param {?} label
     * @return {?}
     */
    setLabel(label) {
        this._label = label;
        return this;
    }
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    static fromJSON(json) {
        json.filter = Filter.fromJSON(json.filter);
        return new Favorite(json);
    }
    /**
     * @return {?}
     */
    toObject() {
        /** @type {?} */
        let o = {
            label: this.getLabel(),
            filter: this.getFilter(),
        };
        if (this.getId())
            o.id = this.getId();
        return o;
    }
    /**
     * @return {?}
     */
    toJSON() {
        return JSON.stringify(this.toObject());
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._id;
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._filter;
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._label;
}

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: teleo-common.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { CommonComponent, CommonModule, CommonService, Favorite, Filter, FilterComponent, LoadingOverlayComponent as ɵa, IsLoadingDirective as ɵb };
//# sourceMappingURL=teleo-common.js.map
