(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('lodash'), require('@angular/common'), require('@angular/material'), require('@angular/material/tabs'), require('@angular/material/select'), require('@angular/material/form-field'), require('@angular/material/button'), require('@angular/forms'), require('@angular/material/tooltip'), require('@angular/material/core'), require('@angular/material/progress-spinner'), require('@angular/platform-browser/animations')) :
    typeof define === 'function' && define.amd ? define('@teleo/common', ['exports', '@angular/core', 'rxjs', 'lodash', '@angular/common', '@angular/material', '@angular/material/tabs', '@angular/material/select', '@angular/material/form-field', '@angular/material/button', '@angular/forms', '@angular/material/tooltip', '@angular/material/core', '@angular/material/progress-spinner', '@angular/platform-browser/animations'], factory) :
    (global = global || self, factory((global.teleo = global.teleo || {}, global.teleo.common = {}), global.ng.core, global.rxjs, global.lodash, global.ng.common, global.ng.material, global.ng.material.tabs, global.ng.material.select, global.ng.material['form-field'], global.ng.material.button, global.ng.forms, global.ng.material.tooltip, global.ng.material.core, global.ng.material['progress-spinner'], global.ng.platformBrowser.animations));
}(this, (function (exports, core, rxjs, lodash, common, material, tabs, select, formField, button, forms, tooltip, core$1, progressSpinner, animations) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/models/filter.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var Filter = /** @class */ (function () {
        function Filter(data) {
            if (data === void 0) { data = {}; }
            // this.setDate(data.date || Filter.defaults.date);
            // this.setTypes(data.types || Filter.defaults.type);
            // this.setModes(data.modes || Filter.defaults.modes);
            this.setRegions(data.regions || Filter.defaults.regions);
            this.setTerritories(data.territories || Filter.defaults.territories);
            this.setContracts(data.contracts || Filter.defaults.contracts);
            this.setCommunes(data.communes || Filter.defaults.communes);
        }
        // ------------------------------------------------
        // ------------------------------------------------
        /**
         * @return {?}
         */
        Filter.prototype.isEmpty = 
        // ------------------------------------------------
        /**
         * @return {?}
         */
        function () {
            return (this.getRegions().length === 0
                && this.getTerritories().length === 0
                && this.getContracts().length === 0
                && this.getCommunes().length === 0);
        };
        /**
         * @return {?}
         */
        Filter.prototype.isSingleContract = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var isSingle = (this.getContracts().length === 1 && this.getCommunes().length === 0);
            return isSingle ? this.singleContract() : false;
        };
        /**
         * @return {?}
         */
        Filter.prototype.singleContract = /**
         * @return {?}
         */
        function () {
            return this.getContracts()[0];
        };
        /**
         * @return {?}
         */
        Filter.prototype.isSingleCommune = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var isSingle = (this.getCommunes().length === 1);
            return isSingle ? this.getCommunes()[0] : false;
        };
        // GETTERS -------------------------------------------
        // public getDate() {
        //     return this._date || Filter.defaults.date;
        // }
        // public getDateParam() {
        //     return moment(this.getDate()).format('YYYY-MM-DD');
        // }
        // public getTypes(): string[] {
        //     return this._types || Filter.defaults.types;
        // }
        // public getModes(): string[] {
        //     return this._modes || Filter.defaults.modes;
        // }
        // GETTERS -------------------------------------------
        // public getDate() {
        //     return this._date || Filter.defaults.date;
        // }
        // public getDateParam() {
        //     return moment(this.getDate()).format('YYYY-MM-DD');
        // }
        // public getTypes(): string[] {
        //     return this._types || Filter.defaults.types;
        // }
        // public getModes(): string[] {
        //     return this._modes || Filter.defaults.modes;
        // }
        /**
         * @return {?}
         */
        Filter.prototype.getRegions = 
        // GETTERS -------------------------------------------
        // public getDate() {
        //     return this._date || Filter.defaults.date;
        // }
        // public getDateParam() {
        //     return moment(this.getDate()).format('YYYY-MM-DD');
        // }
        // public getTypes(): string[] {
        //     return this._types || Filter.defaults.types;
        // }
        // public getModes(): string[] {
        //     return this._modes || Filter.defaults.modes;
        // }
        /**
         * @return {?}
         */
        function () {
            return this._regions || Filter.defaults.regions;
        };
        /**
         * @return {?}
         */
        Filter.prototype.getTerritories = /**
         * @return {?}
         */
        function () {
            return this._territories || Filter.defaults.territories;
        };
        /**
         * @return {?}
         */
        Filter.prototype.getContracts = /**
         * @return {?}
         */
        function () {
            return this._contracts || Filter.defaults.contracts;
        };
        /**
         * @return {?}
         */
        Filter.prototype.getCommunes = /**
         * @return {?}
         */
        function () {
            return this._communes || Filter.defaults.communes;
        };
        // SETTERS -------------------------------------------
        // public setDate(date: Date) {
        //     this._date = date;
        // }
        // public setTypes(types: string[]) {
        //     this._types = types;
        // }
        // public setModes(modes: string[]) {
        //     this._modes = modes
        // }
        // SETTERS -------------------------------------------
        // public setDate(date: Date) {
        //     this._date = date;
        // }
        // public setTypes(types: string[]) {
        //     this._types = types;
        // }
        // public setModes(modes: string[]) {
        //     this._modes = modes
        // }
        /**
         * @param {?} regions
         * @return {?}
         */
        Filter.prototype.setRegions = 
        // SETTERS -------------------------------------------
        // public setDate(date: Date) {
        //     this._date = date;
        // }
        // public setTypes(types: string[]) {
        //     this._types = types;
        // }
        // public setModes(modes: string[]) {
        //     this._modes = modes
        // }
        /**
         * @param {?} regions
         * @return {?}
         */
        function (regions) {
            this._regions = regions;
        };
        /**
         * @param {?} territories
         * @return {?}
         */
        Filter.prototype.setTerritories = /**
         * @param {?} territories
         * @return {?}
         */
        function (territories) {
            this._territories = territories;
        };
        /**
         * @param {?} contracts
         * @return {?}
         */
        Filter.prototype.setContracts = /**
         * @param {?} contracts
         * @return {?}
         */
        function (contracts) {
            this._contracts = contracts;
        };
        /**
         * @param {?} communes
         * @return {?}
         */
        Filter.prototype.setCommunes = /**
         * @param {?} communes
         * @return {?}
         */
        function (communes) {
            this._communes = communes;
        };
        /**
         * @param {?} filter
         * @return {?}
         */
        Filter.prototype.isEqualTo = /**
         * @param {?} filter
         * @return {?}
         */
        function (filter) {
            return (this.toJSON() === filter.toJSON());
        };
        /**
         * @return {?}
         */
        Filter.prototype.hasPerimeter = /**
         * @return {?}
         */
        function () {
            return (this.getRegions().length || this.getTerritories().length || this.getContracts().length);
        };
        // SERIALIZATION -------------------------------------------
        // SERIALIZATION -------------------------------------------
        /**
         * @param {?} json
         * @return {?}
         */
        Filter.fromJSON = 
        // SERIALIZATION -------------------------------------------
        /**
         * @param {?} json
         * @return {?}
         */
        function (json) {
            return new Filter(json);
        };
        /**
         * @return {?}
         */
        Filter.prototype.toObject = /**
         * @return {?}
         */
        function () {
            return {
                regions: this.getRegions(),
                territories: this.getTerritories(),
                contracts: this.getContracts(),
                communes: this.getCommunes(),
            };
        };
        /**
         * @return {?}
         */
        Filter.prototype.toJSON = /**
         * @return {?}
         */
        function () {
            return JSON.stringify(this.toObject());
        };
        /**
         * @return {?}
         */
        Filter.prototype.toStorage = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var json = this.toJSON();
            localStorage.setItem("filter", json);
        };
        /**
         * @return {?}
         */
        Filter.fromStorage = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var filter = new Filter();
            // fill from storage
            /** @type {?} */
            var params = localStorage.getItem("filter");
            if (params) {
                params = JSON.parse(params);
                filter.setRegions(params['regions'] || this.defaults.regions);
                filter.setTerritories(params['territories'] || this.defaults.territories);
                filter.setContracts(params['contracts'] || this.defaults.contracts);
                filter.setCommunes(params['communes'] || this.defaults.communes);
                // filter.setDate(new Date(params['date'] || this.defaults.date));
                // filter.setTypes(params['types'] || this.defaults.types);
                // filter.setModes(params['modes'] || this.defaults.modes);
            }
            return filter;
        };
        /**
         * @return {?}
         */
        Filter.prototype.clearStorage = /**
         * @return {?}
         */
        function () {
            localStorage.removeItem("filter");
        };
        // QUERY PARAMS --------------------------------------------------------
        // QUERY PARAMS --------------------------------------------------------
        /**
         * @param {?=} fields
         * @return {?}
         */
        Filter.prototype.toParams = 
        // QUERY PARAMS --------------------------------------------------------
        /**
         * @param {?=} fields
         * @return {?}
         */
        function (fields) {
            if (fields === void 0) { fields = null; }
            /** @type {?} */
            var params = {
                // day: this.getDateParam(),
                // types: this.getTypes().join(','),
                // modes: this.getModes().join(','),
                regions: this.getRegions().join(','),
                territories: this.getTerritories().join(','),
                contracts: this.getContracts().join(','),
                communes: this.getCommunes().join(','),
            }
            // pick the fields we want
            ;
            // pick the fields we want
            if (fields)
                params = lodash.pick(params, fields);
            return params;
        };
        Filter.defaults = {
            // date: new Date(),
            // types: ['DBO', 'OM'],
            // modes: ['RR', 'TR'],
            regions: [],
            territories: [],
            contracts: [],
            communes: [],
        };
        return Filter;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        Filter.defaults;
        /**
         * @type {?}
         * @private
         */
        Filter.prototype._date;
        /**
         * @type {?}
         * @private
         */
        Filter.prototype._types;
        /**
         * @type {?}
         * @private
         */
        Filter.prototype._modes;
        /**
         * @type {?}
         * @private
         */
        Filter.prototype._regions;
        /**
         * @type {?}
         * @private
         */
        Filter.prototype._territories;
        /**
         * @type {?}
         * @private
         */
        Filter.prototype._contracts;
        /**
         * @type {?}
         * @private
         */
        Filter.prototype._communes;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/filter/filter.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FilterComponent = /** @class */ (function () {
        function FilterComponent() {
            this.options = null; // don't modify this object, except the ".hidden" properties
            this.filterChanged = new core.EventEmitter();
            this.deleteFavorite = new core.EventEmitter();
            this.saveAsFavorite = new core.EventEmitter();
            this.sortedOptions = null;
            this.devMode = false;
            this.closed = false;
            this.selectedTab = 0;
            this.form = {
                contracts: [],
                regions: [],
                territories: [],
                communes: [],
                searchRegions: '',
                searchTerritories: '',
                searchContracts: '',
                searchCommunes: '',
            };
        }
        /**
         * @return {?}
         */
        FilterComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        FilterComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (changes.filter)
                this.setForm(this.filter);
            if (changes.options) {
                this.options = lodash.cloneDeep(this.options);
                this.updateOptions();
            }
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.isSubmittable = /**
         * @return {?}
         */
        function () {
            return this.form.regions.length || this.form.territories.length || this.form.contracts.length;
        };
        /**
         * @param {?} filter
         * @return {?}
         */
        FilterComponent.prototype.setForm = /**
         * @param {?} filter
         * @return {?}
         */
        function (filter) {
            this.form.contracts = filter.getContracts();
            this.form.regions = filter.getRegions();
            this.form.territories = filter.getTerritories();
            this.form.communes = filter.getCommunes();
            this.onChange();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.onChange = /**
         * @return {?}
         */
        function () {
            // clean user selection (remove impossible cases)
            this.cleanSelection();
            this.updateOptions();
        };
        // -----------------------------------------
        // -----------------------------------------
        /**
         * @return {?}
         */
        FilterComponent.prototype.cleanSelection = 
        // -----------------------------------------
        /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.options)
                return;
            /** @type {?} */
            var filter = new Filter(this.form);
            /** @type {?} */
            var cleanedFilter = lodash.cloneDeep(filter);
            /** @type {?} */
            var currentTerritories = cleanedFilter.getTerritories().map((/**
             * @param {?} id
             * @return {?}
             */
            function (id) { return _this.options.territories.find((/**
             * @param {?} t
             * @return {?}
             */
            function (t) { return t.id == id; })); }));
            /** @type {?} */
            var currentContracts = cleanedFilter.getContracts().map((/**
             * @param {?} id
             * @return {?}
             */
            function (id) { return _this.options.contracts.find((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.id == id; })); }));
            /** @type {?} */
            var currentCommunes = cleanedFilter.getCommunes().map((/**
             * @param {?} id
             * @return {?}
             */
            function (id) { return _this.options.communes.find((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.id == id; })); }));
            // filter by regions
            if (cleanedFilter.getRegions().length) {
                cleanedFilter.setTerritories(currentTerritories.filter((/**
                 * @param {?} t
                 * @return {?}
                 */
                function (t) { return cleanedFilter.getRegions().includes(t.region_id); })).map((/**
                 * @param {?} t
                 * @return {?}
                 */
                function (t) { return t.id; })));
                cleanedFilter.setContracts(currentContracts.filter((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return cleanedFilter.getRegions().includes(c.region_id); })).map((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return c.id; })));
                cleanedFilter.setCommunes(currentCommunes.filter((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return cleanedFilter.getRegions().includes(c.region_id); })).map((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return c.id; })));
            }
            // filter by territories
            if (cleanedFilter.getTerritories().length) {
                cleanedFilter.setContracts(currentContracts.filter((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return cleanedFilter.getTerritories().includes(c.territory_id); })).map((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return c.id; })));
                cleanedFilter.setCommunes(currentCommunes.filter((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return cleanedFilter.getTerritories().includes(c.territory_id); })).map((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return c.id; })));
            }
            // filter by contracts
            if (cleanedFilter.getContracts().length) {
                cleanedFilter.setCommunes(currentCommunes.filter((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return cleanedFilter.getContracts().includes(c.contract_id); })).map((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return c.id; })));
            }
            // update form if the cleaned version of the filter is different
            if (!cleanedFilter.isEqualTo(filter))
                this.setForm(cleanedFilter);
        };
        // -----------------------------------------
        // -----------------------------------------
        /**
         * @return {?}
         */
        FilterComponent.prototype.updateOptions = 
        // -----------------------------------------
        /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.options)
                return;
            /** @type {?} */
            var filter = new Filter(this.form);
            /** @type {?} */
            var filterRegions = filter.getRegions();
            /** @type {?} */
            var filterTerritories = filter.getTerritories();
            /** @type {?} */
            var filterContracts = filter.getContracts();
            // filter region options
            this.options.regions.map((/**
             * @param {?} r
             * @return {?}
             */
            function (r) {
                /** @type {?} */
                var passSearchFilter = !_this.form.searchRegions.length || (String.search(_this.form.searchRegions, r.name) || String.search(_this.form.searchRegions, r.id));
                /** @type {?} */
                var pass = passSearchFilter;
                r.hidden = !pass;
                return r;
            }));
            // filter territory options
            this.options.territories.map((/**
             * @param {?} t
             * @return {?}
             */
            function (t) {
                /** @type {?} */
                var passRegionFilter = !filterRegions.length || filterRegions.includes(t.region_id);
                /** @type {?} */
                var passSearchFilter = !_this.form.searchTerritories.length || (String.search(_this.form.searchTerritories, t.name) || String.search(_this.form.searchTerritories, t.id));
                /** @type {?} */
                var pass = passRegionFilter && passSearchFilter;
                /** @type {?} */
                var hidden = !pass;
                if (t.hidden !== hidden)
                    t.hidden = hidden;
                return t;
            }));
            // filter contract options
            this.options.contracts.map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) {
                /** @type {?} */
                var passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
                /** @type {?} */
                var passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
                /** @type {?} */
                var passSearchFilter = !_this.form.searchContracts.length || (String.search(_this.form.searchContracts, c.name) || String.search(_this.form.searchContracts, c.id));
                /** @type {?} */
                var pass = passRegionFilter && passTerritoryFilter && passSearchFilter;
                /** @type {?} */
                var hidden = !pass;
                if (c.hidden !== hidden)
                    c.hidden = hidden;
                return c;
            }));
            // filter commune options
            this.options.communes.map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) {
                /** @type {?} */
                var passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
                /** @type {?} */
                var passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
                /** @type {?} */
                var passContractFilter = !filterContracts.length || filterContracts.includes(c.contract_id);
                /** @type {?} */
                var passSearchFilter = !_this.form.searchCommunes.length || (String.search(_this.form.searchCommunes, c.name) || String.search(_this.form.searchCommunes, c.id));
                /** @type {?} */
                var pass = passRegionFilter && passTerritoryFilter && passContractFilter && passSearchFilter;
                /** @type {?} */
                var hidden = !pass;
                if (c.hidden !== hidden)
                    c.hidden = hidden;
                return c;
            }));
            // Below, we put hidden options at the end, so that mat-select's scrolling behaves correctly
            // 1. this way we also can use "visibility" css property which is faster than *ngIf which would insert/removes DOM nodes
            // 2. this way, using the search field doesn't remove our previous selection
            this.sortedOptions = {
                regions: this.options.regions.sort((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
                territories: this.options.territories.sort((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
                contracts: this.options.contracts.sort((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
                communes: this.options.communes.sort((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
            };
        };
        /**
         * @param {?} index
         * @param {?} element
         * @return {?}
         */
        FilterComponent.prototype.trackElement = /**
         * @param {?} index
         * @param {?} element
         * @return {?}
         */
        function (index, element) {
            return element ? element.id : null;
        };
        // SELECT / DESELECT --------------------------------------------
        // SELECT / DESELECT --------------------------------------------
        /**
         * @return {?}
         */
        FilterComponent.prototype.selectAllRegions = 
        // SELECT / DESELECT --------------------------------------------
        /**
         * @return {?}
         */
        function () {
            if (!this.options)
                return;
            this.form.regions = this.options.regions.map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item.id; }));
            this.onChange();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.deselectAllRegions = /**
         * @return {?}
         */
        function () {
            this.form.regions = [];
            this.form.territories = [];
            this.form.contracts = [];
            this.form.communes = [];
            this.onChange();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.selectAllTerritories = /**
         * @return {?}
         */
        function () {
            if (!this.options)
                return;
            this.form.territories = this.options.territories.map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item.id; }));
            this.onChange();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.deselectAllTerritories = /**
         * @return {?}
         */
        function () {
            this.form.territories = [];
            this.form.contracts = [];
            this.form.communes = [];
            this.onChange();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.selectAllContracts = /**
         * @return {?}
         */
        function () {
            if (!this.options)
                return;
            this.form.contracts = this.options.contracts.map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item.id; }));
            this.onChange();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.deselectAllContracts = /**
         * @return {?}
         */
        function () {
            this.form.contracts = [];
            this.form.communes = [];
            this.onChange();
        };
        /**
         * @param {?} event
         * @return {?}
         */
        FilterComponent.prototype._handleKeydown = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            event.stopPropagation();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.getRegionsPlaceholder = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var selectedOptions = this.form && (this.form.regions || []).length;
            /** @type {?} */
            var availableOptions = this.countRegionsOptions();
            return this.devMode ? "R\u00E9gions (" + selectedOptions + "/" + availableOptions + ")" : "R\u00E9gions (" + selectedOptions + ")";
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.getTerritoriesPlaceholder = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var selectedOptions = this.form && (this.form.territories || []).length;
            /** @type {?} */
            var availableOptions = this.countTerritoriesOptions();
            return this.devMode ? "Territoires (" + selectedOptions + "/" + availableOptions + ")" : "Territoires (" + selectedOptions + ")";
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.getContractsPlaceholder = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var selectedOptions = this.form && (this.form.contracts || []).length;
            /** @type {?} */
            var availableOptions = this.countContractsOptions();
            return this.devMode ? "Contrats (" + selectedOptions + "/" + availableOptions + ")" : "Contrats (" + selectedOptions + ")";
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.countRegionsOptions = /**
         * @return {?}
         */
        function () {
            if (!this.options)
                return 0;
            return this.options.regions.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return !item.hidden; })).length;
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.countTerritoriesOptions = /**
         * @return {?}
         */
        function () {
            if (!this.options)
                return 0;
            return this.options.territories.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return !item.hidden; })).length;
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.countContractsOptions = /**
         * @return {?}
         */
        function () {
            if (!this.options)
                return 0;
            return this.options.contracts.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return !item.hidden; })).length;
        };
        // APPLY -------------------------------------------------------------
        // APPLY -------------------------------------------------------------
        /**
         * @param {?} field
         * @return {?}
         */
        FilterComponent.prototype.emptySearchField = 
        // APPLY -------------------------------------------------------------
        /**
         * @param {?} field
         * @return {?}
         */
        function (field) {
            // empty search fields
            if (this.form[field].length) {
                this.form[field] = '';
                this.updateOptions();
            }
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.reinit = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var emptyFilter = new Filter();
            // empty search fields
            this.form.searchRegions = '';
            this.form.searchTerritories = '';
            this.form.searchContracts = '';
            this.form.searchCommunes = '';
            this.setForm(emptyFilter);
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype._saveAsFavorite = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var filter = new Filter(this.form);
            this.saveAsFavorite.emit(filter);
        };
        /**
         * @param {?} favorite
         * @return {?}
         */
        FilterComponent.prototype._deleteFavorite = /**
         * @param {?} favorite
         * @return {?}
         */
        function (favorite) {
            this.deleteFavorite.emit(favorite);
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.applyFilter = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var filter = new Filter(this.form);
            this.filterChanged.emit(filter);
        };
        /**
         * @param {?} favorite
         * @return {?}
         */
        FilterComponent.prototype.applyFavorite = /**
         * @param {?} favorite
         * @return {?}
         */
        function (favorite) {
            this.setForm(favorite.getFilter());
            this.applyFilter();
            this.goToFilterTab();
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.goToFilterTab = /**
         * @return {?}
         */
        function () {
            this.selectedTab = 0;
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.closeNav = /**
         * @return {?}
         */
        function () {
            this.closed = true;
        };
        /**
         * @return {?}
         */
        FilterComponent.prototype.openNav = /**
         * @return {?}
         */
        function () {
            this.closed = false;
        };
        FilterComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'teleo-filter',
                        template: "<!-- HIDE/SHOW BUTTON -->\n<div [ngClass]=\"{'filter-button' : true, 'filter-button--hidden' : !closed}\" (click)=\"openNav();\">\n  <mat-icon class=\"filter-button__icon\">filter_list</mat-icon>\n</div>\n\n<div class=\"wrapper\" [ngClass]=\"{'wrapper--hidden': closed}\">\n\n  <!-- HIDE BUTTON -->\n  <mat-icon class=\"sidebar-close\" (click)=\"closeNav()\">arrow_back</mat-icon>\n\n  <mat-tab-group [(selectedIndex)]=\"selectedTab\" dynamicHeight=\"true\" animationDuration=\"200ms\"> \n    \n    <!-- FILTER ----------------------------------->\n    <mat-tab label=\"FILTRE\">\n      \n      <!-- BODY -->\n      <div class=\"filters\" [appIsLoading]=\"!options\">\n\n        <!-- REGIONS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchRegions')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getRegionsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.regions\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchRegions\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllRegions()\">Toutes</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllRegions()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let region of sortedOptions?.regions; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: region.hidden ? 'none' : 'flex'}\" [value]=\"region.id\" matTooltip=\"{{ region.name }}\"\n                matTooltipPosition=\"above\" matTooltipShowDelay=\"500\">\n                {{ region.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- TERRITOIRES -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchTerritories')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getTerritoriesPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.territories\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchTerritories\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllTerritories()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllTerritories()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let territory of sortedOptions?.territories; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: territory.hidden ? 'none' : 'flex'}\" \n                [value]=\"territory.id\" matTooltip=\"{{ territory.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ territory.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- CONTRATS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchContracts')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getContractsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.contracts\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchContracts\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllContracts()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllContracts()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let contract of sortedOptions?.contracts; trackBy: trackElement\">\n              <mat-option [value]=\"contract.id\" [ngStyle]=\"{display: contract.hidden ? 'none' : 'flex'}\" \n                matTooltip=\"{{ contract.id }} - {{ contract.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ contract.id }} - {{ contract.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n\n\n\n      </div>\n    </mat-tab>\n\n\n    <!-- FAVORITES ----------------------------------------------------->\n    <mat-tab label=\"FAVORIS\" *ngIf=\"favorites\">\n      <div class=\"favorites\" [appIsLoading]=\"isFetchingFavorites$ | async\">\n        <div class=\"favorite\" *ngFor=\"let favorite of (favorites | keyvalue)\">\n          <div class=\"favorite__label\" matRipple matTooltip=\"{{ favorite?.value?.getLabel() }}\" matTooltipPosition=\"above\" matTooltipShowDelay=\"500\" (click)=\"applyFavorite(favorite.value)\">{{ favorite?.value?.getLabel() }}</div>\n          <div class=\"favorite__delete\" matRipple (click)=\"_deleteFavorite(favorite.value)\"><mat-icon>delete</mat-icon></div>\n        </div>\n      </div>\n      \n    </mat-tab>\n  </mat-tab-group>\n\n  <!-- FOOTER -->\n  <div [ngClass]=\"{'filter-footer--hidden': selectedTab !== 0, 'filter-footer': true}\">\n    <button mat-stroked-button color=\"primary\" (click)=\"reinit();\">R\u00C9INITIALISER LES FILTRES</button>\n    <button mat-stroked-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"_saveAsFavorite()\">SAUVEGARDER</button>\n    <button mat-raised-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"applyFilter()\">APPLIQUER</button>\n  </div>\n</div>",
                        styles: [":host{position:relative}:host ::ng-deep .mat-tab-label{min-width:115px;font-family:TheSans,Roboto,sans-serif;font-size:16px;line-height:22px;font-weight:600;height:72px;color:#353535;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}:host ::ng-deep .mat-tab-label.mat-tab-label-active{color:#353535;opacity:1}.dropdown-controls{display:-webkit-box;display:flex;position:-webkit-sticky;position:sticky;top:0;background:#fff;z-index:10;box-shadow:0 0 3px 1px #c5c5c5}.filter-button{position:absolute;top:0;left:0;width:36px;height:72px;background-color:#804180;border-radius:0 16px 16px 0;box-shadow:0 2px 4px 0 rgba(0,0,0,.5);color:#fff;cursor:pointer;z-index:90;-webkit-transition:left .2s;transition:left .2s}.filter-button__icon{margin-top:24px;margin-left:4px}.filter-button--hidden{left:-40px}.filters{padding:25px;display:block;overflow-y:auto;opacity:1}.favorites{display:block;overflow-y:auto;opacity:1;min-height:220px;max-height:50vh;overflow:auto}.favorites:empty{display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.favorites:empty::before{content:\"Vous n'avez pas de favoris\"}.favorite{display:-webkit-box;display:flex;width:100%;-webkit-box-align:stretch;align-items:stretch;border-bottom:#f0f0f0;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer}.favorite:hover{background:#f0f0f0}.favorite__label{padding:15px 0 15px 15px;font-family:TheSans,Roboto,sans-serif;-webkit-box-flex:1;flex-grow:1;text-overflow:ellipsis;white-space:nowrap;overflow:hidden}.favorite__delete{cursor:pointer;width:50px;height:50px;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center;display:-webkit-box;display:flex;flex-shrink:0}.favorite__delete:hover{color:#e0421f}.wrapper{margin-left:15px;width:300px;max-width:300px;z-index:90;border-radius:0 20px;box-shadow:0 2px 10px 0 rgba(0,0,0,.24);font-family:TheSans,Roboto,sans-serif;background:#fff;-webkit-transition:width .2s,left .2s,margin .2s;transition:width .2s,left .2s,margin .2s;position:relative;left:0;overflow:hidden}.wrapper--hidden{width:0;left:-300px;margin-left:0;margin-right:51px}.w100{width:100%}.sidebar-close{position:absolute;right:28px;top:24px;font-size:20px;cursor:pointer;font-weight:700;color:#000;z-index:100}.filter-footer{font-weight:600;max-height:350px;padding:24px;border-top:1px solid #ccc;border-bottom-left-radius:20px;overflow:hidden;-webkit-transition:max-height .2s,padding .2s;transition:max-height .2s,padding .2s}.filter-footer button{width:100%;display:block;margin:0 auto 9.5px;height:37px}.filter-footer--hidden{max-height:0;padding:0}.checkbox{margin-top:5px;padding:0;box-sizing:border-box;font-size:13px;margin-bottom:30px}.checkbox ::ng-deep .mat-checkbox-label{white-space:normal}.search{width:100%;border:none;padding:10px 15px}.search:focus{outline:0}"]
                    }] }
        ];
        /** @nocollapse */
        FilterComponent.ctorParameters = function () { return []; };
        FilterComponent.propDecorators = {
            options: [{ type: core.Input, args: ['options',] }],
            filter: [{ type: core.Input, args: ['filter',] }],
            favorites: [{ type: core.Input, args: ['favorites',] }],
            isFetchingFavorites$: [{ type: core.Input, args: ['isFetchingFavorites$',] }],
            filterChanged: [{ type: core.Output, args: ['filterChanged',] }],
            deleteFavorite: [{ type: core.Output, args: ['deleteFavorite',] }],
            saveAsFavorite: [{ type: core.Output, args: ['saveAsFavorite',] }]
        };
        return FilterComponent;
    }());
    if (false) {
        /** @type {?} */
        FilterComponent.prototype.options;
        /** @type {?} */
        FilterComponent.prototype.filter;
        /** @type {?} */
        FilterComponent.prototype.favorites;
        /** @type {?} */
        FilterComponent.prototype.isFetchingFavorites$;
        /** @type {?} */
        FilterComponent.prototype.filterChanged;
        /** @type {?} */
        FilterComponent.prototype.deleteFavorite;
        /** @type {?} */
        FilterComponent.prototype.saveAsFavorite;
        /** @type {?} */
        FilterComponent.prototype.sortedOptions;
        /** @type {?} */
        FilterComponent.prototype.devMode;
        /** @type {?} */
        FilterComponent.prototype.closed;
        /** @type {?} */
        FilterComponent.prototype.selectedTab;
        /** @type {?} */
        FilterComponent.prototype.form;
    }
    var String = /** @class */ (function () {
        function String() {
        }
        /**
         * return true if each terms (space-separated) of $needle
         * are present in $haystack
         */
        /**
         * return true if each terms (space-separated) of $needle
         * are present in $haystack
         * @param {?} needle
         * @param {?} haystack
         * @return {?}
         */
        String.search = /**
         * return true if each terms (space-separated) of $needle
         * are present in $haystack
         * @param {?} needle
         * @param {?} haystack
         * @return {?}
         */
        function (needle, haystack) {
            var e_1, _a;
            // clean strings
            needle = this.replaceAccentedCharacters(needle.toLowerCase());
            haystack = this.replaceAccentedCharacters(haystack.toLowerCase());
            // split string in search terms
            /** @type {?} */
            var terms = needle.split(/\s+/);
            try {
                // check each term is present
                for (var terms_1 = __values(terms), terms_1_1 = terms_1.next(); !terms_1_1.done; terms_1_1 = terms_1.next()) {
                    var term = terms_1_1.value;
                    if (!haystack.includes(term))
                        return false;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (terms_1_1 && !terms_1_1.done && (_a = terms_1.return)) _a.call(terms_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            return true;
        };
        /**
         * @param {?} str
         * @return {?}
         */
        String.replaceAccentedCharacters = /**
         * @param {?} str
         * @return {?}
         */
        function (str) {
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        };
        return String;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/common.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CommonService = /** @class */ (function () {
        function CommonService() {
        }
        CommonService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        CommonService.ctorParameters = function () { return []; };
        /** @nocollapse */ CommonService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function CommonService_Factory() { return new CommonService(); }, token: CommonService, providedIn: "root" });
        return CommonService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/common.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CommonComponent = /** @class */ (function () {
        function CommonComponent() {
        }
        /**
         * @return {?}
         */
        CommonComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        CommonComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'teleo-common',
                        template: "\n    <p>\n      common works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        CommonComponent.ctorParameters = function () { return []; };
        return CommonComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/loading-overlay/loading-overlay.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoadingOverlayComponent = /** @class */ (function () {
        function LoadingOverlayComponent() {
        }
        /**
         * @return {?}
         */
        LoadingOverlayComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        LoadingOverlayComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'app-loading-overlay',
                        template: "<mat-spinner diameter=\"20\"></mat-spinner>\n",
                        styles: [":host{position:absolute;top:0;left:0;bottom:0;right:0;background:rgba(255,255,255,.7);display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center;pointer-events:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}"]
                    }] }
        ];
        /** @nocollapse */
        LoadingOverlayComponent.ctorParameters = function () { return []; };
        return LoadingOverlayComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/directives/is-loading.directive.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var IsLoadingDirective = /** @class */ (function () {
        function IsLoadingDirective(viewContainerRef, componentFactoryResolver) {
            this.viewContainerRef = viewContainerRef;
            this.componentFactoryResolver = componentFactoryResolver;
            this.isLoading = false;
        }
        /**
         * @return {?}
         */
        IsLoadingDirective.prototype.ngOnChanges = /**
         * @return {?}
         */
        function () {
            this.isLoading ? this.addLoader() : this.removeLoader();
        };
        /**
         * @return {?}
         */
        IsLoadingDirective.prototype.removeLoader = /**
         * @return {?}
         */
        function () {
            this.loader && this.loader.destroy();
        };
        /**
         * @return {?}
         */
        IsLoadingDirective.prototype.addLoader = /**
         * @return {?}
         */
        function () {
            if (this.loader)
                return;
            /** @type {?} */
            var factory = this.componentFactoryResolver.resolveComponentFactory(LoadingOverlayComponent);
            this.loader = this.viewContainerRef.createComponent(factory);
        };
        IsLoadingDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[appIsLoading]'
                    },] }
        ];
        /** @nocollapse */
        IsLoadingDirective.ctorParameters = function () { return [
            { type: core.ViewContainerRef },
            { type: core.ComponentFactoryResolver }
        ]; };
        IsLoadingDirective.propDecorators = {
            isLoading: [{ type: core.Input, args: ['appIsLoading',] }]
        };
        return IsLoadingDirective;
    }());
    if (false) {
        /** @type {?} */
        IsLoadingDirective.prototype.loader;
        /** @type {?} */
        IsLoadingDirective.prototype.isLoading;
        /**
         * @type {?}
         * @private
         */
        IsLoadingDirective.prototype.viewContainerRef;
        /**
         * @type {?}
         * @private
         */
        IsLoadingDirective.prototype.componentFactoryResolver;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/common.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CommonModule = /** @class */ (function () {
        function CommonModule() {
        }
        CommonModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [
                            CommonComponent,
                            FilterComponent,
                            LoadingOverlayComponent,
                            IsLoadingDirective,
                        ],
                        imports: [
                            animations.BrowserAnimationsModule,
                            common.CommonModule,
                            material.MatIconModule,
                            tabs.MatTabsModule,
                            select.MatSelectModule,
                            formField.MatFormFieldModule,
                            button.MatButtonModule,
                            forms.FormsModule,
                            core$1.MatRippleModule,
                            tooltip.MatTooltipModule,
                            progressSpinner.MatProgressSpinnerModule,
                        ],
                        exports: [
                            CommonComponent,
                            FilterComponent,
                        ]
                    },] }
        ];
        return CommonModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/models/favorite.model.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var Favorite = /** @class */ (function () {
        function Favorite(data) {
            if (data === void 0) { data = {}; }
            this.setId(data.id);
            this.setFilter(data.filter || new Filter());
            this.setLabel(data.label || '');
        }
        // GETTERS -------------------------------------------
        // GETTERS -------------------------------------------
        /**
         * @return {?}
         */
        Favorite.prototype.getId = 
        // GETTERS -------------------------------------------
        /**
         * @return {?}
         */
        function () {
            return this._id;
        };
        /**
         * @return {?}
         */
        Favorite.prototype.getFilter = /**
         * @return {?}
         */
        function () {
            return this._filter || new Filter();
        };
        /**
         * @return {?}
         */
        Favorite.prototype.getLabel = /**
         * @return {?}
         */
        function () {
            return this._label || '';
        };
        // SETTERS -------------------------------------------
        // SETTERS -------------------------------------------
        /**
         * @param {?} id
         * @return {?}
         */
        Favorite.prototype.setId = 
        // SETTERS -------------------------------------------
        /**
         * @param {?} id
         * @return {?}
         */
        function (id) {
            this._id = id;
            return this;
        };
        /**
         * @param {?} filter
         * @return {?}
         */
        Favorite.prototype.setFilter = /**
         * @param {?} filter
         * @return {?}
         */
        function (filter) {
            this._filter = filter;
            return this;
        };
        /**
         * @param {?} label
         * @return {?}
         */
        Favorite.prototype.setLabel = /**
         * @param {?} label
         * @return {?}
         */
        function (label) {
            this._label = label;
            return this;
        };
        // SERIALIZATION -------------------------------------------
        // SERIALIZATION -------------------------------------------
        /**
         * @param {?} json
         * @return {?}
         */
        Favorite.fromJSON = 
        // SERIALIZATION -------------------------------------------
        /**
         * @param {?} json
         * @return {?}
         */
        function (json) {
            json.filter = Filter.fromJSON(json.filter);
            return new Favorite(json);
        };
        /**
         * @return {?}
         */
        Favorite.prototype.toObject = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var o = {
                label: this.getLabel(),
                filter: this.getFilter(),
            };
            if (this.getId())
                o.id = this.getId();
            return o;
        };
        /**
         * @return {?}
         */
        Favorite.prototype.toJSON = /**
         * @return {?}
         */
        function () {
            return JSON.stringify(this.toObject());
        };
        return Favorite;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        Favorite.prototype._id;
        /**
         * @type {?}
         * @private
         */
        Favorite.prototype._filter;
        /**
         * @type {?}
         * @private
         */
        Favorite.prototype._label;
    }

    exports.CommonComponent = CommonComponent;
    exports.CommonModule = CommonModule;
    exports.CommonService = CommonService;
    exports.Favorite = Favorite;
    exports.Filter = Filter;
    exports.FilterComponent = FilterComponent;
    exports.ɵa = LoadingOverlayComponent;
    exports.ɵb = IsLoadingDirective;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=teleo-common.umd.js.map
