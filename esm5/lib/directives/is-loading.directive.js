/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/is-loading.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { LoadingOverlayComponent } from '../loading-overlay/loading-overlay.component';
var IsLoadingDirective = /** @class */ (function () {
    function IsLoadingDirective(viewContainerRef, componentFactoryResolver) {
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.isLoading = false;
    }
    /**
     * @return {?}
     */
    IsLoadingDirective.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.isLoading ? this.addLoader() : this.removeLoader();
    };
    /**
     * @return {?}
     */
    IsLoadingDirective.prototype.removeLoader = /**
     * @return {?}
     */
    function () {
        this.loader && this.loader.destroy();
    };
    /**
     * @return {?}
     */
    IsLoadingDirective.prototype.addLoader = /**
     * @return {?}
     */
    function () {
        if (this.loader)
            return;
        /** @type {?} */
        var factory = this.componentFactoryResolver.resolveComponentFactory(LoadingOverlayComponent);
        this.loader = this.viewContainerRef.createComponent(factory);
    };
    IsLoadingDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[appIsLoading]'
                },] }
    ];
    /** @nocollapse */
    IsLoadingDirective.ctorParameters = function () { return [
        { type: ViewContainerRef },
        { type: ComponentFactoryResolver }
    ]; };
    IsLoadingDirective.propDecorators = {
        isLoading: [{ type: Input, args: ['appIsLoading',] }]
    };
    return IsLoadingDirective;
}());
export { IsLoadingDirective };
if (false) {
    /** @type {?} */
    IsLoadingDirective.prototype.loader;
    /** @type {?} */
    IsLoadingDirective.prototype.isLoading;
    /**
     * @type {?}
     * @private
     */
    IsLoadingDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @private
     */
    IsLoadingDirective.prototype.componentFactoryResolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXMtbG9hZGluZy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AdGVsZW8vY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2RpcmVjdGl2ZXMvaXMtbG9hZGluZy5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFHLEtBQUssRUFBYSxnQkFBZ0IsRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUV2RjtJQVFFLDRCQUNVLGdCQUFrQyxFQUNsQyx3QkFBa0Q7UUFEbEQscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBSHJDLGNBQVMsR0FBRyxLQUFLLENBQUM7SUFNekMsQ0FBQzs7OztJQUVELHdDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzFELENBQUM7Ozs7SUFFRCx5Q0FBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUE7SUFDdEMsQ0FBQzs7OztJQUVELHNDQUFTOzs7SUFBVDtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU07WUFBRSxPQUFPOztZQUNsQixPQUFPLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixDQUFDO1FBQzlGLElBQUksQ0FBQyxNQUFNLEdBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQTtJQUMvRCxDQUFDOztnQkEzQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxnQkFBZ0I7aUJBQzNCOzs7O2dCQUxzQyxnQkFBZ0I7Z0JBQUUsd0JBQXdCOzs7NEJBVTlFLEtBQUssU0FBQyxjQUFjOztJQXNCdkIseUJBQUM7Q0FBQSxBQTdCRCxJQTZCQztTQTFCWSxrQkFBa0I7OztJQUU3QixvQ0FBTzs7SUFFUCx1Q0FBeUM7Ozs7O0lBRXZDLDhDQUEwQzs7Ozs7SUFDMUMsc0RBQTBEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCAgSW5wdXQsIE9uQ2hhbmdlcywgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMb2FkaW5nT3ZlcmxheUNvbXBvbmVudCB9IGZyb20gJy4uL2xvYWRpbmctb3ZlcmxheS9sb2FkaW5nLW92ZXJsYXkuY29tcG9uZW50JztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2FwcElzTG9hZGluZ10nXG59KVxuZXhwb3J0IGNsYXNzIElzTG9hZGluZ0RpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG5cbiAgbG9hZGVyO1xuXG4gIEBJbnB1dCgnYXBwSXNMb2FkaW5nJykgaXNMb2FkaW5nID0gZmFsc2U7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcbiAgICBwcml2YXRlIGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxuICApIHtcblxuICB9XG5cbiAgbmdPbkNoYW5nZXMoKSB7XG4gICAgdGhpcy5pc0xvYWRpbmcgPyB0aGlzLmFkZExvYWRlcigpIDogdGhpcy5yZW1vdmVMb2FkZXIoKTtcbiAgfVxuXG4gIHJlbW92ZUxvYWRlcigpIHtcbiAgICB0aGlzLmxvYWRlciAmJiB0aGlzLmxvYWRlci5kZXN0cm95KClcbiAgfVxuXG4gIGFkZExvYWRlcigpIHtcbiAgICBpZiAodGhpcy5sb2FkZXIpIHJldHVybjtcbiAgICBjb25zdCBmYWN0b3J5ID0gdGhpcy5jb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoTG9hZGluZ092ZXJsYXlDb21wb25lbnQpO1xuICAgIHRoaXMubG9hZGVyICA9IHRoaXMudmlld0NvbnRhaW5lclJlZi5jcmVhdGVDb21wb25lbnQoZmFjdG9yeSlcbiAgfVxuXG59XG4iXX0=