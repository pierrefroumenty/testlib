/**
 * @fileoverview added by tsickle
 * Generated from: lib/filter/filter.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { cloneDeep } from 'lodash';
import { Filter } from '../models/filter.model';
var FilterComponent = /** @class */ (function () {
    function FilterComponent() {
        this.options = null; // don't modify this object, except the ".hidden" properties
        this.filterChanged = new EventEmitter();
        this.deleteFavorite = new EventEmitter();
        this.saveAsFavorite = new EventEmitter();
        this.sortedOptions = null;
        this.devMode = false;
        this.closed = false;
        this.selectedTab = 0;
        this.form = {
            contracts: [],
            regions: [],
            territories: [],
            communes: [],
            searchRegions: '',
            searchTerritories: '',
            searchContracts: '',
            searchCommunes: '',
        };
    }
    /**
     * @return {?}
     */
    FilterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    FilterComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.filter)
            this.setForm(this.filter);
        if (changes.options) {
            this.options = cloneDeep(this.options);
            this.updateOptions();
        }
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.isSubmittable = /**
     * @return {?}
     */
    function () {
        return this.form.regions.length || this.form.territories.length || this.form.contracts.length;
    };
    /**
     * @param {?} filter
     * @return {?}
     */
    FilterComponent.prototype.setForm = /**
     * @param {?} filter
     * @return {?}
     */
    function (filter) {
        this.form.contracts = filter.getContracts();
        this.form.regions = filter.getRegions();
        this.form.territories = filter.getTerritories();
        this.form.communes = filter.getCommunes();
        this.onChange();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.onChange = /**
     * @return {?}
     */
    function () {
        // clean user selection (remove impossible cases)
        this.cleanSelection();
        this.updateOptions();
    };
    // -----------------------------------------
    // -----------------------------------------
    /**
     * @return {?}
     */
    FilterComponent.prototype.cleanSelection = 
    // -----------------------------------------
    /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.options)
            return;
        /** @type {?} */
        var filter = new Filter(this.form);
        /** @type {?} */
        var cleanedFilter = cloneDeep(filter);
        /** @type {?} */
        var currentTerritories = cleanedFilter.getTerritories().map((/**
         * @param {?} id
         * @return {?}
         */
        function (id) { return _this.options.territories.find((/**
         * @param {?} t
         * @return {?}
         */
        function (t) { return t.id == id; })); }));
        /** @type {?} */
        var currentContracts = cleanedFilter.getContracts().map((/**
         * @param {?} id
         * @return {?}
         */
        function (id) { return _this.options.contracts.find((/**
         * @param {?} c
         * @return {?}
         */
        function (c) { return c.id == id; })); }));
        /** @type {?} */
        var currentCommunes = cleanedFilter.getCommunes().map((/**
         * @param {?} id
         * @return {?}
         */
        function (id) { return _this.options.communes.find((/**
         * @param {?} c
         * @return {?}
         */
        function (c) { return c.id == id; })); }));
        // filter by regions
        if (cleanedFilter.getRegions().length) {
            cleanedFilter.setTerritories(currentTerritories.filter((/**
             * @param {?} t
             * @return {?}
             */
            function (t) { return cleanedFilter.getRegions().includes(t.region_id); })).map((/**
             * @param {?} t
             * @return {?}
             */
            function (t) { return t.id; })));
            cleanedFilter.setContracts(currentContracts.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return cleanedFilter.getRegions().includes(c.region_id); })).map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.id; })));
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return cleanedFilter.getRegions().includes(c.region_id); })).map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.id; })));
        }
        // filter by territories
        if (cleanedFilter.getTerritories().length) {
            cleanedFilter.setContracts(currentContracts.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return cleanedFilter.getTerritories().includes(c.territory_id); })).map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.id; })));
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return cleanedFilter.getTerritories().includes(c.territory_id); })).map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.id; })));
        }
        // filter by contracts
        if (cleanedFilter.getContracts().length) {
            cleanedFilter.setCommunes(currentCommunes.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return cleanedFilter.getContracts().includes(c.contract_id); })).map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.id; })));
        }
        // update form if the cleaned version of the filter is different
        if (!cleanedFilter.isEqualTo(filter))
            this.setForm(cleanedFilter);
    };
    // -----------------------------------------
    // -----------------------------------------
    /**
     * @return {?}
     */
    FilterComponent.prototype.updateOptions = 
    // -----------------------------------------
    /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.options)
            return;
        /** @type {?} */
        var filter = new Filter(this.form);
        /** @type {?} */
        var filterRegions = filter.getRegions();
        /** @type {?} */
        var filterTerritories = filter.getTerritories();
        /** @type {?} */
        var filterContracts = filter.getContracts();
        // filter region options
        this.options.regions.map((/**
         * @param {?} r
         * @return {?}
         */
        function (r) {
            /** @type {?} */
            var passSearchFilter = !_this.form.searchRegions.length || (String.search(_this.form.searchRegions, r.name) || String.search(_this.form.searchRegions, r.id));
            /** @type {?} */
            var pass = passSearchFilter;
            r.hidden = !pass;
            return r;
        }));
        // filter territory options
        this.options.territories.map((/**
         * @param {?} t
         * @return {?}
         */
        function (t) {
            /** @type {?} */
            var passRegionFilter = !filterRegions.length || filterRegions.includes(t.region_id);
            /** @type {?} */
            var passSearchFilter = !_this.form.searchTerritories.length || (String.search(_this.form.searchTerritories, t.name) || String.search(_this.form.searchTerritories, t.id));
            /** @type {?} */
            var pass = passRegionFilter && passSearchFilter;
            /** @type {?} */
            var hidden = !pass;
            if (t.hidden !== hidden)
                t.hidden = hidden;
            return t;
        }));
        // filter contract options
        this.options.contracts.map((/**
         * @param {?} c
         * @return {?}
         */
        function (c) {
            /** @type {?} */
            var passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
            /** @type {?} */
            var passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
            /** @type {?} */
            var passSearchFilter = !_this.form.searchContracts.length || (String.search(_this.form.searchContracts, c.name) || String.search(_this.form.searchContracts, c.id));
            /** @type {?} */
            var pass = passRegionFilter && passTerritoryFilter && passSearchFilter;
            /** @type {?} */
            var hidden = !pass;
            if (c.hidden !== hidden)
                c.hidden = hidden;
            return c;
        }));
        // filter commune options
        this.options.communes.map((/**
         * @param {?} c
         * @return {?}
         */
        function (c) {
            /** @type {?} */
            var passRegionFilter = !filterRegions.length || filterRegions.includes(c.region_id);
            /** @type {?} */
            var passTerritoryFilter = !filterTerritories.length || filterTerritories.includes(c.territory_id);
            /** @type {?} */
            var passContractFilter = !filterContracts.length || filterContracts.includes(c.contract_id);
            /** @type {?} */
            var passSearchFilter = !_this.form.searchCommunes.length || (String.search(_this.form.searchCommunes, c.name) || String.search(_this.form.searchCommunes, c.id));
            /** @type {?} */
            var pass = passRegionFilter && passTerritoryFilter && passContractFilter && passSearchFilter;
            /** @type {?} */
            var hidden = !pass;
            if (c.hidden !== hidden)
                c.hidden = hidden;
            return c;
        }));
        // Below, we put hidden options at the end, so that mat-select's scrolling behaves correctly
        // 1. this way we also can use "visibility" css property which is faster than *ngIf which would insert/removes DOM nodes
        // 2. this way, using the search field doesn't remove our previous selection
        this.sortedOptions = {
            regions: this.options.regions.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
            territories: this.options.territories.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
            contracts: this.options.contracts.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
            communes: this.options.communes.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return (Number(a.hidden) - Number(b.hidden)) || a.name.localeCompare(b.name, 'fr', { sensitivity: "base" }); })),
        };
    };
    /**
     * @param {?} index
     * @param {?} element
     * @return {?}
     */
    FilterComponent.prototype.trackElement = /**
     * @param {?} index
     * @param {?} element
     * @return {?}
     */
    function (index, element) {
        return element ? element.id : null;
    };
    // SELECT / DESELECT --------------------------------------------
    // SELECT / DESELECT --------------------------------------------
    /**
     * @return {?}
     */
    FilterComponent.prototype.selectAllRegions = 
    // SELECT / DESELECT --------------------------------------------
    /**
     * @return {?}
     */
    function () {
        if (!this.options)
            return;
        this.form.regions = this.options.regions.map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.id; }));
        this.onChange();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.deselectAllRegions = /**
     * @return {?}
     */
    function () {
        this.form.regions = [];
        this.form.territories = [];
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.selectAllTerritories = /**
     * @return {?}
     */
    function () {
        if (!this.options)
            return;
        this.form.territories = this.options.territories.map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.id; }));
        this.onChange();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.deselectAllTerritories = /**
     * @return {?}
     */
    function () {
        this.form.territories = [];
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.selectAllContracts = /**
     * @return {?}
     */
    function () {
        if (!this.options)
            return;
        this.form.contracts = this.options.contracts.map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.id; }));
        this.onChange();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.deselectAllContracts = /**
     * @return {?}
     */
    function () {
        this.form.contracts = [];
        this.form.communes = [];
        this.onChange();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FilterComponent.prototype._handleKeydown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        event.stopPropagation();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.getRegionsPlaceholder = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var selectedOptions = this.form && (this.form.regions || []).length;
        /** @type {?} */
        var availableOptions = this.countRegionsOptions();
        return this.devMode ? "R\u00E9gions (" + selectedOptions + "/" + availableOptions + ")" : "R\u00E9gions (" + selectedOptions + ")";
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.getTerritoriesPlaceholder = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var selectedOptions = this.form && (this.form.territories || []).length;
        /** @type {?} */
        var availableOptions = this.countTerritoriesOptions();
        return this.devMode ? "Territoires (" + selectedOptions + "/" + availableOptions + ")" : "Territoires (" + selectedOptions + ")";
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.getContractsPlaceholder = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var selectedOptions = this.form && (this.form.contracts || []).length;
        /** @type {?} */
        var availableOptions = this.countContractsOptions();
        return this.devMode ? "Contrats (" + selectedOptions + "/" + availableOptions + ")" : "Contrats (" + selectedOptions + ")";
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.countRegionsOptions = /**
     * @return {?}
     */
    function () {
        if (!this.options)
            return 0;
        return this.options.regions.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return !item.hidden; })).length;
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.countTerritoriesOptions = /**
     * @return {?}
     */
    function () {
        if (!this.options)
            return 0;
        return this.options.territories.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return !item.hidden; })).length;
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.countContractsOptions = /**
     * @return {?}
     */
    function () {
        if (!this.options)
            return 0;
        return this.options.contracts.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return !item.hidden; })).length;
    };
    // APPLY -------------------------------------------------------------
    // APPLY -------------------------------------------------------------
    /**
     * @param {?} field
     * @return {?}
     */
    FilterComponent.prototype.emptySearchField = 
    // APPLY -------------------------------------------------------------
    /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        // empty search fields
        if (this.form[field].length) {
            this.form[field] = '';
            this.updateOptions();
        }
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.reinit = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var emptyFilter = new Filter();
        // empty search fields
        this.form.searchRegions = '';
        this.form.searchTerritories = '';
        this.form.searchContracts = '';
        this.form.searchCommunes = '';
        this.setForm(emptyFilter);
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype._saveAsFavorite = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var filter = new Filter(this.form);
        this.saveAsFavorite.emit(filter);
    };
    /**
     * @param {?} favorite
     * @return {?}
     */
    FilterComponent.prototype._deleteFavorite = /**
     * @param {?} favorite
     * @return {?}
     */
    function (favorite) {
        this.deleteFavorite.emit(favorite);
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.applyFilter = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var filter = new Filter(this.form);
        this.filterChanged.emit(filter);
    };
    /**
     * @param {?} favorite
     * @return {?}
     */
    FilterComponent.prototype.applyFavorite = /**
     * @param {?} favorite
     * @return {?}
     */
    function (favorite) {
        this.setForm(favorite.getFilter());
        this.applyFilter();
        this.goToFilterTab();
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.goToFilterTab = /**
     * @return {?}
     */
    function () {
        this.selectedTab = 0;
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.closeNav = /**
     * @return {?}
     */
    function () {
        this.closed = true;
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.openNav = /**
     * @return {?}
     */
    function () {
        this.closed = false;
    };
    FilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'teleo-filter',
                    template: "<!-- HIDE/SHOW BUTTON -->\n<div [ngClass]=\"{'filter-button' : true, 'filter-button--hidden' : !closed}\" (click)=\"openNav();\">\n  <mat-icon class=\"filter-button__icon\">filter_list</mat-icon>\n</div>\n\n<div class=\"wrapper\" [ngClass]=\"{'wrapper--hidden': closed}\">\n\n  <!-- HIDE BUTTON -->\n  <mat-icon class=\"sidebar-close\" (click)=\"closeNav()\">arrow_back</mat-icon>\n\n  <mat-tab-group [(selectedIndex)]=\"selectedTab\" dynamicHeight=\"true\" animationDuration=\"200ms\"> \n    \n    <!-- FILTER ----------------------------------->\n    <mat-tab label=\"FILTRE\">\n      \n      <!-- BODY -->\n      <div class=\"filters\" [appIsLoading]=\"!options\">\n\n        <!-- REGIONS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchRegions')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getRegionsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.regions\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchRegions\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllRegions()\">Toutes</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllRegions()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let region of sortedOptions?.regions; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: region.hidden ? 'none' : 'flex'}\" [value]=\"region.id\" matTooltip=\"{{ region.name }}\"\n                matTooltipPosition=\"above\" matTooltipShowDelay=\"500\">\n                {{ region.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- TERRITOIRES -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchTerritories')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getTerritoriesPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.territories\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchTerritories\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllTerritories()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllTerritories()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let territory of sortedOptions?.territories; trackBy: trackElement\">\n              <mat-option [ngStyle]=\"{display: territory.hidden ? 'none' : 'flex'}\" \n                [value]=\"territory.id\" matTooltip=\"{{ territory.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ territory.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n        <!-- CONTRATS -->\n        <mat-form-field class=\"w100\">\n          <mat-select (openedChange)=\"emptySearchField('searchContracts')\" [disableOptionCentering]=\"true\" placeholder=\"{{ getContractsPlaceholder() }}\" multiple\n            [(ngModel)]=\"form.contracts\" (selectionChange)=\"onChange()\">\n            <div class=\"dropdown-controls\">\n              <input placeholder=\"Recherche\" (keydown)=\"_handleKeydown($event)\" [(ngModel)]=\"form.searchContracts\"\n                (ngModelChange)=\"onChange()\" class=\"search\">\n              <button mat-button color=\"primary\" (click)=\"selectAllContracts()\">Tous</button>\n              <button mat-button color=\"primary\" (click)=\"deselectAllContracts()\">Aucun</button>\n            </div>\n            <ng-container *ngFor=\"let contract of sortedOptions?.contracts; trackBy: trackElement\">\n              <mat-option [value]=\"contract.id\" [ngStyle]=\"{display: contract.hidden ? 'none' : 'flex'}\" \n                matTooltip=\"{{ contract.id }} - {{ contract.name }}\" matTooltipPosition=\"above\"\n                matTooltipShowDelay=\"500\">\n                {{ contract.id }} - {{ contract.name }}\n              </mat-option>\n            </ng-container>\n          </mat-select>\n        </mat-form-field>\n\n\n\n\n      </div>\n    </mat-tab>\n\n\n    <!-- FAVORITES ----------------------------------------------------->\n    <mat-tab label=\"FAVORIS\" *ngIf=\"favorites\">\n      <div class=\"favorites\" [appIsLoading]=\"isFetchingFavorites$ | async\">\n        <div class=\"favorite\" *ngFor=\"let favorite of (favorites | keyvalue)\">\n          <div class=\"favorite__label\" matRipple matTooltip=\"{{ favorite?.value?.getLabel() }}\" matTooltipPosition=\"above\" matTooltipShowDelay=\"500\" (click)=\"applyFavorite(favorite.value)\">{{ favorite?.value?.getLabel() }}</div>\n          <div class=\"favorite__delete\" matRipple (click)=\"_deleteFavorite(favorite.value)\"><mat-icon>delete</mat-icon></div>\n        </div>\n      </div>\n      \n    </mat-tab>\n  </mat-tab-group>\n\n  <!-- FOOTER -->\n  <div [ngClass]=\"{'filter-footer--hidden': selectedTab !== 0, 'filter-footer': true}\">\n    <button mat-stroked-button color=\"primary\" (click)=\"reinit();\">R\u00C9INITIALISER LES FILTRES</button>\n    <button mat-stroked-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"_saveAsFavorite()\">SAUVEGARDER</button>\n    <button mat-raised-button color=\"primary\" [disabled]=\"!options || !isSubmittable()\" (click)=\"applyFilter()\">APPLIQUER</button>\n  </div>\n</div>",
                    styles: [":host{position:relative}:host ::ng-deep .mat-tab-label{min-width:115px;font-family:TheSans,Roboto,sans-serif;font-size:16px;line-height:22px;font-weight:600;height:72px;color:#353535;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}:host ::ng-deep .mat-tab-label.mat-tab-label-active{color:#353535;opacity:1}.dropdown-controls{display:-webkit-box;display:flex;position:-webkit-sticky;position:sticky;top:0;background:#fff;z-index:10;box-shadow:0 0 3px 1px #c5c5c5}.filter-button{position:absolute;top:0;left:0;width:36px;height:72px;background-color:#804180;border-radius:0 16px 16px 0;box-shadow:0 2px 4px 0 rgba(0,0,0,.5);color:#fff;cursor:pointer;z-index:90;-webkit-transition:left .2s;transition:left .2s}.filter-button__icon{margin-top:24px;margin-left:4px}.filter-button--hidden{left:-40px}.filters{padding:25px;display:block;overflow-y:auto;opacity:1}.favorites{display:block;overflow-y:auto;opacity:1;min-height:220px;max-height:50vh;overflow:auto}.favorites:empty{display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.favorites:empty::before{content:\"Vous n'avez pas de favoris\"}.favorite{display:-webkit-box;display:flex;width:100%;-webkit-box-align:stretch;align-items:stretch;border-bottom:#f0f0f0;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer}.favorite:hover{background:#f0f0f0}.favorite__label{padding:15px 0 15px 15px;font-family:TheSans,Roboto,sans-serif;-webkit-box-flex:1;flex-grow:1;text-overflow:ellipsis;white-space:nowrap;overflow:hidden}.favorite__delete{cursor:pointer;width:50px;height:50px;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center;display:-webkit-box;display:flex;flex-shrink:0}.favorite__delete:hover{color:#e0421f}.wrapper{margin-left:15px;width:300px;max-width:300px;z-index:90;border-radius:0 20px;box-shadow:0 2px 10px 0 rgba(0,0,0,.24);font-family:TheSans,Roboto,sans-serif;background:#fff;-webkit-transition:width .2s,left .2s,margin .2s;transition:width .2s,left .2s,margin .2s;position:relative;left:0;overflow:hidden}.wrapper--hidden{width:0;left:-300px;margin-left:0;margin-right:51px}.w100{width:100%}.sidebar-close{position:absolute;right:28px;top:24px;font-size:20px;cursor:pointer;font-weight:700;color:#000;z-index:100}.filter-footer{font-weight:600;max-height:350px;padding:24px;border-top:1px solid #ccc;border-bottom-left-radius:20px;overflow:hidden;-webkit-transition:max-height .2s,padding .2s;transition:max-height .2s,padding .2s}.filter-footer button{width:100%;display:block;margin:0 auto 9.5px;height:37px}.filter-footer--hidden{max-height:0;padding:0}.checkbox{margin-top:5px;padding:0;box-sizing:border-box;font-size:13px;margin-bottom:30px}.checkbox ::ng-deep .mat-checkbox-label{white-space:normal}.search{width:100%;border:none;padding:10px 15px}.search:focus{outline:0}"]
                }] }
    ];
    /** @nocollapse */
    FilterComponent.ctorParameters = function () { return []; };
    FilterComponent.propDecorators = {
        options: [{ type: Input, args: ['options',] }],
        filter: [{ type: Input, args: ['filter',] }],
        favorites: [{ type: Input, args: ['favorites',] }],
        isFetchingFavorites$: [{ type: Input, args: ['isFetchingFavorites$',] }],
        filterChanged: [{ type: Output, args: ['filterChanged',] }],
        deleteFavorite: [{ type: Output, args: ['deleteFavorite',] }],
        saveAsFavorite: [{ type: Output, args: ['saveAsFavorite',] }]
    };
    return FilterComponent;
}());
export { FilterComponent };
if (false) {
    /** @type {?} */
    FilterComponent.prototype.options;
    /** @type {?} */
    FilterComponent.prototype.filter;
    /** @type {?} */
    FilterComponent.prototype.favorites;
    /** @type {?} */
    FilterComponent.prototype.isFetchingFavorites$;
    /** @type {?} */
    FilterComponent.prototype.filterChanged;
    /** @type {?} */
    FilterComponent.prototype.deleteFavorite;
    /** @type {?} */
    FilterComponent.prototype.saveAsFavorite;
    /** @type {?} */
    FilterComponent.prototype.sortedOptions;
    /** @type {?} */
    FilterComponent.prototype.devMode;
    /** @type {?} */
    FilterComponent.prototype.closed;
    /** @type {?} */
    FilterComponent.prototype.selectedTab;
    /** @type {?} */
    FilterComponent.prototype.form;
}
var String = /** @class */ (function () {
    function String() {
    }
    /**
     * return true if each terms (space-separated) of $needle
     * are present in $haystack
     */
    /**
     * return true if each terms (space-separated) of $needle
     * are present in $haystack
     * @param {?} needle
     * @param {?} haystack
     * @return {?}
     */
    String.search = /**
     * return true if each terms (space-separated) of $needle
     * are present in $haystack
     * @param {?} needle
     * @param {?} haystack
     * @return {?}
     */
    function (needle, haystack) {
        var e_1, _a;
        // clean strings
        needle = this.replaceAccentedCharacters(needle.toLowerCase());
        haystack = this.replaceAccentedCharacters(haystack.toLowerCase());
        // split string in search terms
        /** @type {?} */
        var terms = needle.split(/\s+/);
        try {
            // check each term is present
            for (var terms_1 = tslib_1.__values(terms), terms_1_1 = terms_1.next(); !terms_1_1.done; terms_1_1 = terms_1.next()) {
                var term = terms_1_1.value;
                if (!haystack.includes(term))
                    return false;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (terms_1_1 && !terms_1_1.done && (_a = terms_1.return)) _a.call(terms_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return true;
    };
    /**
     * @param {?} str
     * @return {?}
     */
    String.replaceAccentedCharacters = /**
     * @param {?} str
     * @return {?}
     */
    function (str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    };
    return String;
}());
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B0ZWxlby9jb21tb24vIiwic291cmNlcyI6WyJsaWIvZmlsdGVyL2ZpbHRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFDbkMsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBUWhEO0lBK0JFO1FBeEJrQixZQUFPLEdBQWtCLElBQUksQ0FBQyxDQUFDLDREQUE0RDtRQUtwRixrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDMUMsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBWSxDQUFDO1FBQzlDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUV0RSxrQkFBYSxHQUFrQixJQUFJLENBQUM7UUFDcEMsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixXQUFNLEdBQVksS0FBSyxDQUFDO1FBQ3hCLGdCQUFXLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLFNBQUksR0FBUTtZQUNWLFNBQVMsRUFBRSxFQUFFO1lBQ2IsT0FBTyxFQUFFLEVBQUU7WUFDWCxXQUFXLEVBQUUsRUFBRTtZQUNmLFFBQVEsRUFBRSxFQUFFO1lBQ1osYUFBYSxFQUFFLEVBQUU7WUFDakIsaUJBQWlCLEVBQUUsRUFBRTtZQUNyQixlQUFlLEVBQUUsRUFBRTtZQUNuQixjQUFjLEVBQUUsRUFBRTtTQUNuQixDQUFBO0lBR0QsQ0FBQzs7OztJQUVELGtDQUFROzs7SUFBUjtJQUVBLENBQUM7Ozs7O0lBRUQscUNBQVc7Ozs7SUFBWCxVQUFZLE9BQU87UUFDakIsSUFBSSxPQUFPLENBQUMsTUFBTTtZQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQzs7OztJQUVELHVDQUFhOzs7SUFBYjtRQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7SUFDaEcsQ0FBQzs7Ozs7SUFFRCxpQ0FBTzs7OztJQUFQLFVBQVEsTUFBYztRQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUE7UUFDM0MsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFBO1FBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQTtRQUMvQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUE7UUFDekMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO0lBQ2pCLENBQUM7Ozs7SUFHRCxrQ0FBUTs7O0lBQVI7UUFDRSxpREFBaUQ7UUFDakQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO1FBQ3JCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtJQUN0QixDQUFDO0lBRUQsNENBQTRDOzs7OztJQUU1Qyx3Q0FBYzs7Ozs7SUFBZDtRQUFBLGlCQTZCQztRQTNCQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFNOztZQUVyQixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzs7WUFDOUIsYUFBYSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUM7O1lBRWpDLGtCQUFrQixHQUFnQixhQUFhLENBQUMsY0FBYyxFQUFFLENBQUMsR0FBRzs7OztRQUFDLFVBQUEsRUFBRSxJQUFJLE9BQUEsS0FBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQVYsQ0FBVSxFQUFDLEVBQTlDLENBQThDLEVBQUM7O1lBQzFILGdCQUFnQixHQUFlLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxFQUFFLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBVixDQUFVLEVBQUMsRUFBNUMsQ0FBNEMsRUFBQzs7WUFDbkgsZUFBZSxHQUFjLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxFQUFFLElBQUksT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBVixDQUFVLEVBQUMsRUFBM0MsQ0FBMkMsRUFBQztRQUVuSCxvQkFBb0I7UUFDcEIsSUFBSSxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsTUFBTSxFQUFFO1lBQ3JDLGFBQWEsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsTUFBTTs7OztZQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQWhELENBQWdELEVBQUMsQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsRUFBRSxFQUFKLENBQUksRUFBQyxDQUFDLENBQUE7WUFDL0gsYUFBYSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBaEQsQ0FBZ0QsRUFBQyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxFQUFFLEVBQUosQ0FBSSxFQUFDLENBQUMsQ0FBQTtZQUMzSCxhQUFhLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBaEQsQ0FBZ0QsRUFBQyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxFQUFFLEVBQUosQ0FBSSxFQUFDLENBQUMsQ0FBQTtTQUMxSDtRQUNELHdCQUF3QjtRQUN4QixJQUFJLGFBQWEsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNLEVBQUU7WUFDekMsYUFBYSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxhQUFhLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBdkQsQ0FBdUQsRUFBQyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxFQUFFLEVBQUosQ0FBSSxFQUFDLENBQUMsQ0FBQTtZQUNsSSxhQUFhLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxhQUFhLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBdkQsQ0FBdUQsRUFBQyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxFQUFFLEVBQUosQ0FBSSxFQUFDLENBQUMsQ0FBQTtTQUNqSTtRQUNELHNCQUFzQjtRQUN0QixJQUFJLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxNQUFNLEVBQUU7WUFDdkMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsTUFBTTs7OztZQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEVBQXBELENBQW9ELEVBQUMsQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsRUFBRSxFQUFKLENBQUksRUFBQyxDQUFDLENBQUE7U0FDOUg7UUFFRCxnRUFBZ0U7UUFDaEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQTtJQUNuRSxDQUFDO0lBSUQsNENBQTRDOzs7OztJQUU1Qyx1Q0FBYTs7Ozs7SUFBYjtRQUFBLGlCQTZEQztRQTNEQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPOztZQUV0QixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzs7WUFDNUIsYUFBYSxHQUFHLE1BQU0sQ0FBQyxVQUFVLEVBQUU7O1lBQ25DLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxjQUFjLEVBQUU7O1lBQzNDLGVBQWUsR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFO1FBRTdDLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxDQUFDOztnQkFDbEIsZ0JBQWdCLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Z0JBQ3RKLElBQUksR0FBRyxnQkFBZ0I7WUFDN0IsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQztZQUNqQixPQUFPLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBQyxDQUFBO1FBRUYsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLENBQUM7O2dCQUN0QixnQkFBZ0IsR0FBRyxDQUFDLGFBQWEsQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDOztnQkFDL0UsZ0JBQWdCLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Z0JBQ2xLLElBQUksR0FBRyxnQkFBZ0IsSUFBSSxnQkFBZ0I7O2dCQUMzQyxNQUFNLEdBQUcsQ0FBQyxJQUFJO1lBQ3BCLElBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxNQUFNO2dCQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBRTNDLE9BQU8sQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFDLENBQUE7UUFFRiwwQkFBMEI7UUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRzs7OztRQUFDLFVBQUEsQ0FBQzs7Z0JBQ3BCLGdCQUFnQixHQUFHLENBQUMsYUFBYSxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7O2dCQUMvRSxtQkFBbUIsR0FBRyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sSUFBSSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQzs7Z0JBQzdGLGdCQUFnQixHQUFHLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7O2dCQUM1SixJQUFJLEdBQUcsZ0JBQWdCLElBQUksbUJBQW1CLElBQUksZ0JBQWdCOztnQkFDbEUsTUFBTSxHQUFHLENBQUMsSUFBSTtZQUNwQixJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssTUFBTTtnQkFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUMzQyxPQUFPLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBQyxDQUFBO1FBRUYseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLENBQUM7O2dCQUNuQixnQkFBZ0IsR0FBRyxDQUFDLGFBQWEsQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDOztnQkFDL0UsbUJBQW1CLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLElBQUksaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7O2dCQUM3RixrQkFBa0IsR0FBRyxDQUFDLGVBQWUsQ0FBQyxNQUFNLElBQUksZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDOztnQkFDdkYsZ0JBQWdCLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Z0JBQ3pKLElBQUksR0FBRyxnQkFBZ0IsSUFBSSxtQkFBbUIsSUFBSSxrQkFBa0IsSUFBSSxnQkFBZ0I7O2dCQUN4RixNQUFNLEdBQUcsQ0FBQyxJQUFJO1lBQ3BCLElBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxNQUFNO2dCQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQzNDLE9BQU8sQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFDLENBQUE7UUFHRiw0RkFBNEY7UUFDNUYsd0hBQXdIO1FBQ3hILDRFQUE0RTtRQUM1RSxJQUFJLENBQUMsYUFBYSxHQUFHO1lBQ25CLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7OztZQUFDLFVBQUMsQ0FBQyxFQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBQyxXQUFXLEVBQUUsTUFBTSxFQUFDLENBQUMsRUFBbEcsQ0FBa0csRUFBQztZQUMvSSxXQUFXLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSTs7Ozs7WUFBQyxVQUFDLENBQUMsRUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUMsV0FBVyxFQUFFLE1BQU0sRUFBQyxDQUFDLEVBQW5HLENBQW1HLEVBQUM7WUFDeEosU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUk7Ozs7O1lBQUMsVUFBQyxDQUFDLEVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUMsQ0FBQyxFQUFuRyxDQUFtRyxFQUFDO1lBQ3BKLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJOzs7OztZQUFDLFVBQUMsQ0FBQyxFQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBQyxXQUFXLEVBQUUsTUFBTSxFQUFDLENBQUMsRUFBbkcsQ0FBbUcsRUFBQztTQUNuSixDQUFBO0lBQ0gsQ0FBQzs7Ozs7O0lBRUQsc0NBQVk7Ozs7O0lBQVosVUFBYSxLQUFhLEVBQUUsT0FBWTtRQUN0QyxPQUFPLE9BQU8sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO0lBQ3BDLENBQUM7SUFFRCxpRUFBaUU7Ozs7O0lBRWpFLDBDQUFnQjs7Ozs7SUFBaEI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPO1FBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxFQUFFLEVBQVAsQ0FBTyxFQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7Ozs7SUFFRCw0Q0FBa0I7OztJQUFsQjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEIsQ0FBQzs7OztJQUVELDhDQUFvQjs7O0lBQXBCO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsRUFBRSxFQUFQLENBQU8sRUFBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBRUQsZ0RBQXNCOzs7SUFBdEI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEIsQ0FBQzs7OztJQUVELDRDQUFrQjs7O0lBQWxCO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsRUFBRSxFQUFQLENBQU8sRUFBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7O0lBRUQsOENBQW9COzs7SUFBcEI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDOzs7OztJQUlELHdDQUFjOzs7O0lBQWQsVUFBZSxLQUFvQjtRQUNqQyxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDMUIsQ0FBQzs7OztJQUVELCtDQUFxQjs7O0lBQXJCOztZQUNRLGVBQWUsR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLENBQUMsTUFBTTs7WUFDL0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1FBQ25ELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsbUJBQVksZUFBZSxTQUFJLGdCQUFnQixNQUFHLENBQUMsQ0FBQyxDQUFDLG1CQUFZLGVBQWUsTUFBRyxDQUFBO0lBQzNHLENBQUM7Ozs7SUFFRCxtREFBeUI7OztJQUF6Qjs7WUFDUSxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQyxDQUFDLE1BQU07O1lBQ25FLGdCQUFnQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtRQUN2RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLGtCQUFnQixlQUFlLFNBQUksZ0JBQWdCLE1BQUcsQ0FBQyxDQUFDLENBQUMsa0JBQWdCLGVBQWUsTUFBRyxDQUFBO0lBQ25ILENBQUM7Ozs7SUFFRCxpREFBdUI7OztJQUF2Qjs7WUFDUSxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQyxDQUFDLE1BQU07O1lBQ2pFLGdCQUFnQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtRQUNyRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLGVBQWEsZUFBZSxTQUFJLGdCQUFnQixNQUFHLENBQUMsQ0FBQyxDQUFDLGVBQWEsZUFBZSxNQUFHLENBQUE7SUFDN0csQ0FBQzs7OztJQUVELDZDQUFtQjs7O0lBQW5CO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDNUIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQVosQ0FBWSxFQUFDLENBQUMsTUFBTSxDQUFBO0lBQ2pFLENBQUM7Ozs7SUFFRCxpREFBdUI7OztJQUF2QjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQzVCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFaLENBQVksRUFBQyxDQUFDLE1BQU0sQ0FBQTtJQUNyRSxDQUFDOzs7O0lBRUQsK0NBQXFCOzs7SUFBckI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPLENBQUMsQ0FBQztRQUM1QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBWixDQUFZLEVBQUMsQ0FBQyxNQUFNLENBQUE7SUFDbkUsQ0FBQztJQUNELHNFQUFzRTs7Ozs7O0lBRXRFLDBDQUFnQjs7Ozs7O0lBQWhCLFVBQWlCLEtBQUs7UUFDcEIsc0JBQXNCO1FBQ3RCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUU7WUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQzs7OztJQUVELGdDQUFNOzs7SUFBTjs7WUFDUSxXQUFXLEdBQUcsSUFBSSxNQUFNLEVBQUU7UUFFaEMsc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQTtRQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQTtRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUE7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFBO1FBRTdCLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUE7SUFDM0IsQ0FBQzs7OztJQUVELHlDQUFlOzs7SUFBZjs7WUFDTSxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNsQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVELHlDQUFlOzs7O0lBQWYsVUFBZ0IsUUFBa0I7UUFDaEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7SUFFcEMsQ0FBQzs7OztJQUdELHFDQUFXOzs7SUFBWDs7WUFDTSxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNsQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELHVDQUFhOzs7O0lBQWIsVUFBYyxRQUFrQjtRQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7OztJQUdELHVDQUFhOzs7SUFBYjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFHRCxrQ0FBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNyQixDQUFDOzs7O0lBRUQsaUNBQU87OztJQUFQO1FBQ0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDdEIsQ0FBQzs7Z0JBaFRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsNHBMQUFzQzs7aUJBRXZDOzs7OzswQkFHRSxLQUFLLFNBQUMsU0FBUzt5QkFDZixLQUFLLFNBQUMsUUFBUTs0QkFDZCxLQUFLLFNBQUMsV0FBVzt1Q0FDakIsS0FBSyxTQUFDLHNCQUFzQjtnQ0FFNUIsTUFBTSxTQUFDLGVBQWU7aUNBQ3RCLE1BQU0sU0FBQyxnQkFBZ0I7aUNBQ3ZCLE1BQU0sU0FBQyxnQkFBZ0I7O0lBb1MxQixzQkFBQztDQUFBLEFBbFRELElBa1RDO1NBN1NZLGVBQWU7OztJQUUxQixrQ0FBZ0Q7O0lBQ2hELGlDQUFnQzs7SUFDaEMsb0NBQStDOztJQUMvQywrQ0FBeUU7O0lBRXpFLHdDQUFvRTs7SUFDcEUseUNBQXdFOztJQUN4RSx5Q0FBc0U7O0lBRXRFLHdDQUFvQzs7SUFDcEMsa0NBQWdCOztJQUNoQixpQ0FBd0I7O0lBQ3hCLHNDQUFnQjs7SUFDaEIsK0JBU0M7O0FBdVJIO0lBQUE7SUF3QkEsQ0FBQztJQXJCQzs7O09BR0c7Ozs7Ozs7O0lBQ0ksYUFBTTs7Ozs7OztJQUFiLFVBQWMsTUFBYyxFQUFFLFFBQWdCOztRQUMxQyxnQkFBZ0I7UUFDaEIsTUFBTSxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQTtRQUM3RCxRQUFRLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDOzs7WUFFNUQsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDOztZQUNqQyw2QkFBNkI7WUFDN0IsS0FBaUIsSUFBQSxVQUFBLGlCQUFBLEtBQUssQ0FBQSw0QkFBQSwrQ0FBRTtnQkFBbkIsSUFBSSxJQUFJLGtCQUFBO2dCQUNULElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFBRSxPQUFPLEtBQUssQ0FBQzthQUM5Qzs7Ozs7Ozs7O1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFTSxnQ0FBeUI7Ozs7SUFBaEMsVUFBaUMsR0FBRztRQUNoQyxPQUFPLEdBQUcsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLEVBQUUsQ0FBQyxDQUFBO0lBQy9ELENBQUM7SUFFSCxhQUFDO0FBQUQsQ0FBQyxBQXhCRCxJQXdCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGNsb25lRGVlcCB9IGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgeyBGaWx0ZXIgfSBmcm9tICcuLi9tb2RlbHMvZmlsdGVyLm1vZGVsJztcbmltcG9ydCB7IEZpbHRlck9wdGlvbnMgfSBmcm9tICcuLi9pbnRlcmZhY2VzL2ZpbHRlci1vcHRpb25zLmludGVyZmFjZSc7XG5pbXBvcnQgeyBUZXJyaXRvcnkgfSBmcm9tICcuLi9pbnRlcmZhY2VzL3RlcnJpdG9yeS5pbnRlcmZhY2UnO1xuaW1wb3J0IHsgQ29udHJhY3QgfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NvbnRyYWN0LmludGVyZmFjZSc7XG5pbXBvcnQgeyBDb21tdW5lIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jb21tdW5lLmludGVyZmFjZSc7XG5pbXBvcnQgeyBGYXZvcml0ZSB9IGZyb20gJy4uL21vZGVscy9mYXZvcml0ZS5tb2RlbCc7XG5pbXBvcnQgeyBJbmRleCB9IGZyb20gJy4uL2ludGVyZmFjZXMvaW5kZXguaW50ZXJmYWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndGVsZW8tZmlsdGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpbHRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2ZpbHRlci5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBGaWx0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgnb3B0aW9ucycpIG9wdGlvbnM6IEZpbHRlck9wdGlvbnMgPSBudWxsOyAvLyBkb24ndCBtb2RpZnkgdGhpcyBvYmplY3QsIGV4Y2VwdCB0aGUgXCIuaGlkZGVuXCIgcHJvcGVydGllc1xuICBASW5wdXQoJ2ZpbHRlcicpIGZpbHRlcjogRmlsdGVyO1xuICBASW5wdXQoJ2Zhdm9yaXRlcycpIGZhdm9yaXRlczogSW5kZXg8RmF2b3JpdGU+O1xuICBASW5wdXQoJ2lzRmV0Y2hpbmdGYXZvcml0ZXMkJykgaXNGZXRjaGluZ0Zhdm9yaXRlcyQ6IE9ic2VydmFibGU8Ym9vbGVhbj47XG5cbiAgQE91dHB1dCgnZmlsdGVyQ2hhbmdlZCcpIGZpbHRlckNoYW5nZWQgPSBuZXcgRXZlbnRFbWl0dGVyPEZpbHRlcj4oKTtcbiAgQE91dHB1dCgnZGVsZXRlRmF2b3JpdGUnKSBkZWxldGVGYXZvcml0ZSA9IG5ldyBFdmVudEVtaXR0ZXI8RmF2b3JpdGU+KCk7XG4gIEBPdXRwdXQoJ3NhdmVBc0Zhdm9yaXRlJykgc2F2ZUFzRmF2b3JpdGUgPSBuZXcgRXZlbnRFbWl0dGVyPEZpbHRlcj4oKTtcblxuICBzb3J0ZWRPcHRpb25zOiBGaWx0ZXJPcHRpb25zID0gbnVsbDtcbiAgZGV2TW9kZSA9IGZhbHNlO1xuICBjbG9zZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgc2VsZWN0ZWRUYWIgPSAwO1xuICBmb3JtOiBhbnkgPSB7XG4gICAgY29udHJhY3RzOiBbXSxcbiAgICByZWdpb25zOiBbXSxcbiAgICB0ZXJyaXRvcmllczogW10sXG4gICAgY29tbXVuZXM6IFtdLFxuICAgIHNlYXJjaFJlZ2lvbnM6ICcnLFxuICAgIHNlYXJjaFRlcnJpdG9yaWVzOiAnJyxcbiAgICBzZWFyY2hDb250cmFjdHM6ICcnLFxuICAgIHNlYXJjaENvbW11bmVzOiAnJyxcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXMpIHtcbiAgICBpZiAoY2hhbmdlcy5maWx0ZXIpIHRoaXMuc2V0Rm9ybSh0aGlzLmZpbHRlcik7XG4gICAgaWYgKGNoYW5nZXMub3B0aW9ucykge1xuICAgICAgdGhpcy5vcHRpb25zID0gY2xvbmVEZWVwKHRoaXMub3B0aW9ucyk7XG4gICAgICB0aGlzLnVwZGF0ZU9wdGlvbnMoKTtcbiAgICB9XG4gIH1cblxuICBpc1N1Ym1pdHRhYmxlKCkge1xuICAgIHJldHVybiB0aGlzLmZvcm0ucmVnaW9ucy5sZW5ndGggfHwgdGhpcy5mb3JtLnRlcnJpdG9yaWVzLmxlbmd0aCB8fCB0aGlzLmZvcm0uY29udHJhY3RzLmxlbmd0aDtcbiAgfVxuXG4gIHNldEZvcm0oZmlsdGVyOiBGaWx0ZXIpIHtcbiAgICB0aGlzLmZvcm0uY29udHJhY3RzID0gZmlsdGVyLmdldENvbnRyYWN0cygpXG4gICAgdGhpcy5mb3JtLnJlZ2lvbnMgPSBmaWx0ZXIuZ2V0UmVnaW9ucygpXG4gICAgdGhpcy5mb3JtLnRlcnJpdG9yaWVzID0gZmlsdGVyLmdldFRlcnJpdG9yaWVzKClcbiAgICB0aGlzLmZvcm0uY29tbXVuZXMgPSBmaWx0ZXIuZ2V0Q29tbXVuZXMoKVxuICAgIHRoaXMub25DaGFuZ2UoKVxuICB9XG5cblxuICBvbkNoYW5nZSgpIHtcbiAgICAvLyBjbGVhbiB1c2VyIHNlbGVjdGlvbiAocmVtb3ZlIGltcG9zc2libGUgY2FzZXMpXG4gICAgdGhpcy5jbGVhblNlbGVjdGlvbigpXG4gICAgdGhpcy51cGRhdGVPcHRpb25zKClcbiAgfVxuXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbiAgY2xlYW5TZWxlY3Rpb24oKTogdm9pZCB7XG5cbiAgICBpZiAoIXRoaXMub3B0aW9ucykgcmV0dXJuXG5cbiAgICBsZXQgZmlsdGVyID0gbmV3IEZpbHRlcih0aGlzLmZvcm0pXG4gICAgbGV0IGNsZWFuZWRGaWx0ZXIgPSBjbG9uZURlZXAoZmlsdGVyKVxuXG4gICAgbGV0IGN1cnJlbnRUZXJyaXRvcmllczogVGVycml0b3J5W10gPSBjbGVhbmVkRmlsdGVyLmdldFRlcnJpdG9yaWVzKCkubWFwKGlkID0+IHRoaXMub3B0aW9ucy50ZXJyaXRvcmllcy5maW5kKHQgPT4gdC5pZCA9PSBpZCkpO1xuICAgIGxldCBjdXJyZW50Q29udHJhY3RzOiBDb250cmFjdFtdID0gY2xlYW5lZEZpbHRlci5nZXRDb250cmFjdHMoKS5tYXAoaWQgPT4gdGhpcy5vcHRpb25zLmNvbnRyYWN0cy5maW5kKGMgPT4gYy5pZCA9PSBpZCkpO1xuICAgIGxldCBjdXJyZW50Q29tbXVuZXM6IENvbW11bmVbXSA9IGNsZWFuZWRGaWx0ZXIuZ2V0Q29tbXVuZXMoKS5tYXAoaWQgPT4gdGhpcy5vcHRpb25zLmNvbW11bmVzLmZpbmQoYyA9PiBjLmlkID09IGlkKSk7XG5cbiAgICAvLyBmaWx0ZXIgYnkgcmVnaW9uc1xuICAgIGlmIChjbGVhbmVkRmlsdGVyLmdldFJlZ2lvbnMoKS5sZW5ndGgpIHtcbiAgICAgIGNsZWFuZWRGaWx0ZXIuc2V0VGVycml0b3JpZXMoY3VycmVudFRlcnJpdG9yaWVzLmZpbHRlcigodCkgPT4gY2xlYW5lZEZpbHRlci5nZXRSZWdpb25zKCkuaW5jbHVkZXModC5yZWdpb25faWQpKS5tYXAodCA9PiB0LmlkKSlcbiAgICAgIGNsZWFuZWRGaWx0ZXIuc2V0Q29udHJhY3RzKGN1cnJlbnRDb250cmFjdHMuZmlsdGVyKChjKSA9PiBjbGVhbmVkRmlsdGVyLmdldFJlZ2lvbnMoKS5pbmNsdWRlcyhjLnJlZ2lvbl9pZCkpLm1hcChjID0+IGMuaWQpKVxuICAgICAgY2xlYW5lZEZpbHRlci5zZXRDb21tdW5lcyhjdXJyZW50Q29tbXVuZXMuZmlsdGVyKChjKSA9PiBjbGVhbmVkRmlsdGVyLmdldFJlZ2lvbnMoKS5pbmNsdWRlcyhjLnJlZ2lvbl9pZCkpLm1hcChjID0+IGMuaWQpKVxuICAgIH1cbiAgICAvLyBmaWx0ZXIgYnkgdGVycml0b3JpZXNcbiAgICBpZiAoY2xlYW5lZEZpbHRlci5nZXRUZXJyaXRvcmllcygpLmxlbmd0aCkge1xuICAgICAgY2xlYW5lZEZpbHRlci5zZXRDb250cmFjdHMoY3VycmVudENvbnRyYWN0cy5maWx0ZXIoKGMpID0+IGNsZWFuZWRGaWx0ZXIuZ2V0VGVycml0b3JpZXMoKS5pbmNsdWRlcyhjLnRlcnJpdG9yeV9pZCkpLm1hcChjID0+IGMuaWQpKVxuICAgICAgY2xlYW5lZEZpbHRlci5zZXRDb21tdW5lcyhjdXJyZW50Q29tbXVuZXMuZmlsdGVyKChjKSA9PiBjbGVhbmVkRmlsdGVyLmdldFRlcnJpdG9yaWVzKCkuaW5jbHVkZXMoYy50ZXJyaXRvcnlfaWQpKS5tYXAoYyA9PiBjLmlkKSlcbiAgICB9XG4gICAgLy8gZmlsdGVyIGJ5IGNvbnRyYWN0c1xuICAgIGlmIChjbGVhbmVkRmlsdGVyLmdldENvbnRyYWN0cygpLmxlbmd0aCkge1xuICAgICAgY2xlYW5lZEZpbHRlci5zZXRDb21tdW5lcyhjdXJyZW50Q29tbXVuZXMuZmlsdGVyKChjKSA9PiBjbGVhbmVkRmlsdGVyLmdldENvbnRyYWN0cygpLmluY2x1ZGVzKGMuY29udHJhY3RfaWQpKS5tYXAoYyA9PiBjLmlkKSlcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgZm9ybSBpZiB0aGUgY2xlYW5lZCB2ZXJzaW9uIG9mIHRoZSBmaWx0ZXIgaXMgZGlmZmVyZW50XG4gICAgaWYgKCFjbGVhbmVkRmlsdGVyLmlzRXF1YWxUbyhmaWx0ZXIpKSB0aGlzLnNldEZvcm0oY2xlYW5lZEZpbHRlcilcbiAgfVxuXG5cblxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gIHVwZGF0ZU9wdGlvbnMoKSB7XG5cbiAgICBpZiAoIXRoaXMub3B0aW9ucykgcmV0dXJuO1xuXG4gICAgbGV0IGZpbHRlciA9IG5ldyBGaWx0ZXIodGhpcy5mb3JtKTtcbiAgICBjb25zdCBmaWx0ZXJSZWdpb25zID0gZmlsdGVyLmdldFJlZ2lvbnMoKTtcbiAgICBjb25zdCBmaWx0ZXJUZXJyaXRvcmllcyA9IGZpbHRlci5nZXRUZXJyaXRvcmllcygpO1xuICAgIGNvbnN0IGZpbHRlckNvbnRyYWN0cyA9IGZpbHRlci5nZXRDb250cmFjdHMoKTtcblxuICAgIC8vIGZpbHRlciByZWdpb24gb3B0aW9uc1xuICAgIHRoaXMub3B0aW9ucy5yZWdpb25zLm1hcChyID0+IHtcbiAgICAgIGNvbnN0IHBhc3NTZWFyY2hGaWx0ZXIgPSAhdGhpcy5mb3JtLnNlYXJjaFJlZ2lvbnMubGVuZ3RoIHx8IChTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hSZWdpb25zLCByLm5hbWUpIHx8IFN0cmluZy5zZWFyY2godGhpcy5mb3JtLnNlYXJjaFJlZ2lvbnMsIHIuaWQpKVxuICAgICAgY29uc3QgcGFzcyA9IHBhc3NTZWFyY2hGaWx0ZXI7XG4gICAgICByLmhpZGRlbiA9ICFwYXNzO1xuICAgICAgcmV0dXJuIHI7XG4gICAgfSlcblxuICAgIC8vIGZpbHRlciB0ZXJyaXRvcnkgb3B0aW9uc1xuICAgIHRoaXMub3B0aW9ucy50ZXJyaXRvcmllcy5tYXAodCA9PiB7XG4gICAgICBjb25zdCBwYXNzUmVnaW9uRmlsdGVyID0gIWZpbHRlclJlZ2lvbnMubGVuZ3RoIHx8IGZpbHRlclJlZ2lvbnMuaW5jbHVkZXModC5yZWdpb25faWQpXG4gICAgICBjb25zdCBwYXNzU2VhcmNoRmlsdGVyID0gIXRoaXMuZm9ybS5zZWFyY2hUZXJyaXRvcmllcy5sZW5ndGggfHwgKFN0cmluZy5zZWFyY2godGhpcy5mb3JtLnNlYXJjaFRlcnJpdG9yaWVzLCB0Lm5hbWUpIHx8IFN0cmluZy5zZWFyY2godGhpcy5mb3JtLnNlYXJjaFRlcnJpdG9yaWVzLCB0LmlkKSlcbiAgICAgIGNvbnN0IHBhc3MgPSBwYXNzUmVnaW9uRmlsdGVyICYmIHBhc3NTZWFyY2hGaWx0ZXI7XG4gICAgICBjb25zdCBoaWRkZW4gPSAhcGFzcztcbiAgICAgIGlmICh0LmhpZGRlbiAhPT0gaGlkZGVuKSB0LmhpZGRlbiA9IGhpZGRlbjtcblxuICAgICAgcmV0dXJuIHQ7XG4gICAgfSlcblxuICAgIC8vIGZpbHRlciBjb250cmFjdCBvcHRpb25zXG4gICAgdGhpcy5vcHRpb25zLmNvbnRyYWN0cy5tYXAoYyA9PiB7XG4gICAgICBjb25zdCBwYXNzUmVnaW9uRmlsdGVyID0gIWZpbHRlclJlZ2lvbnMubGVuZ3RoIHx8IGZpbHRlclJlZ2lvbnMuaW5jbHVkZXMoYy5yZWdpb25faWQpXG4gICAgICBjb25zdCBwYXNzVGVycml0b3J5RmlsdGVyID0gIWZpbHRlclRlcnJpdG9yaWVzLmxlbmd0aCB8fCBmaWx0ZXJUZXJyaXRvcmllcy5pbmNsdWRlcyhjLnRlcnJpdG9yeV9pZClcbiAgICAgIGNvbnN0IHBhc3NTZWFyY2hGaWx0ZXIgPSAhdGhpcy5mb3JtLnNlYXJjaENvbnRyYWN0cy5sZW5ndGggfHwgKFN0cmluZy5zZWFyY2godGhpcy5mb3JtLnNlYXJjaENvbnRyYWN0cywgYy5uYW1lKSB8fCBTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hDb250cmFjdHMsIGMuaWQpKVxuICAgICAgY29uc3QgcGFzcyA9IHBhc3NSZWdpb25GaWx0ZXIgJiYgcGFzc1RlcnJpdG9yeUZpbHRlciAmJiBwYXNzU2VhcmNoRmlsdGVyO1xuICAgICAgY29uc3QgaGlkZGVuID0gIXBhc3M7XG4gICAgICBpZiAoYy5oaWRkZW4gIT09IGhpZGRlbikgYy5oaWRkZW4gPSBoaWRkZW47XG4gICAgICByZXR1cm4gYztcbiAgICB9KVxuXG4gICAgLy8gZmlsdGVyIGNvbW11bmUgb3B0aW9uc1xuICAgIHRoaXMub3B0aW9ucy5jb21tdW5lcy5tYXAoYyA9PiB7XG4gICAgICBjb25zdCBwYXNzUmVnaW9uRmlsdGVyID0gIWZpbHRlclJlZ2lvbnMubGVuZ3RoIHx8IGZpbHRlclJlZ2lvbnMuaW5jbHVkZXMoYy5yZWdpb25faWQpXG4gICAgICBjb25zdCBwYXNzVGVycml0b3J5RmlsdGVyID0gIWZpbHRlclRlcnJpdG9yaWVzLmxlbmd0aCB8fCBmaWx0ZXJUZXJyaXRvcmllcy5pbmNsdWRlcyhjLnRlcnJpdG9yeV9pZClcbiAgICAgIGNvbnN0IHBhc3NDb250cmFjdEZpbHRlciA9ICFmaWx0ZXJDb250cmFjdHMubGVuZ3RoIHx8IGZpbHRlckNvbnRyYWN0cy5pbmNsdWRlcyhjLmNvbnRyYWN0X2lkKVxuICAgICAgY29uc3QgcGFzc1NlYXJjaEZpbHRlciA9ICF0aGlzLmZvcm0uc2VhcmNoQ29tbXVuZXMubGVuZ3RoIHx8IChTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hDb21tdW5lcywgYy5uYW1lKSB8fCBTdHJpbmcuc2VhcmNoKHRoaXMuZm9ybS5zZWFyY2hDb21tdW5lcywgYy5pZCkpXG4gICAgICBjb25zdCBwYXNzID0gcGFzc1JlZ2lvbkZpbHRlciAmJiBwYXNzVGVycml0b3J5RmlsdGVyICYmIHBhc3NDb250cmFjdEZpbHRlciAmJiBwYXNzU2VhcmNoRmlsdGVyO1xuICAgICAgY29uc3QgaGlkZGVuID0gIXBhc3M7XG4gICAgICBpZiAoYy5oaWRkZW4gIT09IGhpZGRlbikgYy5oaWRkZW4gPSBoaWRkZW47XG4gICAgICByZXR1cm4gYztcbiAgICB9KVxuXG4gICAgXG4gICAgLy8gQmVsb3csIHdlIHB1dCBoaWRkZW4gb3B0aW9ucyBhdCB0aGUgZW5kLCBzbyB0aGF0IG1hdC1zZWxlY3QncyBzY3JvbGxpbmcgYmVoYXZlcyBjb3JyZWN0bHlcbiAgICAvLyAxLiB0aGlzIHdheSB3ZSBhbHNvIGNhbiB1c2UgXCJ2aXNpYmlsaXR5XCIgY3NzIHByb3BlcnR5IHdoaWNoIGlzIGZhc3RlciB0aGFuICpuZ0lmIHdoaWNoIHdvdWxkIGluc2VydC9yZW1vdmVzIERPTSBub2Rlc1xuICAgIC8vIDIuIHRoaXMgd2F5LCB1c2luZyB0aGUgc2VhcmNoIGZpZWxkIGRvZXNuJ3QgcmVtb3ZlIG91ciBwcmV2aW91cyBzZWxlY3Rpb25cbiAgICB0aGlzLnNvcnRlZE9wdGlvbnMgPSB7XG4gICAgICByZWdpb25zOiB0aGlzLm9wdGlvbnMucmVnaW9ucy5zb3J0KChhLGIpID0+IChOdW1iZXIoYS5oaWRkZW4pIC0gTnVtYmVyKGIuaGlkZGVuKSkgfHwgYS5uYW1lLmxvY2FsZUNvbXBhcmUoYi5uYW1lLCAnZnInLCB7c2Vuc2l0aXZpdHk6IFwiYmFzZVwifSkpLFxuICAgICAgdGVycml0b3JpZXM6IHRoaXMub3B0aW9ucy50ZXJyaXRvcmllcy5zb3J0KChhLGIpID0+IChOdW1iZXIoYS5oaWRkZW4pIC0gTnVtYmVyKGIuaGlkZGVuKSkgfHwgIGEubmFtZS5sb2NhbGVDb21wYXJlKGIubmFtZSwgJ2ZyJywge3NlbnNpdGl2aXR5OiBcImJhc2VcIn0pKSxcbiAgICAgIGNvbnRyYWN0czogdGhpcy5vcHRpb25zLmNvbnRyYWN0cy5zb3J0KChhLGIpID0+IChOdW1iZXIoYS5oaWRkZW4pIC0gTnVtYmVyKGIuaGlkZGVuKSkgfHwgIGEubmFtZS5sb2NhbGVDb21wYXJlKGIubmFtZSwgJ2ZyJywge3NlbnNpdGl2aXR5OiBcImJhc2VcIn0pKSxcbiAgICAgIGNvbW11bmVzOiB0aGlzLm9wdGlvbnMuY29tbXVuZXMuc29ydCgoYSxiKSA9PiAoTnVtYmVyKGEuaGlkZGVuKSAtIE51bWJlcihiLmhpZGRlbikpIHx8ICBhLm5hbWUubG9jYWxlQ29tcGFyZShiLm5hbWUsICdmcicsIHtzZW5zaXRpdml0eTogXCJiYXNlXCJ9KSksXG4gICAgfVxuICB9XG5cbiAgdHJhY2tFbGVtZW50KGluZGV4OiBudW1iZXIsIGVsZW1lbnQ6IGFueSkge1xuICAgIHJldHVybiBlbGVtZW50ID8gZWxlbWVudC5pZCA6IG51bGxcbiAgfVxuXG4gIC8vIFNFTEVDVCAvIERFU0VMRUNUIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbiAgc2VsZWN0QWxsUmVnaW9ucygpIHtcbiAgICBpZiAoIXRoaXMub3B0aW9ucykgcmV0dXJuO1xuICAgIHRoaXMuZm9ybS5yZWdpb25zID0gdGhpcy5vcHRpb25zLnJlZ2lvbnMubWFwKGl0ZW0gPT4gaXRlbS5pZCk7XG4gICAgdGhpcy5vbkNoYW5nZSgpO1xuICB9XG5cbiAgZGVzZWxlY3RBbGxSZWdpb25zKCkge1xuICAgIHRoaXMuZm9ybS5yZWdpb25zID0gW107XG4gICAgdGhpcy5mb3JtLnRlcnJpdG9yaWVzID0gW107XG4gICAgdGhpcy5mb3JtLmNvbnRyYWN0cyA9IFtdO1xuICAgIHRoaXMuZm9ybS5jb21tdW5lcyA9IFtdO1xuICAgIHRoaXMub25DaGFuZ2UoKTtcbiAgfVxuXG4gIHNlbGVjdEFsbFRlcnJpdG9yaWVzKCkge1xuICAgIGlmICghdGhpcy5vcHRpb25zKSByZXR1cm47XG4gICAgdGhpcy5mb3JtLnRlcnJpdG9yaWVzID0gdGhpcy5vcHRpb25zLnRlcnJpdG9yaWVzLm1hcChpdGVtID0+IGl0ZW0uaWQpO1xuICAgIHRoaXMub25DaGFuZ2UoKTtcbiAgfVxuXG4gIGRlc2VsZWN0QWxsVGVycml0b3JpZXMoKSB7XG4gICAgdGhpcy5mb3JtLnRlcnJpdG9yaWVzID0gW107XG4gICAgdGhpcy5mb3JtLmNvbnRyYWN0cyA9IFtdO1xuICAgIHRoaXMuZm9ybS5jb21tdW5lcyA9IFtdO1xuICAgIHRoaXMub25DaGFuZ2UoKTtcbiAgfVxuXG4gIHNlbGVjdEFsbENvbnRyYWN0cygpIHtcbiAgICBpZiAoIXRoaXMub3B0aW9ucykgcmV0dXJuO1xuICAgIHRoaXMuZm9ybS5jb250cmFjdHMgPSB0aGlzLm9wdGlvbnMuY29udHJhY3RzLm1hcChpdGVtID0+IGl0ZW0uaWQpO1xuICAgIHRoaXMub25DaGFuZ2UoKTtcbiAgfVxuXG4gIGRlc2VsZWN0QWxsQ29udHJhY3RzKCkge1xuICAgIHRoaXMuZm9ybS5jb250cmFjdHMgPSBbXTtcbiAgICB0aGlzLmZvcm0uY29tbXVuZXMgPSBbXTtcbiAgICB0aGlzLm9uQ2hhbmdlKCk7XG4gIH1cblxuXG5cbiAgX2hhbmRsZUtleWRvd24oZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgfVxuXG4gIGdldFJlZ2lvbnNQbGFjZWhvbGRlcigpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9wdGlvbnMgPSB0aGlzLmZvcm0gJiYgKHRoaXMuZm9ybS5yZWdpb25zIHx8IFtdKS5sZW5ndGhcbiAgICBjb25zdCBhdmFpbGFibGVPcHRpb25zID0gdGhpcy5jb3VudFJlZ2lvbnNPcHRpb25zKCk7XG4gICAgcmV0dXJuIHRoaXMuZGV2TW9kZSA/IGBSw6lnaW9ucyAoJHtzZWxlY3RlZE9wdGlvbnN9LyR7YXZhaWxhYmxlT3B0aW9uc30pYCA6IGBSw6lnaW9ucyAoJHtzZWxlY3RlZE9wdGlvbnN9KWBcbiAgfVxuXG4gIGdldFRlcnJpdG9yaWVzUGxhY2Vob2xkZXIoKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPcHRpb25zID0gdGhpcy5mb3JtICYmICh0aGlzLmZvcm0udGVycml0b3JpZXMgfHwgW10pLmxlbmd0aFxuICAgIGNvbnN0IGF2YWlsYWJsZU9wdGlvbnMgPSB0aGlzLmNvdW50VGVycml0b3JpZXNPcHRpb25zKCk7XG4gICAgcmV0dXJuIHRoaXMuZGV2TW9kZSA/IGBUZXJyaXRvaXJlcyAoJHtzZWxlY3RlZE9wdGlvbnN9LyR7YXZhaWxhYmxlT3B0aW9uc30pYCA6IGBUZXJyaXRvaXJlcyAoJHtzZWxlY3RlZE9wdGlvbnN9KWBcbiAgfVxuXG4gIGdldENvbnRyYWN0c1BsYWNlaG9sZGVyKCkge1xuICAgIGNvbnN0IHNlbGVjdGVkT3B0aW9ucyA9IHRoaXMuZm9ybSAmJiAodGhpcy5mb3JtLmNvbnRyYWN0cyB8fCBbXSkubGVuZ3RoXG4gICAgY29uc3QgYXZhaWxhYmxlT3B0aW9ucyA9IHRoaXMuY291bnRDb250cmFjdHNPcHRpb25zKCk7XG4gICAgcmV0dXJuIHRoaXMuZGV2TW9kZSA/IGBDb250cmF0cyAoJHtzZWxlY3RlZE9wdGlvbnN9LyR7YXZhaWxhYmxlT3B0aW9uc30pYCA6IGBDb250cmF0cyAoJHtzZWxlY3RlZE9wdGlvbnN9KWBcbiAgfVxuXG4gIGNvdW50UmVnaW9uc09wdGlvbnMoKSB7XG4gICAgaWYgKCF0aGlzLm9wdGlvbnMpIHJldHVybiAwO1xuICAgIHJldHVybiB0aGlzLm9wdGlvbnMucmVnaW9ucy5maWx0ZXIoaXRlbSA9PiAhaXRlbS5oaWRkZW4pLmxlbmd0aFxuICB9XG5cbiAgY291bnRUZXJyaXRvcmllc09wdGlvbnMoKSB7XG4gICAgaWYgKCF0aGlzLm9wdGlvbnMpIHJldHVybiAwO1xuICAgIHJldHVybiB0aGlzLm9wdGlvbnMudGVycml0b3JpZXMuZmlsdGVyKGl0ZW0gPT4gIWl0ZW0uaGlkZGVuKS5sZW5ndGhcbiAgfVxuXG4gIGNvdW50Q29udHJhY3RzT3B0aW9ucygpIHtcbiAgICBpZiAoIXRoaXMub3B0aW9ucykgcmV0dXJuIDA7XG4gICAgcmV0dXJuIHRoaXMub3B0aW9ucy5jb250cmFjdHMuZmlsdGVyKGl0ZW0gPT4gIWl0ZW0uaGlkZGVuKS5sZW5ndGhcbiAgfVxuICAvLyBBUFBMWSAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cbiAgZW1wdHlTZWFyY2hGaWVsZChmaWVsZCkge1xuICAgIC8vIGVtcHR5IHNlYXJjaCBmaWVsZHNcbiAgICBpZiAodGhpcy5mb3JtW2ZpZWxkXS5sZW5ndGgpIHtcbiAgICAgIHRoaXMuZm9ybVtmaWVsZF0gPSAnJztcbiAgICAgIHRoaXMudXBkYXRlT3B0aW9ucygpO1xuICAgIH1cbiAgfVxuXG4gIHJlaW5pdCgpIHtcbiAgICBjb25zdCBlbXB0eUZpbHRlciA9IG5ldyBGaWx0ZXIoKTtcblxuICAgIC8vIGVtcHR5IHNlYXJjaCBmaWVsZHNcbiAgICB0aGlzLmZvcm0uc2VhcmNoUmVnaW9ucyA9ICcnXG4gICAgdGhpcy5mb3JtLnNlYXJjaFRlcnJpdG9yaWVzID0gJydcbiAgICB0aGlzLmZvcm0uc2VhcmNoQ29udHJhY3RzID0gJydcbiAgICB0aGlzLmZvcm0uc2VhcmNoQ29tbXVuZXMgPSAnJ1xuXG4gICAgdGhpcy5zZXRGb3JtKGVtcHR5RmlsdGVyKVxuICB9XG5cbiAgX3NhdmVBc0Zhdm9yaXRlKCkge1xuICAgIGxldCBmaWx0ZXIgPSBuZXcgRmlsdGVyKHRoaXMuZm9ybSk7XG4gICAgdGhpcy5zYXZlQXNGYXZvcml0ZS5lbWl0KGZpbHRlcik7XG4gIH1cblxuICBfZGVsZXRlRmF2b3JpdGUoZmF2b3JpdGU6IEZhdm9yaXRlKSB7XG4gICAgdGhpcy5kZWxldGVGYXZvcml0ZS5lbWl0KGZhdm9yaXRlKVxuXG4gIH1cblxuXG4gIGFwcGx5RmlsdGVyKCkge1xuICAgIGxldCBmaWx0ZXIgPSBuZXcgRmlsdGVyKHRoaXMuZm9ybSk7XG4gICAgdGhpcy5maWx0ZXJDaGFuZ2VkLmVtaXQoZmlsdGVyKTtcbiAgfVxuXG4gIGFwcGx5RmF2b3JpdGUoZmF2b3JpdGU6IEZhdm9yaXRlKSB7XG4gICAgdGhpcy5zZXRGb3JtKGZhdm9yaXRlLmdldEZpbHRlcigpKTtcbiAgICB0aGlzLmFwcGx5RmlsdGVyKCk7XG4gICAgdGhpcy5nb1RvRmlsdGVyVGFiKCk7XG4gIH1cblxuXG4gIGdvVG9GaWx0ZXJUYWIoKSB7XG4gICAgdGhpcy5zZWxlY3RlZFRhYiA9IDA7XG4gIH1cblxuXG4gIGNsb3NlTmF2KCkge1xuICAgIHRoaXMuY2xvc2VkID0gdHJ1ZTtcbiAgfVxuXG4gIG9wZW5OYXYoKSB7XG4gICAgdGhpcy5jbG9zZWQgPSBmYWxzZTtcbiAgfVxuXG59XG5cbmNsYXNzIFN0cmluZyB7XG5cblxuICAvKipcbiAgICogcmV0dXJuIHRydWUgaWYgZWFjaCB0ZXJtcyAoc3BhY2Utc2VwYXJhdGVkKSBvZiAkbmVlZGxlXG4gICAqIGFyZSBwcmVzZW50IGluICRoYXlzdGFja1xuICAgKi9cbiAgc3RhdGljIHNlYXJjaChuZWVkbGU6IHN0cmluZywgaGF5c3RhY2s6IHN0cmluZykge1xuICAgICAgLy8gY2xlYW4gc3RyaW5nc1xuICAgICAgbmVlZGxlID0gdGhpcy5yZXBsYWNlQWNjZW50ZWRDaGFyYWN0ZXJzKG5lZWRsZS50b0xvd2VyQ2FzZSgpKVxuICAgICAgaGF5c3RhY2sgPSB0aGlzLnJlcGxhY2VBY2NlbnRlZENoYXJhY3RlcnMoaGF5c3RhY2sudG9Mb3dlckNhc2UoKSk7XG4gICAgICAvLyBzcGxpdCBzdHJpbmcgaW4gc2VhcmNoIHRlcm1zXG4gICAgICBjb25zdCB0ZXJtcyA9IG5lZWRsZS5zcGxpdCgvXFxzKy8pO1xuICAgICAgLy8gY2hlY2sgZWFjaCB0ZXJtIGlzIHByZXNlbnRcbiAgICAgIGZvciAobGV0IHRlcm0gb2YgdGVybXMpIHtcbiAgICAgICAgICBpZiAoIWhheXN0YWNrLmluY2x1ZGVzKHRlcm0pKSByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHN0YXRpYyByZXBsYWNlQWNjZW50ZWRDaGFyYWN0ZXJzKHN0cikge1xuICAgICAgcmV0dXJuIHN0ci5ub3JtYWxpemUoXCJORkRcIikucmVwbGFjZSgvW1xcdTAzMDAtXFx1MDM2Zl0vZywgXCJcIilcbiAgfVxuXG59Il19