/**
 * @fileoverview added by tsickle
 * Generated from: lib/interfaces/region.interface.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function Region() { }
if (false) {
    /** @type {?} */
    Region.prototype.id;
    /** @type {?} */
    Region.prototype.name;
    /** @type {?|undefined} */
    Region.prototype.hidden;
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaW9uLmludGVyZmFjZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B0ZWxlby9jb21tb24vIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlcy9yZWdpb24uaW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsNEJBSUM7OztJQUhHLG9CQUFXOztJQUNYLHNCQUFhOztJQUNiLHdCQUFpQjs7QUFDcEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgUmVnaW9uIHtcclxuICAgIGlkOiBzdHJpbmc7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICBoaWRkZW4/OiBib29sZWFuO1xyXG59O1xyXG4iXX0=