/**
 * @fileoverview added by tsickle
 * Generated from: lib/interfaces/territory.interface.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function Territory() { }
if (false) {
    /** @type {?} */
    Territory.prototype.id;
    /** @type {?} */
    Territory.prototype.name;
    /** @type {?} */
    Territory.prototype.region_id;
    /** @type {?|undefined} */
    Territory.prototype.hidden;
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVycml0b3J5LmludGVyZmFjZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B0ZWxlby9jb21tb24vIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlcy90ZXJyaXRvcnkuaW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsK0JBS0M7OztJQUpHLHVCQUFXOztJQUNYLHlCQUFhOztJQUNiLDhCQUFrQjs7SUFDbEIsMkJBQWlCOztBQUNwQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBUZXJyaXRvcnkge1xyXG4gICAgaWQ6IHN0cmluZztcclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIHJlZ2lvbl9pZDogc3RyaW5nO1xyXG4gICAgaGlkZGVuPzogYm9vbGVhbjtcclxufTtcclxuIl19