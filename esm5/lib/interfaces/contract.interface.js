/**
 * @fileoverview added by tsickle
 * Generated from: lib/interfaces/contract.interface.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function Contract() { }
if (false) {
    /** @type {?} */
    Contract.prototype.id;
    /** @type {?} */
    Contract.prototype.name;
    /** @type {?} */
    Contract.prototype.region_id;
    /** @type {?} */
    Contract.prototype.territory_id;
    /** @type {?|undefined} */
    Contract.prototype.hidden;
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udHJhY3QuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHRlbGVvL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmZhY2VzL2NvbnRyYWN0LmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLDhCQU1DOzs7SUFMRyxzQkFBVzs7SUFDWCx3QkFBYTs7SUFDYiw2QkFBa0I7O0lBQ2xCLGdDQUFvQjs7SUFDcEIsMEJBQWlCOztBQUNwQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBDb250cmFjdCB7XHJcbiAgICBpZDogc3RyaW5nO1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG4gICAgcmVnaW9uX2lkOiBzdHJpbmc7XHJcbiAgICB0ZXJyaXRvcnlfaWQ6IHN0cmluZ1xyXG4gICAgaGlkZGVuPzogYm9vbGVhbjtcclxufTtcclxuXHJcbiJdfQ==