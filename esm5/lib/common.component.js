/**
 * @fileoverview added by tsickle
 * Generated from: lib/common.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var CommonComponent = /** @class */ (function () {
    function CommonComponent() {
    }
    /**
     * @return {?}
     */
    CommonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    CommonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'teleo-common',
                    template: "\n    <p>\n      common works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    CommonComponent.ctorParameters = function () { return []; };
    return CommonComponent;
}());
export { CommonComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0B0ZWxlby9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tbW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFFbEQ7SUFXRTtJQUFnQixDQUFDOzs7O0lBRWpCLGtDQUFROzs7SUFBUjtJQUNBLENBQUM7O2dCQWRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsUUFBUSxFQUFFLDhDQUlUO2lCQUVGOzs7O0lBUUQsc0JBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQVBZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd0ZWxlby1jb21tb24nLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxwPlxuICAgICAgY29tbW9uIHdvcmtzIVxuICAgIDwvcD5cbiAgYCxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBDb21tb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19