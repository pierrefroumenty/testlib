/**
 * @fileoverview added by tsickle
 * Generated from: lib/models/filter.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// import * as moment from 'moment';
import { pick as _pick } from 'lodash';
var Filter = /** @class */ (function () {
    function Filter(data) {
        if (data === void 0) { data = {}; }
        // this.setDate(data.date || Filter.defaults.date);
        // this.setTypes(data.types || Filter.defaults.type);
        // this.setModes(data.modes || Filter.defaults.modes);
        this.setRegions(data.regions || Filter.defaults.regions);
        this.setTerritories(data.territories || Filter.defaults.territories);
        this.setContracts(data.contracts || Filter.defaults.contracts);
        this.setCommunes(data.communes || Filter.defaults.communes);
    }
    // ------------------------------------------------
    // ------------------------------------------------
    /**
     * @return {?}
     */
    Filter.prototype.isEmpty = 
    // ------------------------------------------------
    /**
     * @return {?}
     */
    function () {
        return (this.getRegions().length === 0
            && this.getTerritories().length === 0
            && this.getContracts().length === 0
            && this.getCommunes().length === 0);
    };
    /**
     * @return {?}
     */
    Filter.prototype.isSingleContract = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var isSingle = (this.getContracts().length === 1 && this.getCommunes().length === 0);
        return isSingle ? this.singleContract() : false;
    };
    /**
     * @return {?}
     */
    Filter.prototype.singleContract = /**
     * @return {?}
     */
    function () {
        return this.getContracts()[0];
    };
    /**
     * @return {?}
     */
    Filter.prototype.isSingleCommune = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var isSingle = (this.getCommunes().length === 1);
        return isSingle ? this.getCommunes()[0] : false;
    };
    // GETTERS -------------------------------------------
    // public getDate() {
    //     return this._date || Filter.defaults.date;
    // }
    // public getDateParam() {
    //     return moment(this.getDate()).format('YYYY-MM-DD');
    // }
    // public getTypes(): string[] {
    //     return this._types || Filter.defaults.types;
    // }
    // public getModes(): string[] {
    //     return this._modes || Filter.defaults.modes;
    // }
    // GETTERS -------------------------------------------
    // public getDate() {
    //     return this._date || Filter.defaults.date;
    // }
    // public getDateParam() {
    //     return moment(this.getDate()).format('YYYY-MM-DD');
    // }
    // public getTypes(): string[] {
    //     return this._types || Filter.defaults.types;
    // }
    // public getModes(): string[] {
    //     return this._modes || Filter.defaults.modes;
    // }
    /**
     * @return {?}
     */
    Filter.prototype.getRegions = 
    // GETTERS -------------------------------------------
    // public getDate() {
    //     return this._date || Filter.defaults.date;
    // }
    // public getDateParam() {
    //     return moment(this.getDate()).format('YYYY-MM-DD');
    // }
    // public getTypes(): string[] {
    //     return this._types || Filter.defaults.types;
    // }
    // public getModes(): string[] {
    //     return this._modes || Filter.defaults.modes;
    // }
    /**
     * @return {?}
     */
    function () {
        return this._regions || Filter.defaults.regions;
    };
    /**
     * @return {?}
     */
    Filter.prototype.getTerritories = /**
     * @return {?}
     */
    function () {
        return this._territories || Filter.defaults.territories;
    };
    /**
     * @return {?}
     */
    Filter.prototype.getContracts = /**
     * @return {?}
     */
    function () {
        return this._contracts || Filter.defaults.contracts;
    };
    /**
     * @return {?}
     */
    Filter.prototype.getCommunes = /**
     * @return {?}
     */
    function () {
        return this._communes || Filter.defaults.communes;
    };
    // SETTERS -------------------------------------------
    // public setDate(date: Date) {
    //     this._date = date;
    // }
    // public setTypes(types: string[]) {
    //     this._types = types;
    // }
    // public setModes(modes: string[]) {
    //     this._modes = modes
    // }
    // SETTERS -------------------------------------------
    // public setDate(date: Date) {
    //     this._date = date;
    // }
    // public setTypes(types: string[]) {
    //     this._types = types;
    // }
    // public setModes(modes: string[]) {
    //     this._modes = modes
    // }
    /**
     * @param {?} regions
     * @return {?}
     */
    Filter.prototype.setRegions = 
    // SETTERS -------------------------------------------
    // public setDate(date: Date) {
    //     this._date = date;
    // }
    // public setTypes(types: string[]) {
    //     this._types = types;
    // }
    // public setModes(modes: string[]) {
    //     this._modes = modes
    // }
    /**
     * @param {?} regions
     * @return {?}
     */
    function (regions) {
        this._regions = regions;
    };
    /**
     * @param {?} territories
     * @return {?}
     */
    Filter.prototype.setTerritories = /**
     * @param {?} territories
     * @return {?}
     */
    function (territories) {
        this._territories = territories;
    };
    /**
     * @param {?} contracts
     * @return {?}
     */
    Filter.prototype.setContracts = /**
     * @param {?} contracts
     * @return {?}
     */
    function (contracts) {
        this._contracts = contracts;
    };
    /**
     * @param {?} communes
     * @return {?}
     */
    Filter.prototype.setCommunes = /**
     * @param {?} communes
     * @return {?}
     */
    function (communes) {
        this._communes = communes;
    };
    /**
     * @param {?} filter
     * @return {?}
     */
    Filter.prototype.isEqualTo = /**
     * @param {?} filter
     * @return {?}
     */
    function (filter) {
        return (this.toJSON() === filter.toJSON());
    };
    /**
     * @return {?}
     */
    Filter.prototype.hasPerimeter = /**
     * @return {?}
     */
    function () {
        return (this.getRegions().length || this.getTerritories().length || this.getContracts().length);
    };
    // SERIALIZATION -------------------------------------------
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    Filter.fromJSON = 
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    function (json) {
        return new Filter(json);
    };
    /**
     * @return {?}
     */
    Filter.prototype.toObject = /**
     * @return {?}
     */
    function () {
        return {
            regions: this.getRegions(),
            territories: this.getTerritories(),
            contracts: this.getContracts(),
            communes: this.getCommunes(),
        };
    };
    /**
     * @return {?}
     */
    Filter.prototype.toJSON = /**
     * @return {?}
     */
    function () {
        return JSON.stringify(this.toObject());
    };
    /**
     * @return {?}
     */
    Filter.prototype.toStorage = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var json = this.toJSON();
        localStorage.setItem("filter", json);
    };
    /**
     * @return {?}
     */
    Filter.fromStorage = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var filter = new Filter();
        // fill from storage
        /** @type {?} */
        var params = localStorage.getItem("filter");
        if (params) {
            params = JSON.parse(params);
            filter.setRegions(params['regions'] || this.defaults.regions);
            filter.setTerritories(params['territories'] || this.defaults.territories);
            filter.setContracts(params['contracts'] || this.defaults.contracts);
            filter.setCommunes(params['communes'] || this.defaults.communes);
            // filter.setDate(new Date(params['date'] || this.defaults.date));
            // filter.setTypes(params['types'] || this.defaults.types);
            // filter.setModes(params['modes'] || this.defaults.modes);
        }
        return filter;
    };
    /**
     * @return {?}
     */
    Filter.prototype.clearStorage = /**
     * @return {?}
     */
    function () {
        localStorage.removeItem("filter");
    };
    // QUERY PARAMS --------------------------------------------------------
    // QUERY PARAMS --------------------------------------------------------
    /**
     * @param {?=} fields
     * @return {?}
     */
    Filter.prototype.toParams = 
    // QUERY PARAMS --------------------------------------------------------
    /**
     * @param {?=} fields
     * @return {?}
     */
    function (fields) {
        if (fields === void 0) { fields = null; }
        /** @type {?} */
        var params = {
            // day: this.getDateParam(),
            // types: this.getTypes().join(','),
            // modes: this.getModes().join(','),
            regions: this.getRegions().join(','),
            territories: this.getTerritories().join(','),
            contracts: this.getContracts().join(','),
            communes: this.getCommunes().join(','),
        }
        // pick the fields we want
        ;
        // pick the fields we want
        if (fields)
            params = _pick(params, fields);
        return params;
    };
    Filter.defaults = {
        // date: new Date(),
        // types: ['DBO', 'OM'],
        // modes: ['RR', 'TR'],
        regions: [],
        territories: [],
        contracts: [],
        communes: [],
    };
    return Filter;
}());
export { Filter };
if (false) {
    /**
     * @type {?}
     * @private
     */
    Filter.defaults;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._date;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._types;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._modes;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._regions;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._territories;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._contracts;
    /**
     * @type {?}
     * @private
     */
    Filter.prototype._communes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHRlbGVvL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvZmlsdGVyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLE9BQU8sRUFBRSxJQUFJLElBQUksS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBRXZDO0lBb0JJLGdCQUFZLElBQWM7UUFBZCxxQkFBQSxFQUFBLFNBQWM7UUFDdEIsbURBQW1EO1FBQ25ELHFEQUFxRDtRQUNyRCxzREFBc0Q7UUFDdEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVELG1EQUFtRDs7Ozs7SUFDNUMsd0JBQU87Ozs7O0lBQWQ7UUFDSSxPQUFPLENBQ0gsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDO2VBQzNCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQztlQUNsQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUM7ZUFDaEMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQ3JDLENBQUE7SUFDTCxDQUFDOzs7O0lBR00saUNBQWdCOzs7SUFBdkI7O1lBQ1UsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7UUFDdEYsT0FBTyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFTSwrQkFBYzs7O0lBQXJCO1FBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVNLGdDQUFlOzs7SUFBdEI7O1lBQ1UsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7UUFDbEQsT0FBTyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3BELENBQUM7SUFHRCxzREFBc0Q7SUFFdEQscUJBQXFCO0lBQ3JCLGlEQUFpRDtJQUNqRCxJQUFJO0lBQ0osMEJBQTBCO0lBQzFCLDBEQUEwRDtJQUMxRCxJQUFJO0lBQ0osZ0NBQWdDO0lBQ2hDLG1EQUFtRDtJQUNuRCxJQUFJO0lBQ0osZ0NBQWdDO0lBQ2hDLG1EQUFtRDtJQUNuRCxJQUFJOzs7Ozs7Ozs7Ozs7Ozs7OztJQUNHLDJCQUFVOzs7Ozs7Ozs7Ozs7Ozs7OztJQUFqQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztJQUNwRCxDQUFDOzs7O0lBQ00sK0JBQWM7OztJQUFyQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztJQUM1RCxDQUFDOzs7O0lBQ00sNkJBQVk7OztJQUFuQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUN4RCxDQUFDOzs7O0lBQ00sNEJBQVc7OztJQUFsQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztJQUN0RCxDQUFDO0lBRUQsc0RBQXNEO0lBRXRELCtCQUErQjtJQUMvQix5QkFBeUI7SUFDekIsSUFBSTtJQUNKLHFDQUFxQztJQUNyQywyQkFBMkI7SUFDM0IsSUFBSTtJQUNKLHFDQUFxQztJQUNyQywwQkFBMEI7SUFDMUIsSUFBSTs7Ozs7Ozs7Ozs7Ozs7O0lBQ0csMkJBQVU7Ozs7Ozs7Ozs7Ozs7OztJQUFqQixVQUFrQixPQUFpQjtRQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztJQUM1QixDQUFDOzs7OztJQUNNLCtCQUFjOzs7O0lBQXJCLFVBQXNCLFdBQXFCO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFBO0lBQ25DLENBQUM7Ozs7O0lBQ00sNkJBQVk7Ozs7SUFBbkIsVUFBb0IsU0FBbUI7UUFDbkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7SUFDaEMsQ0FBQzs7Ozs7SUFDTSw0QkFBVzs7OztJQUFsQixVQUFtQixRQUFrQjtRQUNqQyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUM5QixDQUFDOzs7OztJQUdNLDBCQUFTOzs7O0lBQWhCLFVBQWlCLE1BQWM7UUFDM0IsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQTtJQUM5QyxDQUFDOzs7O0lBRU0sNkJBQVk7OztJQUFuQjtRQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3BHLENBQUM7SUFFRCw0REFBNEQ7Ozs7OztJQUU5QyxlQUFROzs7Ozs7SUFBdEIsVUFBdUIsSUFBWTtRQUMvQixPQUFPLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFTSx5QkFBUTs7O0lBQWY7UUFDSSxPQUFRO1lBQ0osT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDMUIsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDbEMsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDOUIsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUU7U0FJL0IsQ0FBQztJQUNOLENBQUM7Ozs7SUFFTSx1QkFBTTs7O0lBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVNLDBCQUFTOzs7SUFBaEI7O1lBQ1EsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUU7UUFDeEIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDekMsQ0FBQzs7OztJQUVhLGtCQUFXOzs7SUFBekI7O1lBRVEsTUFBTSxHQUFHLElBQUksTUFBTSxFQUFFOzs7WUFHckIsTUFBTSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQzNDLElBQUksTUFBTSxFQUFFO1lBQ1IsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDNUIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM5RCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDcEUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRSxrRUFBa0U7WUFDbEUsMkRBQTJEO1lBQzNELDJEQUEyRDtTQUM5RDtRQUNELE9BQU8sTUFBTSxDQUFBO0lBRWpCLENBQUM7Ozs7SUFFTSw2QkFBWTs7O0lBQW5CO1FBQ0ksWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsd0VBQXdFOzs7Ozs7SUFFeEUseUJBQVE7Ozs7OztJQUFSLFVBQVMsTUFBdUI7UUFBdkIsdUJBQUEsRUFBQSxhQUF1Qjs7WUFDeEIsTUFBTSxHQUFHOzs7O1lBSVQsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ3BDLFdBQVcsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUM1QyxTQUFTLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDeEMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1NBQ3pDO1FBRUQsMEJBQTBCOztRQUExQiwwQkFBMEI7UUFDMUIsSUFBSSxNQUFNO1lBQUUsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFMUMsT0FBTyxNQUFNLENBQUM7SUFFbEIsQ0FBQztJQS9LdUIsZUFBUSxHQUFROzs7O1FBSXBDLE9BQU8sRUFBRSxFQUFFO1FBQ1gsV0FBVyxFQUFFLEVBQUU7UUFDZixTQUFTLEVBQUUsRUFBRTtRQUNiLFFBQVEsRUFBRSxFQUFFO0tBQ2YsQ0FBQTtJQTBLTCxhQUFDO0NBQUEsQUE1TEQsSUE0TEM7U0E1TFksTUFBTTs7Ozs7O0lBVWYsZ0JBUUM7Ozs7O0lBaEJELHVCQUFvQjs7Ozs7SUFDcEIsd0JBQXlCOzs7OztJQUN6Qix3QkFBeUI7Ozs7O0lBQ3pCLDBCQUEyQjs7Ozs7SUFDM0IsOEJBQStCOzs7OztJQUMvQiw0QkFBNkI7Ozs7O0lBQzdCLDJCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbIi8vIGltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBwaWNrIGFzIF9waWNrIH0gZnJvbSAnbG9kYXNoJztcclxuXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXIge1xyXG5cclxuICAgIHByaXZhdGUgX2RhdGU6IERhdGU7XHJcbiAgICBwcml2YXRlIF90eXBlczogc3RyaW5nW107XHJcbiAgICBwcml2YXRlIF9tb2Rlczogc3RyaW5nW107XHJcbiAgICBwcml2YXRlIF9yZWdpb25zOiBzdHJpbmdbXTtcclxuICAgIHByaXZhdGUgX3RlcnJpdG9yaWVzOiBzdHJpbmdbXTtcclxuICAgIHByaXZhdGUgX2NvbnRyYWN0czogc3RyaW5nW107XHJcbiAgICBwcml2YXRlIF9jb21tdW5lczogc3RyaW5nW107XHJcblxyXG4gICAgcHJpdmF0ZSBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdHM6IGFueSA9IHtcclxuICAgICAgICAvLyBkYXRlOiBuZXcgRGF0ZSgpLFxyXG4gICAgICAgIC8vIHR5cGVzOiBbJ0RCTycsICdPTSddLFxyXG4gICAgICAgIC8vIG1vZGVzOiBbJ1JSJywgJ1RSJ10sXHJcbiAgICAgICAgcmVnaW9uczogW10sXHJcbiAgICAgICAgdGVycml0b3JpZXM6IFtdLFxyXG4gICAgICAgIGNvbnRyYWN0czogW10sXHJcbiAgICAgICAgY29tbXVuZXM6IFtdLFxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGRhdGE6IGFueSA9IHt9KSB7XHJcbiAgICAgICAgLy8gdGhpcy5zZXREYXRlKGRhdGEuZGF0ZSB8fCBGaWx0ZXIuZGVmYXVsdHMuZGF0ZSk7XHJcbiAgICAgICAgLy8gdGhpcy5zZXRUeXBlcyhkYXRhLnR5cGVzIHx8IEZpbHRlci5kZWZhdWx0cy50eXBlKTtcclxuICAgICAgICAvLyB0aGlzLnNldE1vZGVzKGRhdGEubW9kZXMgfHwgRmlsdGVyLmRlZmF1bHRzLm1vZGVzKTtcclxuICAgICAgICB0aGlzLnNldFJlZ2lvbnMoZGF0YS5yZWdpb25zIHx8IEZpbHRlci5kZWZhdWx0cy5yZWdpb25zKTtcclxuICAgICAgICB0aGlzLnNldFRlcnJpdG9yaWVzKGRhdGEudGVycml0b3JpZXMgfHwgRmlsdGVyLmRlZmF1bHRzLnRlcnJpdG9yaWVzKTtcclxuICAgICAgICB0aGlzLnNldENvbnRyYWN0cyhkYXRhLmNvbnRyYWN0cyB8fCBGaWx0ZXIuZGVmYXVsdHMuY29udHJhY3RzKTtcclxuICAgICAgICB0aGlzLnNldENvbW11bmVzKGRhdGEuY29tbXVuZXMgfHwgRmlsdGVyLmRlZmF1bHRzLmNvbW11bmVzKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIHB1YmxpYyBpc0VtcHR5KCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UmVnaW9ucygpLmxlbmd0aCA9PT0gMFxyXG4gICAgICAgICAgICAmJiB0aGlzLmdldFRlcnJpdG9yaWVzKCkubGVuZ3RoID09PSAwXHJcbiAgICAgICAgICAgICYmIHRoaXMuZ2V0Q29udHJhY3RzKCkubGVuZ3RoID09PSAwXHJcbiAgICAgICAgICAgICYmIHRoaXMuZ2V0Q29tbXVuZXMoKS5sZW5ndGggPT09IDBcclxuICAgICAgICApXHJcbiAgICB9XHJcblxyXG5cclxuICAgIHB1YmxpYyBpc1NpbmdsZUNvbnRyYWN0KCk6IGJvb2xlYW4gfCBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGlzU2luZ2xlID0gKHRoaXMuZ2V0Q29udHJhY3RzKCkubGVuZ3RoID09PSAxICYmIHRoaXMuZ2V0Q29tbXVuZXMoKS5sZW5ndGggPT09IDApO1xyXG4gICAgICAgIHJldHVybiBpc1NpbmdsZSA/IHRoaXMuc2luZ2xlQ29udHJhY3QoKSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaW5nbGVDb250cmFjdCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldENvbnRyYWN0cygpWzBdO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBpc1NpbmdsZUNvbW11bmUoKTogYm9vbGVhbiB8IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3QgaXNTaW5nbGUgPSAodGhpcy5nZXRDb21tdW5lcygpLmxlbmd0aCA9PT0gMSk7XHJcbiAgICAgICAgcmV0dXJuIGlzU2luZ2xlID8gdGhpcy5nZXRDb21tdW5lcygpWzBdIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8vIEdFVFRFUlMgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8vIHB1YmxpYyBnZXREYXRlKCkge1xyXG4gICAgLy8gICAgIHJldHVybiB0aGlzLl9kYXRlIHx8IEZpbHRlci5kZWZhdWx0cy5kYXRlO1xyXG4gICAgLy8gfVxyXG4gICAgLy8gcHVibGljIGdldERhdGVQYXJhbSgpIHtcclxuICAgIC8vICAgICByZXR1cm4gbW9tZW50KHRoaXMuZ2V0RGF0ZSgpKS5mb3JtYXQoJ1lZWVktTU0tREQnKTtcclxuICAgIC8vIH1cclxuICAgIC8vIHB1YmxpYyBnZXRUeXBlcygpOiBzdHJpbmdbXSB7XHJcbiAgICAvLyAgICAgcmV0dXJuIHRoaXMuX3R5cGVzIHx8IEZpbHRlci5kZWZhdWx0cy50eXBlcztcclxuICAgIC8vIH1cclxuICAgIC8vIHB1YmxpYyBnZXRNb2RlcygpOiBzdHJpbmdbXSB7XHJcbiAgICAvLyAgICAgcmV0dXJuIHRoaXMuX21vZGVzIHx8IEZpbHRlci5kZWZhdWx0cy5tb2RlcztcclxuICAgIC8vIH1cclxuICAgIHB1YmxpYyBnZXRSZWdpb25zKCk6IHN0cmluZ1tdIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVnaW9ucyB8fCBGaWx0ZXIuZGVmYXVsdHMucmVnaW9ucztcclxuICAgIH1cclxuICAgIHB1YmxpYyBnZXRUZXJyaXRvcmllcygpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RlcnJpdG9yaWVzIHx8IEZpbHRlci5kZWZhdWx0cy50ZXJyaXRvcmllcztcclxuICAgIH1cclxuICAgIHB1YmxpYyBnZXRDb250cmFjdHMoKTogc3RyaW5nW10ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jb250cmFjdHMgfHwgRmlsdGVyLmRlZmF1bHRzLmNvbnRyYWN0cztcclxuICAgIH1cclxuICAgIHB1YmxpYyBnZXRDb21tdW5lcygpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbW11bmVzIHx8IEZpbHRlci5kZWZhdWx0cy5jb21tdW5lcztcclxuICAgIH1cclxuXHJcbiAgICAvLyBTRVRURVJTIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvLyBwdWJsaWMgc2V0RGF0ZShkYXRlOiBEYXRlKSB7XHJcbiAgICAvLyAgICAgdGhpcy5fZGF0ZSA9IGRhdGU7XHJcbiAgICAvLyB9XHJcbiAgICAvLyBwdWJsaWMgc2V0VHlwZXModHlwZXM6IHN0cmluZ1tdKSB7XHJcbiAgICAvLyAgICAgdGhpcy5fdHlwZXMgPSB0eXBlcztcclxuICAgIC8vIH1cclxuICAgIC8vIHB1YmxpYyBzZXRNb2Rlcyhtb2Rlczogc3RyaW5nW10pIHtcclxuICAgIC8vICAgICB0aGlzLl9tb2RlcyA9IG1vZGVzXHJcbiAgICAvLyB9XHJcbiAgICBwdWJsaWMgc2V0UmVnaW9ucyhyZWdpb25zOiBzdHJpbmdbXSkge1xyXG4gICAgICAgIHRoaXMuX3JlZ2lvbnMgPSByZWdpb25zO1xyXG4gICAgfVxyXG4gICAgcHVibGljIHNldFRlcnJpdG9yaWVzKHRlcnJpdG9yaWVzOiBzdHJpbmdbXSkge1xyXG4gICAgICAgIHRoaXMuX3RlcnJpdG9yaWVzID0gdGVycml0b3JpZXNcclxuICAgIH1cclxuICAgIHB1YmxpYyBzZXRDb250cmFjdHMoY29udHJhY3RzOiBzdHJpbmdbXSkge1xyXG4gICAgICAgIHRoaXMuX2NvbnRyYWN0cyA9IGNvbnRyYWN0cztcclxuICAgIH1cclxuICAgIHB1YmxpYyBzZXRDb21tdW5lcyhjb21tdW5lczogc3RyaW5nW10pIHtcclxuICAgICAgICB0aGlzLl9jb21tdW5lcyA9IGNvbW11bmVzO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBwdWJsaWMgaXNFcXVhbFRvKGZpbHRlcjogRmlsdGVyKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICh0aGlzLnRvSlNPTigpID09PSBmaWx0ZXIudG9KU09OKCkpXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGhhc1BlcmltZXRlcigpIHtcclxuICAgICAgICByZXR1cm4gKHRoaXMuZ2V0UmVnaW9ucygpLmxlbmd0aCB8fCB0aGlzLmdldFRlcnJpdG9yaWVzKCkubGVuZ3RoIHx8IHRoaXMuZ2V0Q29udHJhY3RzKCkubGVuZ3RoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTRVJJQUxJWkFUSU9OIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21KU09OKGpzb246IG9iamVjdCk6IEZpbHRlciB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBGaWx0ZXIoanNvbik7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHRvT2JqZWN0KCkgOiBvYmplY3Qge1xyXG4gICAgICAgIHJldHVybiAge1xyXG4gICAgICAgICAgICByZWdpb25zOiB0aGlzLmdldFJlZ2lvbnMoKSxcclxuICAgICAgICAgICAgdGVycml0b3JpZXM6IHRoaXMuZ2V0VGVycml0b3JpZXMoKSxcclxuICAgICAgICAgICAgY29udHJhY3RzOiB0aGlzLmdldENvbnRyYWN0cygpLFxyXG4gICAgICAgICAgICBjb21tdW5lczogdGhpcy5nZXRDb21tdW5lcygpLFxyXG4gICAgICAgICAgICAvLyBkYXRlOiB0aGlzLmdldERhdGUoKSxcclxuICAgICAgICAgICAgLy8gdHlwZXM6IHRoaXMuZ2V0VHlwZXMoKSxcclxuICAgICAgICAgICAgLy8gbW9kZXM6IHRoaXMuZ2V0TW9kZXMoKSxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBwdWJsaWMgdG9KU09OKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KHRoaXMudG9PYmplY3QoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHRvU3RvcmFnZSgpIHtcclxuICAgICAgICBsZXQganNvbiA9IHRoaXMudG9KU09OKClcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImZpbHRlclwiLCBqc29uKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21TdG9yYWdlKCk6IEZpbHRlciB7XHJcblxyXG4gICAgICAgIGxldCBmaWx0ZXIgPSBuZXcgRmlsdGVyKCk7XHJcblxyXG4gICAgICAgIC8vIGZpbGwgZnJvbSBzdG9yYWdlXHJcbiAgICAgICAgbGV0IHBhcmFtcyA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZmlsdGVyXCIpO1xyXG4gICAgICAgIGlmIChwYXJhbXMpIHtcclxuICAgICAgICAgICAgcGFyYW1zID0gSlNPTi5wYXJzZShwYXJhbXMpO1xyXG4gICAgICAgICAgICBmaWx0ZXIuc2V0UmVnaW9ucyhwYXJhbXNbJ3JlZ2lvbnMnXSB8fCB0aGlzLmRlZmF1bHRzLnJlZ2lvbnMpO1xyXG4gICAgICAgICAgICBmaWx0ZXIuc2V0VGVycml0b3JpZXMocGFyYW1zWyd0ZXJyaXRvcmllcyddIHx8IHRoaXMuZGVmYXVsdHMudGVycml0b3JpZXMpO1xyXG4gICAgICAgICAgICBmaWx0ZXIuc2V0Q29udHJhY3RzKHBhcmFtc1snY29udHJhY3RzJ10gfHwgdGhpcy5kZWZhdWx0cy5jb250cmFjdHMpO1xyXG4gICAgICAgICAgICBmaWx0ZXIuc2V0Q29tbXVuZXMocGFyYW1zWydjb21tdW5lcyddIHx8IHRoaXMuZGVmYXVsdHMuY29tbXVuZXMpO1xyXG4gICAgICAgICAgICAvLyBmaWx0ZXIuc2V0RGF0ZShuZXcgRGF0ZShwYXJhbXNbJ2RhdGUnXSB8fCB0aGlzLmRlZmF1bHRzLmRhdGUpKTtcclxuICAgICAgICAgICAgLy8gZmlsdGVyLnNldFR5cGVzKHBhcmFtc1sndHlwZXMnXSB8fCB0aGlzLmRlZmF1bHRzLnR5cGVzKTtcclxuICAgICAgICAgICAgLy8gZmlsdGVyLnNldE1vZGVzKHBhcmFtc1snbW9kZXMnXSB8fCB0aGlzLmRlZmF1bHRzLm1vZGVzKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZpbHRlclxyXG5cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY2xlYXJTdG9yYWdlKCkge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiZmlsdGVyXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFFVRVJZIFBBUkFNUyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIHRvUGFyYW1zKGZpZWxkczogc3RyaW5nW10gPSBudWxsKTogYW55IHtcclxuICAgICAgICBsZXQgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAvLyBkYXk6IHRoaXMuZ2V0RGF0ZVBhcmFtKCksXHJcbiAgICAgICAgICAgIC8vIHR5cGVzOiB0aGlzLmdldFR5cGVzKCkuam9pbignLCcpLFxyXG4gICAgICAgICAgICAvLyBtb2RlczogdGhpcy5nZXRNb2RlcygpLmpvaW4oJywnKSxcclxuICAgICAgICAgICAgcmVnaW9uczogdGhpcy5nZXRSZWdpb25zKCkuam9pbignLCcpLFxyXG4gICAgICAgICAgICB0ZXJyaXRvcmllczogdGhpcy5nZXRUZXJyaXRvcmllcygpLmpvaW4oJywnKSxcclxuICAgICAgICAgICAgY29udHJhY3RzOiB0aGlzLmdldENvbnRyYWN0cygpLmpvaW4oJywnKSxcclxuICAgICAgICAgICAgY29tbXVuZXM6IHRoaXMuZ2V0Q29tbXVuZXMoKS5qb2luKCcsJyksXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBwaWNrIHRoZSBmaWVsZHMgd2Ugd2FudFxyXG4gICAgICAgIGlmIChmaWVsZHMpIHBhcmFtcyA9IF9waWNrKHBhcmFtcywgZmllbGRzKVxyXG5cclxuICAgICAgICByZXR1cm4gcGFyYW1zO1xyXG5cclxuICAgIH1cclxuXHJcblxyXG59Il19