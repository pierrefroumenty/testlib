/**
 * @fileoverview added by tsickle
 * Generated from: lib/models/favorite.model.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Filter } from './filter.model';
var Favorite = /** @class */ (function () {
    function Favorite(data) {
        if (data === void 0) { data = {}; }
        this.setId(data.id);
        this.setFilter(data.filter || new Filter());
        this.setLabel(data.label || '');
    }
    // GETTERS -------------------------------------------
    // GETTERS -------------------------------------------
    /**
     * @return {?}
     */
    Favorite.prototype.getId = 
    // GETTERS -------------------------------------------
    /**
     * @return {?}
     */
    function () {
        return this._id;
    };
    /**
     * @return {?}
     */
    Favorite.prototype.getFilter = /**
     * @return {?}
     */
    function () {
        return this._filter || new Filter();
    };
    /**
     * @return {?}
     */
    Favorite.prototype.getLabel = /**
     * @return {?}
     */
    function () {
        return this._label || '';
    };
    // SETTERS -------------------------------------------
    // SETTERS -------------------------------------------
    /**
     * @param {?} id
     * @return {?}
     */
    Favorite.prototype.setId = 
    // SETTERS -------------------------------------------
    /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        this._id = id;
        return this;
    };
    /**
     * @param {?} filter
     * @return {?}
     */
    Favorite.prototype.setFilter = /**
     * @param {?} filter
     * @return {?}
     */
    function (filter) {
        this._filter = filter;
        return this;
    };
    /**
     * @param {?} label
     * @return {?}
     */
    Favorite.prototype.setLabel = /**
     * @param {?} label
     * @return {?}
     */
    function (label) {
        this._label = label;
        return this;
    };
    // SERIALIZATION -------------------------------------------
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    Favorite.fromJSON = 
    // SERIALIZATION -------------------------------------------
    /**
     * @param {?} json
     * @return {?}
     */
    function (json) {
        json.filter = Filter.fromJSON(json.filter);
        return new Favorite(json);
    };
    /**
     * @return {?}
     */
    Favorite.prototype.toObject = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var o = {
            label: this.getLabel(),
            filter: this.getFilter(),
        };
        if (this.getId())
            o.id = this.getId();
        return o;
    };
    /**
     * @return {?}
     */
    Favorite.prototype.toJSON = /**
     * @return {?}
     */
    function () {
        return JSON.stringify(this.toObject());
    };
    return Favorite;
}());
export { Favorite };
if (false) {
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._id;
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._filter;
    /**
     * @type {?}
     * @private
     */
    Favorite.prototype._label;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AdGVsZW8vY29tbW9uLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9mYXZvcml0ZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUV4QztJQU1JLGtCQUFZLElBQWM7UUFBZCxxQkFBQSxFQUFBLFNBQWM7UUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksTUFBTSxFQUFFLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUlELHNEQUFzRDs7Ozs7SUFFL0Msd0JBQUs7Ozs7O0lBQVo7UUFDSSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDcEIsQ0FBQzs7OztJQUNNLDRCQUFTOzs7SUFBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxNQUFNLEVBQUUsQ0FBQztJQUN4QyxDQUFDOzs7O0lBQ00sMkJBQVE7OztJQUFmO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBR0Qsc0RBQXNEOzs7Ozs7SUFFL0Msd0JBQUs7Ozs7OztJQUFaLFVBQWEsRUFBaUI7UUFDekIsSUFBSSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDZCxPQUFPLElBQUksQ0FBQztJQUNqQixDQUFDOzs7OztJQUNNLDRCQUFTOzs7O0lBQWhCLFVBQWlCLE1BQWM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFDTSwyQkFBUTs7OztJQUFmLFVBQWdCLEtBQWE7UUFDekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELDREQUE0RDs7Ozs7O0lBRTlDLGlCQUFROzs7Ozs7SUFBdEIsVUFBdUIsSUFBVTtRQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQzFDLE9BQU8sSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUE7SUFDN0IsQ0FBQzs7OztJQUVNLDJCQUFROzs7SUFBZjs7WUFDUSxDQUFDLEdBQVE7WUFDVCxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUN0QixNQUFNLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRTtTQUN6QjtRQUNELElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUFFLENBQUMsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO1FBQ3ZDLE9BQU8sQ0FBQyxDQUFBO0lBQ1osQ0FBQzs7OztJQUVNLHlCQUFNOzs7SUFBYjtRQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQTtJQUMxQyxDQUFDO0lBQ0wsZUFBQztBQUFELENBQUMsQUE3REQsSUE2REM7Ozs7Ozs7SUEzREcsdUJBQW9COzs7OztJQUNwQiwyQkFBd0I7Ozs7O0lBQ3hCLDBCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpbHRlciB9IGZyb20gJy4vZmlsdGVyLm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBGYXZvcml0ZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBfaWQ6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX2ZpbHRlcjogRmlsdGVyO1xyXG4gICAgcHJpdmF0ZSBfbGFiZWw6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihkYXRhOiBhbnkgPSB7fSkge1xyXG4gICAgICAgIHRoaXMuc2V0SWQoZGF0YS5pZCk7XHJcbiAgICAgICAgdGhpcy5zZXRGaWx0ZXIoZGF0YS5maWx0ZXIgfHwgbmV3IEZpbHRlcigpKTtcclxuICAgICAgICB0aGlzLnNldExhYmVsKGRhdGEubGFiZWwgfHwgJycpO1xyXG4gICAgfVxyXG5cclxuXHJcblxyXG4gICAgLy8gR0VUVEVSUyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgcHVibGljIGdldElkKCkgOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pZDtcclxuICAgIH1cclxuICAgIHB1YmxpYyBnZXRGaWx0ZXIoKSA6IEZpbHRlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ZpbHRlciB8fCBuZXcgRmlsdGVyKCk7XHJcbiAgICB9XHJcbiAgICBwdWJsaWMgZ2V0TGFiZWwoKSA6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xhYmVsIHx8ICcnO1xyXG4gICAgfVxyXG4gICBcclxuXHJcbiAgICAvLyBTRVRURVJTIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICBwdWJsaWMgc2V0SWQoaWQ6IG51bWJlciB8IG51bGwpIDogRmF2b3JpdGUge1xyXG4gICAgICAgICB0aGlzLl9pZCA9IGlkO1xyXG4gICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuICAgIHB1YmxpYyBzZXRGaWx0ZXIoZmlsdGVyOiBGaWx0ZXIpIDogRmF2b3JpdGUge1xyXG4gICAgICAgIHRoaXMuX2ZpbHRlciA9IGZpbHRlcjtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuICAgIHB1YmxpYyBzZXRMYWJlbChsYWJlbDogc3RyaW5nKSA6IEZhdm9yaXRlIHtcclxuICAgICAgICB0aGlzLl9sYWJlbCA9IGxhYmVsO1xyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG4gICBcclxuICAgIC8vIFNFUklBTElaQVRJT04gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpTT04oanNvbiA6IGFueSkgOiBGYXZvcml0ZSB7XHJcbiAgICAgICAganNvbi5maWx0ZXIgPSBGaWx0ZXIuZnJvbUpTT04oanNvbi5maWx0ZXIpXHJcbiAgICAgICAgcmV0dXJuIG5ldyBGYXZvcml0ZShqc29uKVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b09iamVjdCgpIDogb2JqZWN0IHtcclxuICAgICAgICBsZXQgbzphbnkgPSAge1xyXG4gICAgICAgICAgICBsYWJlbDogdGhpcy5nZXRMYWJlbCgpLFxyXG4gICAgICAgICAgICBmaWx0ZXI6IHRoaXMuZ2V0RmlsdGVyKCksXHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgICAgaWYgKHRoaXMuZ2V0SWQoKSkgby5pZCA9IHRoaXMuZ2V0SWQoKVxyXG4gICAgICAgIHJldHVybiBvXHJcbiAgICB9XHJcbiAgICBcclxuICAgIHB1YmxpYyB0b0pTT04oKSA6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KHRoaXMudG9PYmplY3QoKSlcclxuICAgIH1cclxufSJdfQ==